<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
| 
*/

Route::group(['prefix' => 'auth'], function ($router)
{
    Route::post('login', 'api\Auth\AuthController@login');
    Route::post('forgot-password', 'api\Auth\AuthController@forgotPassword');
    Route::get('reset-password/{token}', 'api\Auth\AuthController@resetPassword');
    Route::post('reset-password', 'api\Auth\AuthController@resetUserPassword');

    Route::group(['middleware' => 'auth:api'], function ()
    {
        Route::get('logout', 'api\Auth\AuthController@logout');
    });

});

Route::group(['middleware' => 'auth:api'], function ($router)
{

    Route::get('user', function (Request $request)
    {
        return $request->user();
    });

    // Routes for employees
    Route::group(['prefix' => 'employee'], function ()
    {
        Route::post('create', 'api\Employee\EmployeeController@createEmployee');
        Route::post('update/{id}', 'api\Employee\EmployeeController@updateEmployee');
        Route::post('delete/{id}', 'api\Employee\EmployeeController@deleteEmployee');
        Route::get('list', 'api\Employee\EmployeeController@allEmployees');
        Route::get('{id}', 'api\Employee\EmployeeController@getById');
    });

    // Routes for banks
    Route::group(['prefix' => 'bank'], function ()
    {
        Route::get('all', 'api\Bank\BankController@banksList');
        Route::get('{id}', 'api\Bank\BankController@getBankDetail');
        Route::post('add', 'api\Bank\BankController@createBank');
        Route::post('update/{id}', 'api\Bank\BankController@updateBank');
        Route::post('delete/{id}', 'api\Bank\BankController@deleteBank');
    });

    // Routes for profile
    Route::group(['prefix' => 'profile'], function ()
    {
        Route::post('update/{id}', 'api\Profile\ProfileController@updateProfile');
    });

    // Routes for roles
    Route::group(['prefix' => 'role'], function ()
    {
        Route::get('all', 'api\Role\RoleController@getAllRoles');
    });

    // Routes for branches
    Route::group(['prefix' => 'branches'], function ()
    {
        Route::get('all', 'api\Branch\BranchController@getAllBracnhes');
        Route::post('create', 'api\Branch\BranchController@createBranch');
    });

    // Routes for branches
    Route::group(['prefix' => 'state'], function ()
    {
        Route::get('all', 'api\Branch\BranchController@getAllBracnhes');
        Route::post('create', 'api\Branch\BranchController@createBranch');
    });



    // Routes for statuses
    Route::group(['prefix' => 'status'], function ()
    {
        Route::get('all', 'api\Status\StatusController@getDefaultStatues');
    });

    // Routes for loans
    Route::group(['prefix' => 'loan'], function ()
    {
        Route::get('types', 'api\Loan\LoanController@allLoanTypes');
    });

    // Routes for loans
    Route::group(['prefix' => 'languages'], function ()
    {
        Route::get('list', 'api\Language\LanguageController@allLanguages');
    });

    // Routes for loans
    Route::group(['prefix' => 'common'], function ()
    {
        Route::get('sales_agents', 'api\Common\CommonController@getSalesAgent');
        Route::get('team_leads', 'api\Common\CommonController@getTeamLead');
        Route::get('states', 'api\Common\CommonController@getStates');
        Route::get('get-cities/{id}', 'api\Common\CommonController@getCities');
    });

    // Routes for loans
    Route::group(['prefix' => 'end_user'], function ()
    {
        Route::post('add', 'api\EndUser\EndUserController@addEndUser');
        Route::get('list', 'api\EndUser\EndUserController@getAllEndUsers');
        Route::get('{id}', 'api\EndUser\EndUserController@getById');
        Route::post('edit/{id}', 'api\EndUser\EndUserController@editEndUser');
        Route::get('delete/{id}', 'api\EndUser\EndUserController@deleteEndUser');
        Route::get('logs/{id}', 'api\EndUser\EndUserController@getEndUserLogData');
        Route::get('details/{id}', 'api\EndUser\EndUserController@getEndUserDetails');
        Route::get('get-mobile-number/{id}', 'api\EndUser\EndUserController@getEmployeeNumber');
    });

    // Routes for loans
    Route::group(['prefix' => 'document'], function ()
    {
        Route::post('upload', 'api\Document\DocumentController@uploadFile');
        Route::post('process', 'api\Document\DocumentController@processFile');
        Route::get('get-processed-files', 'api\Document\DocumentController@getProcessedFiles');
        Route::get('delete-processed-file/{id}', 'api\Document\DocumentController@deleteProcessedFile');
        // Route::get('team_leads', 'api\Common\CommonController@getTeamLead');
        
    });

    // Routes for Ivr Registration settings
    Route::group(['prefix' => 'ivr_setting'], function ()
    {
        Route::post('add-new', 'api\IVRSettingRules\IVRSettingRulesController@addNewRule');
        Route::get('all', 'api\IVRSettingRules\IVRSettingRulesController@index');
        Route::get('details/{id}', 'api\IVRSettingRules\IVRSettingRulesController@getRuleById');
    });

    // Routes for Ivr Verification settings
    Route::group(['prefix' => 'ivr_setting_verify'], function ()
    {
        Route::post('add-new-ivr-verification', 'api\IVRSettingRules\IVRVerificationRulesController@addNewVerificationRule');
        Route::get('all', 'api\IVRSettingRules\IVRVerificationRulesController@index');
        Route::get('details/{id}', 'api\IVRSettingRules\IVRVerificationRulesController@getVerifyRuleById');
    });

});

Route::group(['prefix' => 'download'], function ()
{
    Route::get('success-document/{id}/{bank_id}', 'api\Document\DocumentController@successDownloadFile');
    Route::get('failed-document/{id}/{bank_id}', 'api\Document\DocumentController@failedDownloadFile');
    Route::get('document/{id}/{bank_id}', 'api\Document\DocumentController@DownloadFile');
    // Route::get('team_leads', 'api\Common\CommonController@getTeamLead');
    
});

Route::post('outbound-call-webhook', 'api\Common\DialogManagerController@callback');


// Routes for employees
Route::group(['prefix' => 'call'], function ()
{
    Route::post('register/{id}', 'api\DialogManager\DialogManagerController@register');
});


 Route::get('test-ai-curl', function() {
    return 'okkkk';
 });