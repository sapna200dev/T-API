<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Your Jet password has been updated</title>    
  </head>

  <body>

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width">
  <meta name="format-detection" content="telephone=no">
  <!--[if !mso]>
      <!-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800,300&subset=latin" rel="stylesheet" type="text/css">
  <!--<![endif]-->
  <title>Jet.com - Password Reset</title>
  <style type="text/css">
    *{
            margin:0;
            padding:0;
            font-family:'OpenSans-Light', "Helvetica Neue", "Helvetica",Calibri, Arial, sans-serif;
            font-size:100%;
            line-height:1.6;
          }
          img{
            max-width:100%;
          }
          body{
            -webkit-font-smoothing:antialiased;
            -webkit-text-size-adjust:none;
            width:100%!important;
            height:100%;
          }
          a{
            color:#348eda;
          }
          .btn-primary{
            text-decoration:none;
            color:#FFF;
            background-color:#a55bff;
            border:solid #a55bff;
            border-width:10px 20px;
            line-height:2;
            font-weight:bold;
            margin-right:10px;
            text-align:center;
            cursor:pointer;
            display:inline-block;
          }
          .last{
            margin-bottom:0;
          }
          .first{
            margin-top:0;
          }
          .padding{
            padding:10px 0;
          }
          table.body-wrap{
            width:100%;
            padding:0px;
            padding-top:20px;
            margin:0px;
          }
          table.body-wrap .container{
            border:1px solid #f0f0f0;
          }
          table.footer-wrap{
            width:100%;
            clear:both!important;
          }
          .footer-wrap .container p{
            font-size:12px;
            color:#666;
          }
          table.footer-wrap a{
            color:#999;
          }
          .footer-content{
            margin:0px;
            padding:0px;
          }
          h1,h2,h3{
            color:#660099;
            font-family:'OpenSans-Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;
            line-height:1.2;
            margin-bottom:15px;
            margin:40px 0 10px;
            font-weight:200;
          }
          h1{
            font-family:'Open Sans Light';
            font-size:45px;
          }
          h2{
            font-size:28px;
          }
          h3{
            font-size:22px;
          }
          p,ul,ol{
            margin-bottom:10px;
            font-weight:normal;
            font-size:14px;
          }
          ul li,ol li{
            margin-left:5px;
            list-style-position:inside;
          }
          .container{
            display:block!important;
            max-width:600px!important;
            margin:0 auto!important;
            clear:both!important;
          }
          .body-wrap .container{
            padding:0px;
          }
          .content,.footer-wrapper{
            max-width:600px;
            margin:0 auto;
            padding:20px 33px 20px 37px;
            display:block;
          }
          .content table{
            width:100%;
          }
          .content-message p{
            margin:20px 0px 20px 0px;
            padding:0px;
            font-size:22px;
            line-height:38px;
            font-family:'OpenSans-Light',Calibri, Arial, sans-serif;
          }
          .preheader{
            display:none !important;
            visibility:hidden;
            opacity:0;
            color:transparent;
            height:0;
            width:0;
          }
  </style>
</head>

  <body bgcolor="#f6f6f6">

  <div class="container">
    <h2>Reset Password</h2>

    <form class="form-horizontal" method="POST" action="{{ url('api/auth/reset-password') }}">
      <div class="form-group">
        <label class="control-label col-sm-2" for="email">Email:</label>
        <div class="col-sm-10">
          <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
          <input type="hidden" class="form-control" id="token" value=" {{$token}}" placeholder="Enter email" name="token">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2" for="pwd">Password:</label>
        <div class="col-sm-10">          
          <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd">
        </div>
      </div>
      <div class="form-group">        
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" class="btn btn-default">reset</button>
        </div>
      </div>
    </form>
  </div>
    <!-- /body -->
  </body>

</html>
    
    
    
    
    
  </body>
</html>
