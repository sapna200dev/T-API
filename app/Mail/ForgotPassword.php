<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForgotPassword extends Mailable
{
    use Queueable, SerializesModels;

    public $token;
    public $user;
    public $url;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($token, $user_obj)
    {
        $this->token = $token;
        $this->user = $user_obj;
        $this->url = env('MAIL_URL', 'http://sapna-dev.turant.com:3000/');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.forgotPassword')->with(['token' => $this->token, 'user'=> $this->user,  'url' => $this->url]);
    }
}
