<?php 

namespace App\Modals\Employee\Actions\Relations;


use Illuminate\Database\Eloquent\Model;
use App\Modals\Bank\Bank;
use App\Modals\Role\Role; 
use App\Modals\Branch\Branch;




trait EmployeeRelationShip 
{

	/** 
	 * Get user relation
	 */ 
	public function employee_role()
	{
	    return $this->belongsTo(Role::class, 'role_id');
	}

	/**
	 * Get bank relation
	 */ 
	public function bank()
	{
	    return $this->belongsTo(Bank::class);
	}



	/**
	 * Get customer.
	 */
	public function branch()
	{
	    return $this->belongsTo(Branch::class, 'branch_id');
	}


}

