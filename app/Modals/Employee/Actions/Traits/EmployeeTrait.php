<?php
namespace App\Modals\Employee\Actions\Traits;

use Auth;
use App\Modals\Employee\Employee;
use App\Modals\User\User;
use App\Events\SendEmployeeUserPassEvent;


use Exception;

trait EmployeeTrait
{

    /**
     * Get all customers for bank
     *
     * @param $bank_id
     * @return object
     */
    public function getAllEmployessForBank()
    {
        $returnVal = null;
        try
        {
            $user = Auth::user()->id; $bank_id = Auth::user()->bank_id;


            $query = Employee::with(['branch', 'employee_role'])->where('created_by', $user);
            $query = $query->where('bank_id', $bank_id)->get();


            $returnVal = $query;
        } catch(Exception $e) {
            $returnVal = null;
            throw new Exception($e->getMessage() , 400);
        } // End Try-Catch
        return $returnVal;
    } // End function

    /**
     * Get all customers for bank
     *
     * @param $id
     * @return object
     */
    public function getEmployeeDetailById($id)
    {
        $returnVal = null;
        try
        {
            $query = Employee::with(['branch', 'employee_role'])->where('id', $id)->first();

            $returnVal = $query;
        } catch(Exception $e) {
            $returnVal = null;
            throw new Exception($e->getMessage() , 400);
        } // End Try-Catch
        return $returnVal;
    } // End function


    /**
     * Get all customers for bank
     *
     * @param $id
     * @return object
     */
    public function validateEmployeeUsername($username)
    {
        $returnVal = null;
        try
        {
            $query = Employee::where('email', $username)->first();

            $returnVal = $query;
        } catch(Exception $e) {
            $returnVal = null;
            throw new Exception($e->getMessage() , 400);
        } // End Try-Catch
        return $returnVal;
    } // End function


    /**
     * Check employee mobile number by sapna
     *
     * @param $bank_id
     * @return object
     */
    private function checkEmployeeNumber($request)
    {
        $returnVal=null;
        try {
            $bank_id = Auth::user()->bank_id;

            $query = Employee::where('mobile_number', $request->mobile_number)->where('bank_id', $bank_id)->first();

            $returnVal = $query;
        } catch(Exception $e) {
            $returnVal = null;
            throw new Exception($e->getMessage() , 400);
        } // End Try-Catch
        return $returnVal;
    }



    /**
     * Get all bank customers by sapna
     *
     * @param $bank_id
     * @return object
     */
    public function getEmployeeBankIdByNumber($number)
    {
        $returnVal=null;
        try {

            $query = Employee::where('mobile_number', $number)->where('status', 'active')->first();

            $returnVal = $query;
        } catch(Exception $e) {
            $returnVal = null;
            throw new Exception($e->getMessage() , 400);
        } // End Try-Catch

        return $returnVal;
    }



    /**
     * Get all bank customers by sapna
     *
     * @param $bank_id
     * @return object
     */
    public function getAllBankCustomers($bank_id)
    {
        $returnVal=null;
        try {
            $query = Employee::with(['branch', 'employee_role'])->where('bank_id', $bank_id)->get();

            $returnVal = $query;
        } catch (Exception $e) {
            $returnVal = null;
            throw new Exception($e->getMessage() , 400);
        }

        return $returnVal; 
    }

    /**
     * Create new customer for bank by sapna
     *
     * @param $request
     * @return object
     */
    public function createdNewEmployeForBank($request)
    {
        $returnVal = null;
        try
        {
            $created_by = Auth::user()->id;
            $bank_id = Auth::user()->bank_id;

            $checkDuplicate = $this->checkEmployeeNumber($request);
            if($checkDuplicate) {
                throw new Exception("This mobile number is already registered for this bank, please use another one !", 500);
            } else {
                $query = new Employee($request->employee);
                $query->created_by = $created_by;
                $query->bank_id = $bank_id!=null ? $bank_id : $created_by;
                $query->save();

                $this->createUser($request);

                $returnVal = $query;

                return $returnVal;
            } // End if-else

        }
        catch(Exception $e)
        {
            $returnVal = null;
            throw new Exception($e->getMessage() , 400);
        } // End Try-Catch
      
    } // End function
    

    /**
     * Create new bank user by sapna
     *
     * @param $request
     * @return object
     */
    private function createUser($request) 
    { 
        $returnVal=null;
        try {
            $profile_img = '/uploads/profiles/default/default_profile.jpg';

            $query = User::create([
                'name' => $request->employee['first_name'].''.$request->employee['last_name'],
                'email' => $request->admin['username'],
                'username' => $request->admin['username'],
                'password' =>  bcrypt($request->admin['password']),
                'status' =>  ($request->employee['status'] == 'active' ) ? 1 : 0,
                'image' => $profile_img
            ]);

            $this->createUserRole($request->employee['role_id'], $query->id);

            event(new SendEmployeeUserPassEvent($request));

            $returnVal = $query;

        } catch (Exception $e) {
            $returnVal = null;
            throw new Exception($e->getMessage() , 400); 
        }

        return $returnVal;
    }


    /**
     * Check duplicate value
     *
     * @param $request
     * @return object
     */
    public function checkDuplicateEmployee($request)
    {
        $returnVal=null;
        try {
            $query = Employee::where('username', $request->employee['username']);
            $query = $query->where('email',  $request->employee['email'])->first();

            $returnVal = $query;
        } catch (Exception $e) {
            $returnVal = null;
            throw new Exception($e->getMessage() , 400);  
        }

        return $returnVal;
    }


    /**
     * Update customer for bank
     *
     * @param $request
     * @return object
     */
    public function updateCustomerForBank($id, $request)
    {
        $returnVal = null;
        try
        {
            $updated_by = Auth::user()->id;
            $bank_id = Auth::user()->bank_id;
            $request['updated_by'] = $updated_by;
            $request['bank_id'] = $bank_id;
            
            $query = Employee::where('id', $id)->update($request->employee);

            if(isset($request->admin['password']) &&  $request->admin['password']!='') {
                $this->updateUserPassword($request->admin['username'], $request->admin['password']);
            }

            
            $returnVal = $query;
        } catch(Exception $e) {
            $returnVal = null;
            throw new Exception($e->getMessage() , 400);
        } // End Try-Catch

        return $returnVal;
    } // End function

    
    /**
     * Delete customer for bank
     *
     * @param $id, $request
     * @return object
     */
    public function deleteCustomerForBank($id, $request)
    {
        $returnVal = null;
        try
        {
            $user = Auth::user()->id;
            // Check customer exist or not
            $isExist = $this->checkCustomerExistForBank($id);
            if($isExist==null) { throw new Exception("Bank or customer dose not exist, please verify it", 500); }

            // Delete employee by id
            $query = $this->deleteEmployeeById($id);

            // CHeck username exist or not
            $user = $this->validateUsername($request['username']);
            if($user==null) { throw new Exception("Username dose not exits", 500); }

            // Delete user role from DB
            $this->deleteUserRole($user->id, $request['role_id']);

            // Delete user from DB
            $this->deleteUser($request['username']);


            $returnVal = $query;
        } catch(Exception $e) {
            $returnVal = null;
            throw new Exception($e->getMessage() , 400);
        } // End Try-Catch

        return $returnVal;
    } // End function

    /**
     * Delete employee
     *
     * @param $id
     * @return object
     */
    private function deleteEmployeeById($id)
    {
        $returnVal=null;
        try {
            $user = Auth::user()->id;
            $query = Employee::where('id', $id)
                                ->where('created_by', $user)
                                ->delete();

            return $query;
        }   
        catch(Exception $e)
        {
            $returnVal = null;
            throw new Exception($e->getMessage() , 400);
        } // End Try-Catch

        return $returnVal;
    }


    /**
     * Check existing customer for bank
     *
     * @param $id
     * @return object
     */
    private function checkCustomerExistForBank($id)
    {
        $returnVal=null;
        try {
            $user = Auth::user()->id;
            $query = Employee::where('id', $id)->where('created_by', $user)->first();

            return $query;
        }   
        catch(Exception $e)
        {
            $returnVal = null;
            throw new Exception($e->getMessage() , 400);
        } // End Try-Catch

        return $returnVal;
    }



    /**
     * GET SALES AGENT
     *
     * @param $id
     * @return object
     */
    public function getSaleAgentByContact($contact, bool $isAgent=false)
    {
        $returnVal=null;
        try {
             $bank_id = Auth::user()->bank_id;

            $query = config('common.tables.modals.tbl_employees')::where('bank_id', $bank_id);
            $query = $query->where('mobile_number', $contact); 
            // else {
            //     $query = $query->where('mobile_number', 4); //Role is team_lead
            // }
           
            $query = $query->first();

            $returnVal = $query;
        } catch (Exception $e) {
            $returnVal = null;
            throw new Exception($e->getMessage(), 400);     
        } // End Try-Catch

        return $returnVal;
    }








// getTeamAgentByContact

}

