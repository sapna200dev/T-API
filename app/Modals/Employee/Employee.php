<?php

namespace App\Modals\Employee;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Tymon\JWTAuth\Contracts\JWTSubject;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use App\Modals\User\Actions\Relations\UserRelatiohShip;
use App\Modals\Employee\Actions\Relations\EmployeeRelationShip;
use App\Modals\Branch\Actions\Relations\BranchRelatiohShip;


class Employee extends Authenticatable
{
    use EmployeeRelationShip, BranchRelatiohShip;

    protected $table = 'tbl_employees';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'bank_id', 'first_name', 'last_name', 'branch_id', 'status', 'role_id', 'mobile_number', 'email', 'access_level', 'team_lead', 'username', 'created_at', 'created_by', 'updated_by', 'updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       'created_at', 'created_by', 'updated_at', 'updated_by',
    ];

}
