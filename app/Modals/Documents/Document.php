<?php

namespace App\Modals\Documents;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Tymon\JWTAuth\Contracts\JWTSubject;

use App\Modals\Role\Actions\Relations\RoleRelatiohShip;
use App\Modals\Documents\Actions\Relations\DocumentRelations;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;




class Document extends Authenticatable
{
    use Notifiable, RoleRelatiohShip, DocumentRelations;

    protected $table = 'tbl_documents';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uploaded_date_time ', 'file_name', 'middle_name', 'total_records', 'success_records', 'is_visible', 'failed_records', 'success_file_name', 'failed_file_name', 'created_at', 'updated_at', 'created_by', 'updated_by'
    ];



     /**
     * The attributes that are hidden to UI.
     *
     * @var array
     */
	protected $hidden = [
		'created_at', 'updated_at'
	];

}
