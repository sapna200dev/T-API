<?php 
namespace App\Modals\Documents\Actions\Traits;

use Hash;
use Log;
use Illuminate\Support\Str;
use App\Modals\Role\Role;
use App\Modals\UserRole;
use Auth;

use Carbon\Carbon;
use Exception;

trait DocumentTraits {

	/**
	 * Get all file by bank id
	 *
	 * @param $request
	 * @return object
	 */
	public function getProccessedFilesFromDB($request)
	{
		$returnVal=[];
		try {
			$bank_id = Auth::user()->bank_id;

			$query = config('common.tables.modals.tbl_documents')::where('created_by', $bank_id);
			$query = $query->where('is_visible', 1);
			$query = $query->get();

			$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = [];
			throw new Exception($e->getMessage(), 400);	
		}

		return $returnVal;
	}


	/**
	 * Get by id
	 *
	 * @param $request
	 * @return object
	 */
	public function getDocmentFileById($id, $bank_id)
	{
		$returnVal=null;
		try {
			$query = config('common.tables.modals.tbl_documents')::where('id', $id);
			$query = $query->where('created_by', $bank_id);
			$query = $query->where('is_visible', 1);
			$query = $query->first();

			$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);	
		}

		return $returnVal;
	}


	/**
	 * Get by id
	 *
	 * @param $request
	 * @return object
	 */
	public function deleteProcessedFileFromDB($id)
	{
		$returnVal=null;
		try {
			$bank_id = Auth::user()->bank_id;

			$query = config('common.tables.modals.tbl_documents')::where('id', $id);
			$query = $query->where('created_by', $bank_id);
			$query = $query->update([
				'is_visible' => 0
			]);

			$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);	
		}

		return $returnVal;
	}




	/**
	 * Check login user is validate or not
	 *
	 * @param $request
	 * @return object
	 */
	public function saveProcessDocument($file_name, $failedArray, $successArray, $failedFile, $successFile) {
		$returnVal = null;
		try {
			$user = Auth::user()->bank_id;

         	$query = config('common.tables.modals.tbl_documents')::updateOrCreate([
         		'file_name' => $file_name,
         		'created_by' => $user
         	], [
         		'total_records' => count($failedArray) + count($successArray),
         		'success_records' => count($successArray),
         		'failed_records' => count($failedArray),
         		'uploaded_date_time' => Carbon::now(),
         		'success_file_name' => $successFile,
         		'failed_file_name' => $failedFile,
         		'is_visible' => 1
         	]);

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function

	



}