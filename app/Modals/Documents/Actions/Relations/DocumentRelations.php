<?php 

namespace App\Modals\Documents\Actions\Relations;


use Illuminate\Database\Eloquent\Model;
use App\Modals\Role\Role;

use App\Modals\Loan\Loan;
use App\Modals\Language\Language;
// use App\Modals\User\User;


trait DocumentRelations 
{

	/**
	 * Get languages
	 */ 
	public function verifications()
	{
	     return $this->hasOne(config('common.tables.modals.tbl_verifications'), 'id', 'end_user_id');
	}

	/**
	 * Get languages
	 */ 
	public function bank()
	{
	     return $this->hasOne(config('common.tables.modals.tbl_banks'), 'id', 'bank_id');
	}


}

