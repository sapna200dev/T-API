<?php 
namespace App\Modals;

use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Exception;

trait ErrorLogTrait {
	/**
	 * Error logging in modularlogs files
	 *
	 * @param 
	 * @return string Domain name without subdomains.
	 */
	public function handleErrorLogs(Exception $ex, $type=null) {
		$exceptionObj = [];

		$exceptionObj['code'] = $ex->getCode() != 0 ? $ex->getCode() : 500;
		$exceptionObj['status'] = $type!=null ? $type : 'Error';
		$exceptionObj['msg'] = $ex->getMessage();

        return $exceptionObj;
	}
} 