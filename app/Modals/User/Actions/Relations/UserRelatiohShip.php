<?php 

namespace App\Modals\User\Actions\Relations;


use Illuminate\Database\Eloquent\Model;
use App\Modals\Role\Role;

use App\Modals\Customer\Customer;
use App\Modals\Bank\Bank;
// use App\Modals\User\User;



trait UserRelatiohShip 
{

	/**
	 * Get user roles
	 */ 
	// public function role()
	// {
	//     return $this->hasOne(Role::class, 'user_id');
	// }

	/**
	 * Get user roles
	 */ 
	public function role()
	{
	     return $this->belongsToMany('App\Modals\Role\Role', 'tbl_user_roles', 'user_id', 'role_id');
	}
	
	/**
	 * Get ifsce_code.
	 */
	public function ifsc_user()
	{
		// return $this->hasOne(IfscCode::class, 'user_id');
	}


	/**
	 * Get bank relation
	 */ 
	public function bank()
	{
	    return $this->hasOne(Bank::class, 'id', 'bank_id');
	}


	/**
	 * Get customer.
	 */
	public function customer()
	{
	    return $this->hasOne(Customer::class, 'created_by', 'id');
	}

}

