<?php 
namespace App\Modals\User\Actions\Traits;

use Auth;
use Hash;
use Log;
use Illuminate\Support\Str;
use App\Modals\UserRole;



use Carbon\Carbon;
use Exception;

trait UserTrait {
	/**
	 * Check login user is validate or not
	 *
	 * @param $request
	 * @return object
	 */
	public function authCheck($request) {
		$returnVal = null;
		try {
         	$query = config('common.tables.modals.tbl_users')::with(['role']);
         	$query = $query->where('username', $request->email);
         	$query = $query->first();

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function

	/**
	 * Check login user status
	 *
	 * @param $request
	 * @return object
	 */
	public function getUserStatus($request) {
		$returnVal = null;
		try {
         	$query = config('common.tables.modals.tbl_users')::where('username', $request->email);
         	$query = $query->where('status', 1);
         	$query = $query->first();

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function



	/**
	 * Validate forgot email in DB
	 *
	 * @param $request
	 * @return object
	 */
	public function validateForgotEmail($request) {
		$returnVal = null;
		try {
         	$query = config('common.tables.modals.tbl_users')::where(['email' => $request->email ]);
         	$query = $query->first();

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function


	/**
	 * Check forgot email in DB
	 *
	 * @param $request
	 * @return object
	 */
	public function validateUsername($username) {
		$returnVal = null;
		try {
         	$query = config('common.tables.modals.tbl_users')::where(['username' => $username ]);
         	$query = $query->first();

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function


	/**
	 * Update user new password
	 *
	 * @param $request
	 * @return object
	 */
	public function updateUserPassword($email, $password) {
		$returnVal = null;
		try {
         	$query = config('common.tables.modals.tbl_users')::where([ 'username' => $email ]);
         	$query = $query->update(['password' => bcrypt($password), 'updated_at'=>Carbon::now() ]);


         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function


	/**
	 * Update user status
	 *
	 * @param $request
	 * @return object
	 */
	public function updateUserStatus($bank_id, $email, $status=null) {
		$returnVal = null;
		try {
			$isStatus = ($status=='active') ? 1 : 0;

         	$query = config('common.tables.modals.tbl_users')::where([ 'bank_id' => $bank_id ])->where([ 'username' => $email ]);
         	$query = $query->update(['status' => $isStatus ]);
         	

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function


	/**
	 * Check login user status
	 *
	 * @param $request
	 * @return object
	 */
	public function getUserById($id) {
		$returnVal = null;
		try {
         	$query = config('common.tables.modals.tbl_users')::where('id', $id);
         	$query = $query->first();

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function



	/**
	 * Generate custom token for reset passsword
	 *
	 * @param $request
	 * @return object
	 */
	public function generateToken() {
	    return Str::random(32);
	} // End function

	/**
	 * Update user profile 
	 *
	 * @param $request $id
	 * @return object
	 */
	public function updateUserProfile($id, $request) {
		$returnVal = null; $fileUpload=null;
		try {

            if ($request->file('image')) {
                $imagePath = $request->file('image');
                $imageName = $imagePath->getClientOriginalName();
                $fileName = time().'.'.$imagePath->extension();

                $request->file('image')->move(public_path('/uploads/profiles/'.$id), $fileName);
                $filePath1 = '/uploads/profiles/'.$id.'/'.$fileName;

                $fileUpload =  $filePath1;
            }


         	$query = config('common.tables.modals.tbl_users')::where([ 'id' => $id ]);
         	$query = $query->update(['name' => $request->username, 'email' => $request->email, 'updated_at'=>Carbon::now() ]);
       

         	if((isset($request->password) && $request->password!='')) {
         		$query1 = config('common.tables.modals.tbl_users')::where([ 'id' => $id ]);
         		$query = $query1->update(['password' => bcrypt($request->password)]);
         	}
         
         	if((isset($request->image) && $request->file('image') && $request->image != null && $fileUpload!=null)) {
         		$query1 = config('common.tables.modals.tbl_users')::where([ 'id' => $id ]);
         		$query = $query1->update(['image' => $fileUpload ]);
         	}

         	$user = $this->getUserById($id);

         	$returnVal = $user;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	}


	/**
	 * Delete user from DB
	 *
	 * @param $username
	 * @return object
	 */
	public function deleteUser($username) {
		$returnVal = null;
		try {
			 $id = Auth::user()->id;

         	$query = config('common.tables.modals.tbl_users')::where(['email' => $username ]);
         	$query = $query->delete();

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function


	/**
	 * Delete user role from DB
	 *
	 * @param $user_id $role_id
	 * @return object
	 */
	public function deleteUserRole($user_id, $role_id) {
		$returnVal = null;
		try {

         	$query = UserRole::where(['user_id' => $user_id ])->where('role_id', $role_id);
         	$query = $query->delete();

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function


}