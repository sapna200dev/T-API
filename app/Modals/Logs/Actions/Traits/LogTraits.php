<?php 

namespace App\Modals\Logs\Actions\Traits;


use Config;
use App\Modals\Logs\ProvisionLog;

use Exception;

trait LogTraits {

	/** 
	 * INsert new log data
	 *
	 * @param 
	 * @return object
	 */
	public function insertLogData($request) {
		$returnVal = null;
		try {

         	$query = ProvisionLog::create($request);

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function

	/** 
	 * Get User logs by sapna
	 *
	 * @param 
	 * @return object
	 */
	public function getUserLogs($id)
	{
		$returnVal=null;
		try {

			$query = ProvisionLog::with(['createdBy', 'role'])->where('user_id', $id)->get();

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	}

	


}