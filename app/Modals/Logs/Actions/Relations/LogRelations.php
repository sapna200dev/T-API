<?php 

namespace App\Modals\Logs\Actions\Relations;


use Illuminate\Database\Eloquent\Model;
use App\Modals\Role\Role;

use App\Modals\Loan\Loan;
use App\Modals\Language\Language;
// use App\Modals\User\User;


trait LogRelations 
{

	/**
	 * Get languages
	 */ 
	public function createdBy()
	{
	     // return $this->hasOne(config('common.tables.modals.tbl_documents'), 'id', 'end_user_id');
		return $this->hasOne(config('common.tables.modals.tbl_users'), 'id', 'created_by');
	}

	/**
	 * Get languages
	 */ 
	public function role()
	{
	     // return $this->hasOne(config('common.tables.modals.tbl_documents'), 'id', 'end_user_id');
		return $this->hasOne(config('common.tables.modals.tbl_roles'), 'id', 'role_id');
	}
}

