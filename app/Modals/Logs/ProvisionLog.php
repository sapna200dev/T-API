<?php

namespace App\Modals\Logs;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Tymon\JWTAuth\Contracts\JWTSubject;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use App\Modals\User\Actions\Relations\UserRelatiohShip;
use App\Modals\Logs\Actions\Relations\LogRelations;
use App\Modals\Verification\Actions\Relations\VerificationRelations;

use Laravel\Passport\HasApiTokens;



class ProvisionLog extends Authenticatable
{
    use HasApiTokens, Notifiable, LogRelations;

    protected $table = 'tbl_data_logs';
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['new_data', 'old_data', 'created_by', 'role_id', 'user_id', 'field', 'reason'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


}