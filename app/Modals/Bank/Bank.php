<?php

namespace App\Modals\Bank;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Tymon\JWTAuth\Contracts\JWTSubject;

use App\Modals\Bank\Actions\Relations\BankRelationShip;
use App\Modals\User\Actions\Relations\UserRelatiohShip;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;




class Bank extends Authenticatable
{
    use Notifiable, BankRelationShip, UserRelatiohShip;

    protected $table = 'tbl_banks';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = ['*'];
    protected $fillable = ['bank_name', 'location', 'status', 'billing_status', 'company_email', 'compnay_phone', 'contact_email', 'contact_name', 'address', 'contact_location', 'contact_phone', 'billing_cycle', 'company_phone', 'billing_cycle_from', 'billing_cycle_to', 'confirm_password', 'password', 'admin_username', 'upload_1', 'upload_2', 'upload_3', 'upload_4', 'upload_5', 'created_at', 'created_by', 'updated_at', 'updated_by'];



     /**
     * The attributes that are hidden to UI.
     *
     * @var array
     */
	protected $hidden = [
		'created_by', 'updated_by', 'created_at', 'updated_at'
	];

}
