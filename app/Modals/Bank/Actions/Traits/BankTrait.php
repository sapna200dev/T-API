<?php
namespace App\Modals\Bank\Actions\Traits;

use Hash;
use Log;
use Auth;

use App\Events\SendUserEmail;


use Illuminate\Support\Str;
use App\Modals\Bank\Bank;
use App\Modals\User\User;

use Carbon\Carbon;
use Exception;

trait BankTrait
{

    /**
     * Return all bank lists
     *
     * @param $request
     * @return object
     */
    public function getAll()
    {
        $returnVal = null;
        try{
            $query = Bank::get();

            $returnVal = $query;
        }
        catch(Exception $e){
            $returnVal = null;
            throw new Exception($e->getMessage() , 400);
        }

        return $returnVal;
    }

    /**
     * Return all bank lists
     *
     * @param $request
     * @return object
     */
    public function getBankById($bank_id)
    {
        $returnVal = null;
        try {
            $query = Bank::where('id', $bank_id)->first();

            $returnVal = $query;
        }
        catch(Exception $e){
            $returnVal = null;
            throw new Exception($e->getMessage() , 400);
        }

        return $returnVal;
    }

    /**
     * Return all bank lists
     *
     * @param $request
     * @return object
     */
    public function getBankByIfsc($request, bool $isUpdate = false)
    {
        $returnVal = null;
        try {
            $query = Bank::where('company_email', $request->company_email);
            $query = $query->first();

            $returnVal = $query;
        }
        catch(Exception $e) {
            $returnVal = null;
            throw new Exception($e->getMessage() , 400);
        }

        return $returnVal;
    }


    /**
     * Check login user is validate or not
     *
     * @param $request
     * @return object
     */
    public function createNewBank($request)
    {
        $returnVal = null;
        try {
            // create or update bank
            $query = $this->createOrUpdateBank($request);

            // create bank user
            $user = $this->createBankUser($query, $request);

            $returnVal = $query;
        }
        catch(Exception $e)
        {
            $returnVal = null;
            throw new Exception($e->getMessage() , 400);
        } // End Try-Catch
        return $returnVal;
    } // End function
    

    
    /**
     * Check or create bank
     *
     * @param $request
     * @return object
     */
    private function createOrUpdateBank($request)
    {
        $returnVal = null;
        try {
            // Get uploaded file path
            $filePath = $this->uploadedDocuments($request);

            // Generate payload
            $payload = $this->generatePayload($filePath, $request);

            // Update or create bank
            $query = Bank::updateOrCreate(['bank_name' => $payload['bank_name'], 'company_email' => $payload['company_email']], $payload);

            return $query;
        }
        catch(Exception $e)
        {
            $returnVal = null;
            throw new Exception($e->getMessage() , 400);
        }
    }


    /**
     * Create payload
     *
     * @param $filePath $request
     * @return object
     */
    private function generatePayload($filePath, $request) 
    {
        $returnVal=[]; $data=[];

        try {
            if(isset($filePath['upload_1'])) {
                $data['upload_1'] = $filePath['upload_1'];
            }

            if(isset($filePath['upload_2'])) {
                $data['upload_2'] = $filePath['upload_2'];
            }

            if(isset($filePath['upload_3'])) {
                 $data['upload_3'] = $filePath['upload_3'];
            }

            if(isset($filePath['upload_4'])) {
                $data['upload_4'] = $filePath['upload_4'];
            }

            if(isset($filePath['upload_5'])) {
                $data['upload_5'] = $filePath['upload_5'];
            }

            $data['bank_name'] = $request['bank_name'];
            $data['address'] = $request['address'];
            $data['location'] = $request['location'];
            $data['status'] = $request['status'];
            $data['billing_status'] = $request['billing_status'];
            $data['company_email'] = $request['company_email'];
            $data['contact_email'] = $request['contact_email'];
            $data['company_phone'] = $request['company_phone'];
            $data['contact_name'] = $request['contact_name'];
            $data['contact_phone'] = $request['contact_phone'];
            $data['contact_location'] = $request['contact_location'];
            $data['billing_cycle'] = $request['billing_cycle'];
            $data['billing_cycle_from'] = $request['billing_cycle_from'];
            $data['billing_cycle_to'] = $request['billing_cycle_to'];
            $data['admin_username'] = $request['admin_username'];

            $returnVal=$data;
        } catch (Exception $e) {
            $returnVal = [];
            throw new Exception($e->getMessage() , 400); 
        }

        return $returnVal;
    }


    /**
     * Create payload for uploaded documents
     *
     * @param $request
     * @return object
     */
    private function uploadedDocuments($request)
    {
        $returnVal=[]; $filePath=[];
        try {
            if ($request->file('upload_1')) {
                $imagePath = $request->file('upload_1');
                $imageName = $imagePath->getClientOriginalName();
                $fileName = time().'.'.$imagePath->extension();

                $request->file('upload_1')->move(public_path('/uploads/COI'), $fileName);
                $filePath1 = '/uploads/COI/'.$fileName;

                $filePath['upload_1'] =  $filePath1;
            }

            if ($request->file('upload_2')) {
                $imagePath = $request->file('upload_2');
                $imageName = $imagePath->getClientOriginalName();
                $fileName = time().'.'.$imagePath->extension();

                $request->file('upload_2')->move(public_path('/uploads/GST'), $fileName);
                $filePath1 = '/uploads/GST/'.$fileName;

                $filePath['upload_2'] =  $filePath1;
            }

            if ($request->file('upload_3')) {
                $imagePath = $request->file('upload_3');
                $imageName = $imagePath->getClientOriginalName();
                $fileName = time().'.'.$imagePath->extension();

                $request->file('upload_3')->move(public_path('/uploads/PAN'), $fileName);
                $filePath1 = '/uploads/PAN/'.$fileName;

                $filePath['upload_3'] = $filePath1;
            }

            if ($request->file('upload_4')) {
                $imagePath = $request->file('upload_4');
                $imageName = $imagePath->getClientOriginalName();
                $fileName = time().'.'.$imagePath->extension();

                $request->file('upload_4')->move(public_path('/uploads/Agreement'), $fileName);
                $filePath1 = '/uploads/Agreement/'.$fileName;

                $filePath['upload_4'] = $filePath1;
            }

            if ($request->file('upload_5')) {
                $imagePath = $request->file('upload_5');
                $imageName = $imagePath->getClientOriginalName();
                $fileName = time().'.'.$imagePath->extension();

                $request->file('upload_5')->move(public_path('/uploads/Customer_logo'), $fileName);
                $filePath1 = '/uploads/Customer_logo/'.$fileName;

                $filePath['upload_5'] =  $filePath1;
            }

            $returnVal = $filePath;
        } catch (Exception $e) {
            $returnVal = null;
            throw new Exception($e->getMessage() , 400); 
        }

        return $returnVal;
    }


    /**
     * Create new bank user
     *
     * @param $request
     * @return object
     */
    private function createBankUser($bankObj, $request) 
    {
        $returnVal=null;
        try { 
            $profile_img = '/uploads/profiles/default/default_profile.jpg';

            $query = User::create([
                'name' => $request['bank_name'],
                'email' => $request['admin_username'],
                'username' => $request['admin_username'],
                'password' =>  bcrypt($request['password']),
                'bank_id' =>  $bankObj->id,
                'status' => ($request['status'] == 'active') ? 1 : 0,
                'image' => $profile_img
            ]);

            // Get role by name
            $role = $this->getRoleByName('admin');

            // Create user role
            $this->createUserRole($role->id, $query->id);


            event(new SendUserEmail($request));

            $returnVal = $query;

        } catch (Exception $e) {
            $returnVal = null;
            throw new Exception($e->getMessage() , 400); 
        }

        return $returnVal;
    }

    /**
     * Check login user is validate or not
     *
     * @param $request
     * @return object
     */
    public function updatedBank($id, $request)
    {
        $returnVal = null;
        try {
            // Check duplicate bank
            $isDuplicate = $this->checkDuplicateBank($id, $request);
     
            if ($isDuplicate)
            {
                throw new Exception("Already exist bank", 500);
            }
            else {    
                $updated_by = Auth::user()->id;

                $filePath = $this->uploadedDocuments($request);

                // Generate payload
                $payload = $this->generatePayload($filePath, $request);

                // return $request;
                $query = Bank::where('id', $id)->update($payload);
                
                if(isset($request['password']) && $request['password']!='') {
                    $this->updateUserPassword($request['admin_username'], $request['password']);
                }

                // Update user status
                $this->updateUserStatus($id, $request['admin_username'], $request['status']);

                $returnVal = $query;
            }
            $returnVal = $query;
        }
        catch(Exception $e){
            $returnVal = null;
            throw new Exception($e->getMessage() , 400);
        } // End Try-Catch
        return $returnVal;
    } // End function
    

    /**
     * Check email duplicate
     *
     * @param $id $request
     * @return object
     */
    private function checkDuplicateBank($id, $request)
    {
        $returnVal=null;
        try {
            $query = Bank::where('company_email', $request['company_email']);
            $query = $query->whereNotIn('id', [$request->bank_id]);

            $query = $query->first();

            $returnVal =$query;
        }catch(Exception $e){
            $returnVal = null;
            throw new Exception($e->getMessage() , 400);
        } // End Try-Catch

        return $returnVal;
    }
    
    /**
     * Delete bank record from DB
     *
     * @param $request
     * @return object
     */
    public function deleteBankRecord($id, $request)
    {
        $returnVal = null;
        try {
            // Get exist bank
            $query = $this->getBankById($id);
            if ($query == null) { throw new Exception("Bank record dose not exist", 500); }

            $query1 = Bank::where('id', $id)->update('status', 'inactive');
            // $query1 = $query1->delete();

            // Update user status
            $this->updateUserStatus($id, $request['admin_username'], 'inactive');

            $returnVal = $query;
        }
        catch(Exception $e) {
            $returnVal = null;
            throw new Exception($e->getMessage() , 400);
        } // End Try-Catch
        return $returnVal;
    } // End function
    

    
}

