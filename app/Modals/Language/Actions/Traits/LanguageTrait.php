<?php 
namespace App\Modals\Language\Actions\Traits;

use Hash;
use Log;
use Illuminate\Support\Str;
use App\Modals\Role\Role;
use App\Modals\UserRole;

use Carbon\Carbon;
use Exception;

trait LanguageTrait {

	/**
	 * Check login user is validate or not
	 *
	 * @param $request
	 * @return object
	 */
	public function getLanguageByName($language) {
		$returnVal = null;
		try {
         	$query = config('common.tables.modals.tbl_languages')::updateOrCreate([
         		'language' => $language,
         	]);

         	// $query = config('common.tables.modals.tbl_languages')::where('language', $language)->first();

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function

	/**
	 * Check login user is validate or not
	 *
	 * @param $request
	 * @return object
	 */
	public function getLanguageById($id) {
		$returnVal = null;
		try {
         	$query = config('common.tables.modals.tbl_languages')::where([
         		'id' => $id,
         	])->first();

         	// $query = config('common.tables.modals.tbl_languages')::where('language', $language)->first();

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function

	

}