<?php 
namespace App\Modals\DialogManager\Actions\Traits;

use Hash;
use Log;
use Illuminate\Support\Str;
use App\Modals\Role\Role;
use App\Modals\UserRole;
use Auth;

use Carbon\Carbon;
use Exception;

trait DialogManagerTrait {

	/**
	 * Initiate outbound call by sapna
	 *
	 * @param $from, $to, $audio_url
	 * @return object
	 */
	public function initiateOutboundCall($id, $request)
	{
		$returnVal=null;
		try {
			$from='+91'.$request->from ; $to='+91'.$request->to; $audio_url=null;

			//Generate payload
			$params = $this->generatePayload($from, $to, $audio_url);

            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_FRESH_CONNECT => true,
                CURLOPT_URL => config('common.telynx_config.turant.call.outbound.register.v2.initiate_call'),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($params),
                CURLOPT_HTTPHEADER => config('common.telynx_config.turant.call.headers'),
            ]);

            $response = curl_exec($curl);

  			$response1 = json_decode($response, true);
            if(isset($response1['errors']) && $response1['errors']!= '') {

                $response1['error_msg'] = $response1['errors'][0]['title'];
                unset($response1['errors']);
            } else {
            	$call_id = $response1['data']['call_control_id'];
            	$returnVal = $response1 = $this->handleCallAnswer($call_id);
            }	

       		curl_close($curl);

       		// $res =[];
       		// $res['data'] = 
            $returnVal =  $returnVal;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);	
		}
		return $returnVal;
	}


	/**
	 *  Generate payload
	 *
	 * @param $call_control_id
	 * @return object
	 */
	private function generatePayload($from, $to, $audio_url)
	{
		$returnVal=[];
		try {
            // $api_key = config('common.telynx_config.api_key');
            $params = [
                'connection_id' => config('common.telynx_config.connection_id'),
                'to' => $to, 
                'from' => $from, 
                // 'audio_url' => $audio_url != '' ? $audio_url : config('common.telynx_config.audio_url'),
            ];

            $returnVal =  $params;
		} catch (Exception $e) {
			$returnVal = [];
			throw new Exception($e->getMessage(), 400);	
		}

		return $returnVal;
	}


	/**
	 * Handle call answer
	 *
	 * @param $call_control_id
	 * @return object
	 */
	private function handleCallAnswer($call_control_id)
	{
		$returnVal=null;
		try {
          	$curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_FRESH_CONNECT => true,
                CURLOPT_URL => config('common.telynx_config.turant.call.domain')."v2/calls/$call_control_id/actions/answer",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => [],
                CURLOPT_HTTPHEADER => config('common.telynx_config.turant.call.headers'),
            ]);

            $response = curl_exec($curl);
            curl_close($curl);

            $returnVal = $response;
		} catch (Exception $e) {
			$returnVal = [];
			throw new Exception($e->getMessage(), 400);	
		}

		return $returnVal;
	}

}