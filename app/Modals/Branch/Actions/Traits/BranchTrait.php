<?php 
namespace App\Modals\Branch\Actions\Traits;

use Auth;
use Hash;
use Log;
use Illuminate\Support\Str;
use App\Modals\Branch\Branch;

use Config;
use Carbon\Carbon;
use Exception;

trait BranchTrait {
	/**
	 * Get all branches
	 *
	 * @param $request
	 * @return object
	 */
	public function getBranches() {
		$returnVal = null;
		try {
			$bank_id = Auth::user()->bank_id;

         	$query = Branch::where('bank_id', $bank_id);
         	$query = $query->get();

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function

	/**
	 * Get  branche by id
	 *
	 * @param $request
	 * @return object
	 */
	public function getBrancheById($id) {
		$returnVal = null;
		try {
			$bank_id = Auth::user()->bank_id;

         	$query = Branch::where('id', $id);
         	$query = $query->first();

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function

	/**
	 * Get all branches
	 *
	 * @param $request
	 * @return object
	 */
	public function getBranchByName($name) {
		$returnVal = null;
		try {
			$bank_id = Auth::user()->bank_id;

         	$query = config('common.tables.modals.tbl_branches')::where('branch_name', $name)
         														->where('bank_id', $bank_id)
         														->first();


         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function



	/**
	 * Get all branches
	 *
	 * @param $request
	 * @return object
	 */
	public function createNewBranch($name) {
		$returnVal = null;
		try {
			$bank_id = Auth::user()->bank_id;

         	$query = config('common.tables.modals.tbl_branches')::updateOrCreate([
         		'branch_name' => $name,
         		'bank_id' => $bank_id
         	]);


         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function




}