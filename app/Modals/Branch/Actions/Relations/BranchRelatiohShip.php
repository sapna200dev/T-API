<?php 

namespace App\Modals\Branch\Actions\Relations;


use Illuminate\Database\Eloquent\Model;

use App\Modals\Employee\Employee;



trait BranchRelatiohShip 
{

	/**
	 * Get customer.
	 */
	public function employee()
	{
	    return $this->hasOne(Employee::class, 'branch_name');
	}

}

