<?php

namespace App\Modals\Branch;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Tymon\JWTAuth\Contracts\JWTSubject;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use App\Modals\Branch\Actions\Relations\BranchRelatiohShip;

use Laravel\Passport\HasApiTokens;



class Branch extends Authenticatable
{
    use HasApiTokens, Notifiable, BranchRelatiohShip;

    protected $table = 'tbl_branches';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'bank_id', 'branch_name', 'created_by', 'created_at', 'updated_by', 'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

}
