<?php 

namespace App\Modals\CallMapping\Actions\Traits;

use Log;
use Config;
use Auth;
use Exception;

trait CallMappingTraits {

	/** 
	 * Fetch call mapping
	 *
	 * @param 
	 * @return object
	 */
	public function fetchCallMapping($call_control_id)
	{
		$returnVal=[];
		try {

			$query = config('common.tables.modals.call_mapping')::where('call_control_id', $call_control_id)->first();

			$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = [];
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	}



	/** 
	 * Update call mapping
	 *
	 * @param 
	 * @return object
	 */
	public function updateCallCountByCallControlId($call_control_id, $count)
	{
		$returnVal=[];
		try {

			$query = config('common.tables.modals.call_mapping')::updateOrCreate([
				'call_control_id'=> $call_control_id,
			], [
				'count' => $count
			]);

			// $query = config('common.tables.modals.call_mapping')::where('call_control_id', $call_control_id)->update([
			// 	'count' => $count
			// ]);

			$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = [];
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	}





}