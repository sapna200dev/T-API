<?php

namespace App\Modals\CallMapping;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Tymon\JWTAuth\Contracts\JWTSubject;

use App\Modals\IVRRules\Actions\Relations\IVRRulesRelations;


use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;




class CallMapping extends Authenticatable
{
    use Notifiable, IVRRulesRelations;

    protected $table = 'tbl_call_mapping';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'call_control_id', 'count',
    ];



     /**
     * The attributes that are hidden to UI.
     *
     * @var array
     */
	protected $hidden = [
		'created_at', 'updated_at'
	];

}
