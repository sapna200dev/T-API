<?php 

namespace App\Modals\Loan\Actions\Traits;

use Log;
use Config;

use Exception;

trait LoanTrait {

	/** 
	 * Get all loan types by sapna
	 *
	 * @param 
	 * @return object
	 */
	public function loanTypes() {
		$returnVal = null;
		try {
         	$query = config('common.tables.modals.tbl_loan_types')::get();

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function


	/** 
	 * Get all loan types by sapna
	 *
	 * @param 
	 * @return object
	 */
	public function getloanTypeById($id) {
		$returnVal = null;
		try {
         	$query = config('common.tables.modals.tbl_loan_types')::where('id', $id)->first();

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function

	
	/** 
	 *  get loan type
	 *
	 * @param 
	 * @return object
	 */
	public function getLoanType($loan_type) {
		$returnVal = null;
		try {

 			$query = config('common.tables.modals.tbl_loan_types')::updateOrCreate([
 				'loan_type' => $loan_type
 			]);




// where('loan_type', $loan_type)->first();
         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function

	

	/** 
	 *  create loan
	 *
	 * @param 
	 * @return object
	 */
	public function createLoanType($loan_type) {
		$returnVal = null;
		try {

 			$query = config('common.tables.modals.tbl_loan_types')::create(['loan_type' => $loan_type]);

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function

	




}