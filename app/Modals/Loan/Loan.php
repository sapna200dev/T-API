<?php

namespace App\Modals\Loan;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Tymon\JWTAuth\Contracts\JWTSubject;

use App\Modals\Loan\Actions\Relations\LoanRelation;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;


class Loan extends Authenticatable
{
    use Notifiable, LoanRelation;

    protected $table = 'tbl_loan_types';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'loan_type',
    ];


     /**
     * The attributes that are hidden to UI.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];



}
