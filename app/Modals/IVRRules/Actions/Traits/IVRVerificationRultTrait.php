<?php

namespace App\Modals\IVRRules\Actions\Traits;

use Log;
use Config;
use Auth;
use Exception;

trait IVRVerificationRultTrait
{

    /** 
     * Get all ivr rules
     *
     * @param
     * @return object
     */
    public function getAllIvrVerificationRules()
    {
        $returnVal = [];
        try
        {
            $user_id = Auth::user()->id;

            $query = config('common.tables.modals.tbl_ivr_rules') ::where('created_by', $user_id)->where('is_registration', 1)
                ->get();

            $returnVal = $query;
        }
        catch(Exception $e)
        {
            $returnVal = [];
            throw new Exception($e->getMessage() , 400);
        } // End Try-Catch
        return $returnVal;
    }

    /** 
     * Get all ivr rules
     *
     * @param
     * @return object
     */
    public function getIvrVerificationRuleById($id)
    {
        $returnVal = [];
        try
        {
            $user_id = Auth::user()->id;

            $query = config('common.tables.modals.tbl_ivr_rules') ::with(['recording_rules']);
            $query = $query->where('id', $id);
            $query = $query->where('is_registration', 1);
            $query = $query->where('created_by', $user_id)->first();

            $returnVal = $query;
        }
        catch(Exception $e)
        {
            $returnVal = [];
            throw new Exception($e->getMessage() , 400);
        } // End Try-Catch
        return $returnVal;
    }

    /** 
     * save ivr rules
     *
     * @param
     * @return object
     */
    public function saveIvrVerificationRuleSettings($request)
    {
        $returnVal = null;
        try
        {
            $query = config('common.tables.modals.tbl_ivr_rules') ::create($request);

            $returnVal = $query;
        }
        catch(Exception $e)
        {
            $returnVal = null;
            throw new Exception($e->getMessage() , 400);
        } // End Try-Catch
        return $returnVal;
    } // End function
    

    
    /** 
     * save ivr rules
     *
     * @param
     * @return object
     */
    public function updateIvrVerificationRuleSettings($request, $id)
    {
        $returnVal = null;
        try
        {
            $query = config('common.tables.modals.tbl_ivr_rules') ::where('id', $id);
            $query = $query->update($request);

            $returnVal = $query;
        }
        catch(Exception $e)
        {
            $returnVal = null;
            throw new Exception($e->getMessage() , 400);
        } // End Try-Catch
        return $returnVal;
    } // End function
    

    
    /** 
     * Check set name is exist or not
     *
     * @param
     * @return object
     */
    public function checkIvrRuleSet($request)
    {
        $returnVal = null;
        try
        {
            $bank_id = Auth::user()->id;

            $query = config('common.tables.modals.tbl_ivr_rules') ::where('ivr_rule_set_name', strtolower($request->ivr_rules_set))
                ->where('created_by', $bank_id)->first();

            $returnVal = $query;
        }
        catch(Exception $e)
        {
            $returnVal = null;
            throw new Exception($e->getMessage() , 400);
        } // End Try-Catch
        return $returnVal;
    } // End function
    

    
}

