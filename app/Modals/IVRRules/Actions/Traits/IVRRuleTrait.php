<?php 

namespace App\Modals\IVRRules\Actions\Traits;

use Log;
use Config;
use Auth;
use Exception;

trait IVRRuleTrait {

	/** 
	 * Get all ivr rules
	 *
	 * @param 
	 * @return object
	 */
	public function getAllIvrRules()
	{
		$returnVal=[];
		try {
			$user_id = Auth::user()->id;

			$query = config('common.tables.modals.tbl_ivr_rules')::where('created_by', $user_id)->where('is_registration', 0)->get();

			$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = [];
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	}




	/** 
	 * Get all ivr rules
	 *
	 * @param 
	 * @return object
	 */
	public function getIvrRuleById($id)
	{
		$returnVal=[];
		try {
			$user_id = Auth::user()->id;

			$query = config('common.tables.modals.tbl_ivr_rules')::with(['recording_rules']);
			$query = $query->where('id', $id);
			$query = $query->where('is_registration', 0);
			$query = $query->where('created_by', $user_id)->first();

			$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = [];
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	}


	/** 
	 * save ivr rules
	 *
	 * @param 
	 * @return object
	 */
	public function saveIvrRuleSettings($request) {
		$returnVal = null;
		try {
         	$query = config('common.tables.modals.tbl_ivr_rules')::create($request);

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function


	/** 
	 * save ivr rules
	 *
	 * @param 
	 * @return object
	 */
	public function updateIvrRuleSettings($request, $id) {
		$returnVal = null;
		try {
         	$query = config('common.tables.modals.tbl_ivr_rules')::where('id', $id);
         	$query = $query->update($request);

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function



	/** 
	 * Check set name is exist or not
	 *
	 * @param 
	 * @return object
	 */
	public function checkIvrRuleSet($request) {
		$returnVal = null;
		try {
			$bank_id = Auth::user()->id;

         	$query = config('common.tables.modals.tbl_ivr_rules')::where('ivr_rule_set_name', strtolower($request->ivr_rules_set))->where('created_by', $bank_id)->first();

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function




	/** 
	 * Get play back audio urls
	 *
	 * @param 
	 * @return object
	 */
	public function getPlayAudioUrls($id, $request) {
		$returnVal = null;
		try {
			$bank_id = Auth::user()->id;
			$is_registration = $request->is_registration_call ? 0 : 1;

         	$query = config('common.tables.modals.tbl_ivr_rules')::with(['recording_rules']);
         	$query = $query->where('created_by', $bank_id);
         	$query = $query->where('rule_status', 'activated');
         	$query = $query->where('is_registration', $is_registration)->first();

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function



	/** 
	 * Get play back audio urls
	 *
	 * @param 
	 * @return object
	 */
	public function getIvrRulesForCall($bank_id) {
		$returnVal = null;
		try {
			// $is_registration = $request->is_registration_call ? 0 : 1;

         	$query = config('common.tables.modals.tbl_ivr_rules')::with(['recording_rules']);
         	$query = $query->where('bank_id', $bank_id);
         	$query = $query->where('rule_status', 'activated');
         	$query = $query->first();

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function







}