<?php 

namespace App\Modals\IVRRules\Actions\Relations;

use Illuminate\Database\Eloquent\Model;
use App\Modals\Role\Role;

use App\Modals\Loan\Loan;
use App\Modals\Language\Language;
// use App\Modals\User\User;


trait IVRRulesRelations 
{

	/**
	 * Get languages
	 */ 
	public function recording_rules()
	{
	     return $this->hasMany(config('common.tables.modals.tbl_ivr_rules_recording'), 'ivr_rule_id', 'id');
	}

}

