<?php

namespace App\Modals\IVRRules;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Tymon\JWTAuth\Contracts\JWTSubject;

use App\Modals\IVRRules\Actions\Relations\IVRRulesRelations;


use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;




class IVRRule extends Authenticatable
{
    use Notifiable, IVRRulesRelations;

    protected $table = 'tbl_ivr_rules';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ivr_rule_set_name', 'bank_name', 'total_recordings', 'rule_status', 'activation_date', 'deactivation_date', 'check_audio', 'check_audio_recording', 'am_dm_pass_text', 'am_dm_pass_text_recording', 'am_dm_fail_text', 'am_dm_fail_text_recording', 'created_by', 'is_registration', 'ivr_info', 'welcome_text', 'ivr_info_recording', 'welcome_text_audio_recording', 'created_at', 'updated_by', 'updated_at'
    ];



     /**
     * The attributes that are hidden to UI.
     *
     * @var array
     */
	protected $hidden = [
		'created_at', 'updated_at'
	];

}
