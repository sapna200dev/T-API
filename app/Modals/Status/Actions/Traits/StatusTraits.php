<?php 
namespace App\Modals\Status\Actions\Traits;

use Hash;
use Log;
use App\Modals\Status\Status;

use Carbon\Carbon;
use Exception;

trait StatusTraits {

	/**
	 * Get all statuses
	 *
	 * @param 
	 * @return object
	 */
	public function getAllStatuses() {
		$returnVal = null;
		try {
         	$query = Status::get();

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function






}