<?php

namespace App\Modals\Status;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Tymon\JWTAuth\Contracts\JWTSubject;

use App\Modals\Role\Actions\Relations\RoleRelatiohShip;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;




class Status extends Authenticatable
{
    use Notifiable, RoleRelatiohShip;

    protected $table = 'tbl_statuses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'display_name'
    ];



     /**
     * The attributes that are hidden to UI.
     *
     * @var array
     */
	protected $hidden = [
		'created_at', 'updated_at'
	];

}
