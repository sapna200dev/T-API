<?php 

namespace App\Modals\IVRRulesRecording\Actions\Traits;

use Log;
use Config;
use Auth;
use Storage;
use Exception;

trait IVRRulesRecordingTrait {

	/** 
	 * Get all loan types by sapna
	 *
	 * @param 
	 * @return object
	 */
	public function saveIvrRecordings($rules=null, $bankNmae, $recordings, $request) {
		$returnVal = null;
		try {

			$temp=[];

			 $data = json_decode($recordings);
			if($data) {
				foreach ($data as $key => &$record) {

					 $recording_fields_file = 'recording_fields_file_'.$key;
					$folder_name = config('common.s3.upload.audio.folder');


  					if($request->hasFile($recording_fields_file)) {
	                   $file_1 = $request->file($recording_fields_file);
	                   $folder = config('common.s3.upload.playback_recording');


	                    // validate request files
	                    $isError =  $this->validateRequestedFile($file_1);
	                    if($isError) {
	                        throw new Exception("We only accept mp3 and wav formatted file, please check your file. If you have further questions, please contact Bnak Admin support. Thanks!", 500);
	                    } // End if

	                    $filenamewithextension = $file_1->getClientOriginalName();
	                    $extension = $file_1->getClientOriginalExtension();
	                    $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

	                    // $file_path=$folder_name.'/'.$folder.'/' .$filename.'_'.time().'.'.$extension;
	                    $file_path=$folder_name. $bankNmae . '/' .$folder.'/' .$filename.'_'.time().'.'.$extension;
	                    
	                    $exists = Storage::disk('s3')->exists($file_path);


	                    if($exists) {
	                        $url = Storage::disk('s3')->url($file_path);
	                    } else {
	                        //Upload File to s3
	                        Storage::disk('s3')->put($file_path, fopen($file_1, 'r+'), 'public');

	                        $url = Storage::disk('s3')->url($file_path);
	                    }


	                    $temp['field_2_value'] = $url;
                	} 
 
                	// Create payload
                	$payload = $this->createPayload($rules, $record, $temp, $request);

                	if($request->isUpdate == 'true') {
                		// Update Ivr registration rules
                		$isuploaded = $this->updateIvrRulesRecording($payload, $request, $record->row_index);
                	} else {
                		// Save new ivr registration rules
                		$isuploaded = $this->saveIvrRulesRecording($payload);
                	}	
				}

				return response()->json(['status' => 200, 'msg' => 'uploaded successfully'], 200); 
			}

         	$returnVal = $temp;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function



  
    /**
     * Validate uploaded files
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function validateRequestedFile($file)
    {
        $returnVal=null;
        try {
            $file_mimetypes_type = $file->getClientMimeType();

             $csv_mimetypes = config('common.s3.upload.audio.files_validation');
           
          
            if (!in_array($file_mimetypes_type, $csv_mimetypes))
            {
                throw new TurantCustomException('We only accept mp3 and wav formatted file, please check your file. If you have further questions, please contact Bnak Admin support. Thanks!', 500);
            }

        } catch (Exception $ex) {
            $returnVal=null;
            throw new Exception($ex->getMessage(), 500);
            
            // return $this->handleErrorLogs($ex);
        }

        return $returnVal;
    }

    /** 
     * Create payload
     *
     * @param
     * @return object
     */
    private function createPayload($rules, $data, $url, $request)
    {
        $returnVal = [];
        try
        {
            $bank_id = Auth::user()->bank_id;

            $payload = [];
            $payload['ivr_rule_id'] = $request->isUpdate == 'true' ? $request->id : $rules->id;
            $payload['bank_id'] = $bank_id;
            $payload['recording_text'] = $data->field_1_value;

            $payload['recording_start_beep'] = $data->field_3_value;
            $payload['recording_duration'] = $data->field_4_value;

            if (isset($url['field_2_value']) && $url['field_2_value'] != null)
            {
                $payload['playback_recording'] = $url['field_2_value'];
            }

            $returnVal = $payload;
        }
        catch(Exception $e)
        {
            $returnVal = [];
        }

        return $returnVal;
    }


	/** 
	 * Save Ivr rules recording
	 *
	 * @param 
	 * @return object
	 */
	private function saveIvrRulesRecording($request)
	{
		$returnVal=null;
		try {
         	$query = config('common.tables.modals.tbl_ivr_rules_recording')::create($request);

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal=null;
			throw new Exception($e->getMessage(), 400);
		}

		return $returnVal;
	}

	/** 
	 * Save Ivr rules recording
	 *
	 * @param 
	 * @return object
	 */
	private function updateIvrRulesRecording($payload, $request, $id)
	{
		$returnVal=null;
		try {

            $bank_id = Auth::user()->bank_id;
            // $this->deleteIvrRuleRecording($request->id);
            if (isset($payload['playback_recording']) && $payload['playback_recording'] != null)
            {
                // print_r('file uploaded');
                $query = config('common.tables.modals.tbl_ivr_rules_recording') ::where('id', $id)->where('ivr_rule_id', $request->id)
                    ->where('bank_id', $bank_id)->update(['recording_text' => $payload['recording_text'], 'playback_recording' => $payload['playback_recording'], 'recording_start_beep' => $payload['recording_start_beep'], 'recording_duration' => $payload['recording_duration'],

                ]);
            }
            else
            {

                $query = config('common.tables.modals.tbl_ivr_rules_recording') ::where('id', $id)->where('ivr_rule_id', $request->id)
                    ->where('bank_id', $bank_id)->update(['recording_text' => $payload['recording_text'], 'recording_start_beep' => $payload['recording_start_beep'], 'recording_duration' => $payload['recording_duration'],

                ]);
            }


         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal=null;
			throw new Exception($e->getMessage(), 400);
		}

		return $returnVal;
	}


	/** 
	 * Delete Ivr rules recording
	 *
	 * @param 
	 * @return object
	 */
	private function deleteIvrRuleRecording($request)
	{
		$returnVal=null;
		try {
			$bank_id = Auth::user()->bank_id;

         	$query = config('common.tables.modals.tbl_ivr_rules_recording')::where('ivr_rule_id', $id)->where('bank_id', $bank_id)->delete();

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal=null;
			throw new Exception($e->getMessage(), 400);
		}

		return $returnVal;
	}





}