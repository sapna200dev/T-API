<?php

namespace App\Modals\IVRRulesRecording;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Tymon\JWTAuth\Contracts\JWTSubject;



use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;




class IVRRulesRecording extends Authenticatable
{
    use Notifiable;

    protected $table = 'tbl_ivr_rules_mapping';
        public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ivr_rule_id', 'bank_id', 'recording_text', 'playback_recording', 'recording_start_beep', 'recording_duration',
    ];



     /**
     * The attributes that are hidden to UI.
     *
     * @var array
     */
	protected $hidden = [
		'created_at', 'updated_at'
	];

}
