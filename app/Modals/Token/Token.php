<?php

namespace App\Modals\Token;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Tymon\JWTAuth\Contracts\JWTSubject;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use App\Modals\User\Actions\Relations\UserRelatiohShip;

use Laravel\Passport\HasApiTokens;



class Token extends Authenticatable
{
    use  Notifiable;

    protected $table = 'tbl_tokens';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'email', 'token', 'type', 'created_at', 'updated_at'
    ];

}
