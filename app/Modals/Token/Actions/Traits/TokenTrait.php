<?php
namespace App\Modals\Token\Actions\Traits;

use App\Modals\Token\Token;

use Exception;

trait TokenTrait
{

    /**
     * Save user token
     *
     * @param $request
     * @return object
     */
    public function saveUserToken($request, $token)
    {
        $returnVal = null;
        try
        {
            $query = config('common.tables.modals.tbl_tokens') ::updateOrCreate(['email' => $request->email, 'token' => $token, 'type' => config('common.token.forgot_password_type') ]);

            $returnVal = $query;
        }
        catch(Exception $e)
        {
            $returnVal = null;
            throw new Exception($e->getMessage() , 400);
        } // End Try-Catch
        return $returnVal;
    } // End function
    
    /**
     * Delete existing token with user
     *
     * @param $request, $token
     * @return object
     */
    public function deleteToken($request, $token)
    {
        $returnVal = null;
        try
        {
            $query = config('common.tables.modals.tbl_tokens') ::where(['email' => $request->email, 'type' => config('common.token.forgot_password_type') ])
                ->delete();

            $returnVal = $query;
        }
        catch(Exception $e)
        {
            $returnVal = null;
            throw new Exception($e->getMessage() , 400);
        } // End Try-Catch
        return $returnVal;
    } // End function
    

    
    /**
     * Find user token
     *
     * @param $request
     * @return object
     */
    public function getUserToken($request)
    {
        $returnVal = null;
        try
        {
            $query = config('common.tables.modals.tbl_tokens') ::where(['email' => $request->email, 'token' => $request->token, 'type' => config('common.token.forgot_password_type') ])
                ->first();

            $returnVal = $query;
        }
        catch(Exception $e)
        {
            $returnVal = null;
            throw new Exception($e->getMessage() , 400);
        } // End Try-Catch
        return $returnVal;
    } // End function
    

    
}

