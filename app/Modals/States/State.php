<?php

namespace App\Modals\States;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Tymon\JWTAuth\Contracts\JWTSubject;

use App\Modals\Role\Actions\Relations\RoleRelatiohShip;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;




class State extends Authenticatable
{
    use Notifiable, RoleRelatiohShip;

    protected $table = 'tbl_state';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'bank_id'
    ];



     /**
     * The attributes that are hidden to UI.
     *
     * @var array
     */
	protected $hidden = [
		'created_at', 'updated_at'
	];

}
