<?php 

namespace App\Modals\EndUser\Actions\Relations;


use Illuminate\Database\Eloquent\Model;
use App\Modals\Role\Role;

use App\Modals\Loan\Loan;
use App\Modals\Language\Language;
// use App\Modals\User\User;


trait EndUserRelations 
{

	/**
	 * Get languages
	 */ 
	public function language()
	{
	     return $this->hasOne(config('common.tables.modals.tbl_languages'), 'id', 'preferred_langauge_id');
	}

	/**
	 * Get languages
	 */ 
	public function sales_agents()
	{
	     return $this->hasOne(config('common.tables.modals.tbl_employees'), 'id', 'sale_agent_id');
	}

	/**
	 * Get languages
	 */ 
	public function team_leads()
	{
	     return $this->hasOne(config('common.tables.modals.tbl_employees'), 'id', 'team_leader_id');
	}

	/**
	 * Get languages
	 */ 
	public function loan_type()
	{
	     return $this->hasOne(config('common.tables.modals.tbl_loan_types'), 'id', 'loan_type_id');
	}

	/**
	 * Get languages
	 */ 
	public function branch()
	{
	     return $this->hasOne(config('common.tables.modals.tbl_branches'), 'id', 'branch_id');
	}

	/**
	 * Get languages
	 */ 
	public function verifications()
	{
	     return $this->hasOne(config('common.tables.modals.tbl_verifications'), 'end_user_id', 'id');
	}

	/**
	 * Get languages
	 */ 
	public function logs()
	{
	     return $this->hasMany(config('common.tables.modals.tbl_logs'), 'user_id', 'id');
	}

	/**
	 * Get languages
	 */ 
	public function user()
	{
	     return $this->hasOne(config('common.tables.modals.tbl_banks'), 'id', 'updated_by');
	}

	/**
	 * Get ifsce_code.
	 */
	public function ifsc_user()
	{
		// return $this->hasOne(IfscCode::class, 'user_id');
	}


	/**
	 * Get customer.
	 */
	public function customer()
	{
	    return $this->hasOne(Customer::class, 'created_by', 'id');
	}

}

