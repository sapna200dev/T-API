<?php 
namespace App\Modals\EndUser\Actions\Traits;

use Hash;
use Log;
use Auth;
use App\Modals\Status\Status;

use Carbon\Carbon;
use Exception;

trait EndUserTraits {


	/**
	 * Get all end users
	 *
	 * @param 
	 * @return object
	 */
	public function getEndUsers() {
		$returnVal = null;
		try {
          	$bank_id = Auth::user()->bank_id;
         	// Update or create bank
            $query = config('common.tables.modals.tbl_end_users')::with(['language', 'sales_agents', 'team_leads', 'loan_type'])
            														->where('bank_id', $bank_id)
            														->where('is_active', 1)
            														->get();

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function



	/**
	 * Get all end users
	 *
	 * @param 
	 * @return object
	 */
	public function getEndUserById(int $id) {
		$returnVal = null;
		try {
          	$bank_id = Auth::user()->bank_id;
         	// Update or create bank
            $query = config('common.tables.modals.tbl_end_users')::with(['language', 'sales_agents', 'team_leads', 'loan_type', 'branch'])
														            ->where('id', $id)
														            ->where('bank_id', $bank_id)
														            ->first();

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function

	/**
	 * Get all end users
	 *
	 * @param 
	 * @return object
	 */
	public function getEndUserForCall(int $id) {
		$returnVal = null;
		try {
         	// Update or create bank
            $query = config('common.tables.modals.tbl_end_users')::with(['language', 'sales_agents', 'team_leads', 'loan_type', 'branch'])
														            ->where('id', $id)
														            ->first();

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function




	/**
	 * Get all end users
	 *
	 * @param 
	 * @return object
	 */
	public function getEndUserDetailById(int $id) {
		$returnVal = null;
		try {
          	$bank_id = Auth::user()->bank_id;
         	// Update or create bank
            $query = config('common.tables.modals.tbl_end_users')::with(['language', 'sales_agents', 'team_leads', 'loan_type', 'verifications', 'verifications.user', 'branch'])
														            ->where('id', $id)
														            ->where('bank_id', $bank_id)
														            ->first();

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function




	/**
	 * Get end user by registered umber
	 *
	 * @param 
	 * @return object
	 */
	public function getEndUserByRegisteredMobile($registered_mobile, $salesAgentContact, $teamLeaderContact) {
		$returnVal = null;
		try {

          	// return Auth::user();
            $bank_id = Auth::user()->bank_id;


         	// Update or create bank
            $query = config('common.tables.modals.tbl_end_users')::with(['language']);
            $query = $query->where('registered_mobile', $registered_mobile);
            $query = $query->where('bank_id', $bank_id);
            // $query = $query->where('sale_agent_id', $request->sale_agent_id)->first();
			// $query = $query->where('sales_agent_contact',$salesAgentContact);
			// $query = $query->orWhere('team_leader_contact',$teamLeaderContact);
			$query = $query->first();



         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function


	/**
	 * Add end user
	 *
	 * @param 
	 * @return object
	 */
	public function createEndUser($request) {
		$returnVal = null;
		try {
          
          	$request['created_by'] = Auth::user()->id;
          	$request['bank_id'] = Auth::user()->bank_id;
          	$request['current_status'] ='Provisioned';
          	$request['call_status'] ='Provisioned';
          	$request['call_action'] ='Registration - 1st call';
          	$request['is_active'] =1;
          	
        	// Update or create bank
            $query = config('common.tables.modals.tbl_end_users')::create($request);

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function




	/**
	 * Insert record by bulk upload
	 *
	 * @param 
	 * @return object
	 */
	public function insertBulkRecord($request)
	{
		$returnVal = null;
		try {
          	$request['created_by'] = Auth::user()->id;
          	$request['bank_id'] = Auth::user()->bank_id;

			$query = config('common.tables.modals.tbl_end_users')::updateOrCreate([
         		'registered_mobile' => $request['registered_mobile'],
         		'bank_id' => $request['bank_id']
         	], [
         		'first_name' => $request['first_name'],
         		'middle_name' => $request['middle_name'],
         		'last_name' => $request['last_name'],
         		'loan_account_number' => $request['loan_account_number'],
         		'loan_amount' => $request['loan_amount'],
         		'alternate_phone' => $request['alternate_phone'],
         		'preferred_langauge_id' => $request['preferred_langauge_id'],
         		'loan_type_id' => $request['loan_type_id'],
         		'branch_id' => $request['branch_id'],
         		'sale_agent_id' => $request['sale_agent_id'],
         		'team_leader_id' => $request['team_leader_id'],
         		'title' => $request['title'],
         		'current_status' => $request['current_status'],
         		'call_action' => $request['call_action'],
         		'call_status' => $request['call_status'],
         		'sales_agent_contact' => $request['sales_agent_contact'],
         		'team_leader_contact' => $request['team_leader_contact'],
         		'is_active' => 1,
         	]);


         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			\Log::debug($e);
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	}

	/**
	 * Update end user
	 *
	 * @param 
	 * @return object
	 */
	public function updateEndUser($request, $id) {
		$returnVal = null;
		try {
          
          	$request['updated_by'] = Auth::user()->id;
          	$request['bank_id'] = Auth::user()->bank_id;

          	// Check registered mobile number
          	$userExist = $this->checkDuplicateRegisteredNumber($request, $id);
          	if($userExist) { throw new Exception("Registered mobile number is already exist", 500); } // End if

          	// Insert data into log
          	$this->checkChangedFieldData($id, $request);

         	// Update or create bank
            $query = config('common.tables.modals.tbl_end_users')::where('id', $id)->update($request->all());

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function


	/**
	 * Update end user record
	 *
	 * @param 
	 * @return object
	 */
	public function updateEndUserRecord($request, $id)
	{
		$returnVal = null;
		try {
          
          	 $request['updated_by'] = Auth::user()->id;
          	 $request['bank_id'] = Auth::user()->bank_id;

          	// Check registered mobile number
          	 $userExist = $this->checkDuplicateRegisteredNumber($request, $id);
          	if($userExist) { throw new Exception("Registered mobile number is already exist", 500); } // End if

          	// Insert data into log
          	 $this->checkChangedFieldData($id, $request);
       
         	// Update or create bank
            $query = config('common.tables.modals.tbl_end_users')::where('id', $id)->update($request->all());

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	}



	private function checkChangedFieldData($id, $request)
	{
		$returnVal=[]; $fields=[];
		try {
			// Get old data
			$old_data = $this->getEndUserById($id);
			$new_data = $request->all();
			$user = Auth::user();

			// return is_numeric($old_data['sale_agent_id']);
			// get loan type
			$loan = $this->getloanTypeById($new_data['loan_type_id']);

			// get langauge
			$language = $this->getLanguageById($new_data['preferred_langauge_id']);
			
			// get branches
			$branch = $this->getBrancheById($new_data['branch_id']);
				

			// get end user-
			 $sale_end_user = $this->getEmployeeDetailById($new_data['sale_agent_id']);
			$team_end_user = $this->getEmployeeDetailById($new_data['team_leader_id']);

			$payload=[];
			if($old_data) {

				if($old_data['first_name'] != $new_data['first_name']) {
					$payload[] = $this->generatePayload($id, 'First Name', $old_data['first_name'], $new_data['first_name'], $new_data['reason_for_change'], $user->id, $user->role[0]['id']);
				} // End if

				if($old_data['last_name'] != $new_data['last_name']) {
					$payload[] = $this->generatePayload($id, 'Last Name', $old_data['last_name'], $new_data['last_name'], $new_data['reason_for_change'], $user->id, $user->role[0]['id']);
				} // End if

				if((isset($old_data['middle_name']) && $old_data['middle_name'] )!= $new_data['middle_name']) {
					// return [$old_data, $new_data];
					$payload = $this->generatePayload('Last Name', $old_data['middle_name'], $new_data['middle_name'], 'To Correct Spelling Mistake', $user->id, $user->role[0]['id']);

					$this->insertLogData($payload);
				}


				if($old_data['loan_account_number'] != $new_data['loan_account_number']) {
					$payload[] = $this->generatePayload($id, 'Loan Account Number', $old_data['loan_account_number'], $new_data['loan_account_number'], $new_data['reason_for_change'], $user->id, $user->role[0]['id']);
				} // End if

				if($old_data['loan_amount'] != $new_data['loan_amount']) {
					$payload[] = $this->generatePayload($id, 'Loan Amount', $old_data['loan_amount'], $new_data['loan_amount'], $new_data['reason_for_change'], $user->id, $user->role[0]['id']);
				} // End if

				if($old_data['loan_type_id'] != $new_data['loan_type_id']) {
					// return [$old_data, $new_data];
					$payload[] = $this->generatePayload($id, 'Loan  Type', $old_data->loan_type['loan_type'], $loan['loan_type'], $new_data['reason_for_change'], $user->id, $user->role[0]['id']);

					// $this->insertLogData($payload);
				} // End if

				if($old_data['preferred_langauge_id'] != $new_data['preferred_langauge_id']) {
					$payload[] = $this->generatePayload($id, 'Preferred Language', $old_data->language['language'], $language['language'], $new_data['reason_for_change'], $user->id, $user->role[0]['id']);
				} // End if

				if($old_data['registered_mobile'] != $new_data['registered_mobile']) {
					$payload[] = $this->generatePayload($id, 'Registered Mobile', $old_data['registered_mobile'], $new_data['registered_mobile'], $new_data['reason_for_change'], $user->id, $user->role[0]['id']);
				} // End if

				if($old_data['loan_account_number'] != $new_data['loan_account_number']) {
					$payload[] = $this->generatePayload($id, 'loan Account Number', $old_data['loan_account_number'], $new_data['loan_account_number'], $new_data['reason_for_change'], $user->id, $user->role[0]['id']);
				} // End if

				if($old_data['alternate_phone'] != $new_data['alternate_phone']) {
					$payload[] = $this->generatePayload($id, 'Alternate Phone', $old_data['alternate_phone'], $new_data['alternate_phone'], $new_data['reason_for_change'], $user->id, $user->role[0]['id']);
				} // End if

				if($old_data['branch_id'] != $new_data['branch_id']) {
					$payload[] = $this->generatePayload($id, 'Branch Name', $old_data->branch['branch_name'], $branch['branch_name'], $new_data['reason_for_change'], $user->id, $user->role[0]['id']);

				} // End if

				if($new_data['sale_agent_id']  != $old_data['sale_agent_id'] ) {
					$old_sale_agent_name = $old_data->sales_agents['first_name'] .''. $old_data->sales_agents['last_name'];
					$new_sale_agent_name = $sale_end_user['first_name'] .' '. $sale_end_user['last_name'];

					$payload[] = $this->generatePayload($id, 'Sale Agent', $old_sale_agent_name, $new_sale_agent_name, $new_data['reason_for_change'], $user->id, $user->role[0]['id']);
				} // End if

				if($old_data['team_leader_id'] != $new_data['team_leader_id']) {
					$old_agent_name = $old_data->team_leads['first_name'] .' '. $old_data->team_leads['last_name'];
					$new_agent_name = $team_end_user['first_name'] .' '. $team_end_user['last_name'];

					$payload[] = $this->generatePayload($id, 'Team Leader', $old_agent_name, $new_agent_name,  $new_data['reason_for_change'],$user->id, $user->role[0]['id']);
				} // End if

				if($old_data['sales_agent_contact'] != $new_data['sales_agent_contact']) {
					// return [$old_data, $new_data];
					$payload[] = $this->generatePayload($id, 'Sales Agent Contact', $old_data['sales_agent_contact'], $new_data['sales_agent_contact'], $new_data['reason_for_change'], $user->id, $user->role[0]['id']);
				} // End if

				if($old_data['team_leader_contact'] != $new_data['team_leader_contact']) {
					// return [$old_data, $new_data];
					$payload[] = $this->generatePayload($id, 'Team Leader Contact', $old_data['team_leader_contact'], $new_data['team_leader_contact'], $new_data['reason_for_change'], $user->id, $user->role[0]['id']);

				} // End if


				foreach ($payload as $key => $value) {
					// Insert date into logs
					$this->insertLogData($value);
				} // End loop
			} // End if

			$returnVal='Data inserted';

		} catch (Exception $e) {
			$returnVal = [];
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	}


	/**
	 * Generate log payload
	 *
	 * @param 
	 * @return object
	 */
	private function generatePayload($id, $field, $old_data, $new_data, $reason, $user_id, $role_id)
	{
		$returnVal=[];
		try {
			$logPayload = [];
			$logPayload['field'] = $field;
			$logPayload['old_data'] = $old_data;
			$logPayload['new_data'] = $new_data;
			$logPayload['reason'] = $reason;
			$logPayload['user_id'] = $id;
			$logPayload['role_id'] = $role_id;
			$logPayload['created_by'] = $user_id;
			$logPayload['created_at'] = Carbon::now();

			$returnVal=$logPayload;
			
		} catch (Exception $e) {
			$returnVal = [];
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	}


	/**
	 * Validate user
	 *
	 * @param 
	 * @return object
	 */
	private function checkDuplicateRegisteredNumber($request, $id)
	{
		$returnVal = null;
		try {
			$bank_id = Auth::user()->bank_id;

			$query = config('common.tables.modals.tbl_end_users')::where('bank_id', $bank_id);
			$query = $query->where('registered_mobile', $request->registered_mobile);
			$query = $query->whereNotIn('id', [$id])->first();		

			$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function


	/**
	 * Validate user
	 *
	 * @param 
	 * @return object
	 */
	private function deleteEndUserById($request, $id)
	{
		$returnVal = null;
		try {
			$bank_id = Auth::user()->bank_id;

			$query = config('common.tables.modals.tbl_end_users')::where('bank_id', $bank_id);
			$query = $query->where('id', $id);
			$query = $query->update(['is_active'=> 0]);		

			$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function



	/**
	 * Check end user
	 *
	 * @param 
	 * @return object
	 */
	public function checkEURExists($regdMobileNumber, $salesAgentContact, $teamLeaderContact)
	{
		$returnVal=null;
		try {
			$query = config('common.tables.modals.tbl_documents')::where('registered_mobile', $regdMobileNumber);
			$query = $query->where('sales_agent_contact',$salesAgentContact);
			$query = $query->orWhere('team_leader_contact',$teamLeaderContact);
			$query = $query->first();

			return $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	}



	/**
	 * Update user call status
	 *
	 * @param $user, $value
	 * @return object
	 */
	public function updateEndUserStatus($user, $current_status, $call_status, $call_action)
	{
		$returnVal=null;
		try {
			$query = config('common.tables.modals.tbl_end_users')::where('id', $user['id']);
			$query = $query->where('bank_id', $user['bank_id']);
			$query = $query->update([
				'current_status' => $current_status,
				'call_status' => $call_status,
				'call_action' => $call_action,
			]);

			return $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	}


}