<?php

namespace App\Modals\EndUser;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Tymon\JWTAuth\Contracts\JWTSubject;

use App\Modals\EndUser\Actions\Relations\EndUserRelations;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;




class EndUser extends Authenticatable
{
    use Notifiable, EndUserRelations;

    protected $table = 'tbl_end_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'middle_name' ,'first_name', 'last_name', 'registered_mobile', 'loan_account_number', 'loan_amount', 'alternate_phone', 'preferred_langauge_id', 'loan_type_id', 'branch_id', 'sale_agent_id', 'team_leader_id', 'team_leader_contact', 'sales_agent_contact', 'call_status', 'call_action', 'registeration_trigger', 'current_status', 'verification_trigger', 'created_by', 'updated_by', 'created_at', 'updated_at', 'is_active', 'bank_id', 'reason_for_change',
    ];



     /**
     * The attributes that are hidden to UI.
     *
     * @var array
     */
	protected $hidden = [
		 'updated_at', 'updated_by', 'created_by'
	];

}
