<?php 

namespace App\Modals\Role\Actions\Relations;

use Illuminate\Database\Eloquent\Model;
use App\Modals\User\User;


trait RoleRelatiohShip 
{

	/**
	 * Get roles of user.
	 */
	public function user()
	{
	    return $this->belongsTo(User::class);
	}


	
}
 