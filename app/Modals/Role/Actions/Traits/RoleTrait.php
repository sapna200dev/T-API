<?php 
namespace App\Modals\Role\Actions\Traits;

use Hash;
use Log;
use Illuminate\Support\Str;
use App\Modals\Role\Role;
use App\Modals\UserRole;

use Carbon\Carbon;
use Exception;

trait RoleTrait {

	/**
	 * Check login user is validate or not
	 *
	 * @param $request
	 * @return object
	 */
	public function getRoles() {
		$returnVal = null;
		try {
         	$query = Role::get();

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function

	/**
	 * Get role by name
	 *
	 * @param $request
	 * @return object
	 */
	public function getRoleByName($name) {
		$returnVal = null;
		try {
         	$query = Role::where('name', $name)->first();

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function


	/**
	 * Check login user is validate or not
	 *
	 * @param $request
	 * @return object
	 */
	public function createUserRole($role_id, $user_id) {
		$returnVal = null;
		try {
         	$query = UserRole::create([
         		'role_id' =>  $role_id,
         		'user_id' => $user_id,
         	]);

         	$returnVal = $query;
		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	} // End function





}