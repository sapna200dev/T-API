<?php

namespace App\Modals;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Tymon\JWTAuth\Contracts\JWTSubject;

use App\Modals\Role\Actions\Relations\RoleRelatiohShip;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;




class UserRole extends Authenticatable
{
    use Notifiable;

    protected $table = 'tbl_user_roles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id', 'user_id'
    ];



}
