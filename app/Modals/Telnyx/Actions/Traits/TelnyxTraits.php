<?php 
namespace App\Modals\Telnyx\Actions\Traits;

use Hash;
use Log;
use App\Modals\Status\Status;

use Carbon\Carbon;
use Exception;

trait TelnyxTraits {

    /**
     * Get call speak api by sapna
     *
     * @param request
     * @return object
     */
    public function callAudioPlayAPI($request, $call_record, $audio_url)
    {
        $returnVal=null;
        try {
            log::debug('callAudioPlayAPI request = ');
                
            $params = [
                'audio_url' =>$audio_url,
                // 'loop' => 1, 
                // "overlay" => true,
                // // 'stop' => 'current',
                // 'target_legs' => 'self'
            ];

            $url = 'https://api.telnyx.com/v2/calls/'.$request['call_control_id'].'/actions/playback_start';

            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_FRESH_CONNECT => true,
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($params),
                CURLOPT_HTTPHEADER => [
                    "accept: application/json",
                    "content-type: application/json",
                    "Authorization: Bearer KEY0174B8924CE49DEB7AD778DD6AF64E0D_3VdCyVZ793N3YQcpHega01",
                ],
            ]);

            $response = curl_exec($curl);

            $response = json_decode($response, true);
            if(isset($response['errors']) && $response['errors']!= '') {

                $response['error_msg'] = $response['errors'][0]['title'];
                unset($response['errors']);
            } 

            curl_close($curl);


            log::debug('callAudioPlayAPI call response = ' );
            // log::debug($response);
            return $returnVal =  $response;
        } catch (Exception $e) {
            $returnVal=[];
            throw new Exception($e->getMessage(), 500);
        }
    }



    /**
     * Get call speak api by sapna
     *
     * @param request
     * @return object
     */
    public function callStopAudioPlayAPI($request)
    {
        $returnVal=null;
        try {
            log::debug('callStopAudioPlayAPI request = ');
            log::debug($request['call_control_id']);
            $params=[];

            $url = 'https://api.telnyx.com/v2/calls/'.$request['call_control_id'].'/actions/playback_stop';

            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_FRESH_CONNECT => true,
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($params),
                CURLOPT_HTTPHEADER => [
                    "accept: application/json",
                    "content-type: application/json",
                    "Authorization: Bearer KEY0174B8924CE49DEB7AD778DD6AF64E0D_3VdCyVZ793N3YQcpHega01",
                ],
            ]);

            $response = curl_exec($curl);

            $response = json_decode($response, true);
            if(isset($response['errors']) && $response['errors']!= '') {

                $response['error_msg'] = $response['errors'][0]['title'];
                unset($response['errors']);
            } 
            curl_close($curl);


            log::debug('callStopAudioPlayAPI call response = ' );
            log::debug($response);
            return $returnVal =  $response;
        } catch (Exception $e) {
            $returnVal=[];
            throw new Exception($e->getMessage(), 500);
        }
    }



	/**
	 * Get call speak api by sapna
	 *
	 * @param request
	 * @return object
	 */
    public function callSpeakAPI($request, $call_record)
    {
        $returnVal=null;
        try {
                
            $params = [
                'payload' => $call_record->welcome_text,
                'voice' => 'male',
                'language' => 'en-US'
            ];

            $url = 'https://api.telnyx.com/v2/calls/'.$request['call_control_id'].'/actions/speak';

            log::debug('call params = ', $params);
            log::debug('call request = ');
            log::debug( $request['call_control_id']);
           

            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_FRESH_CONNECT => true,
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($params),
                CURLOPT_HTTPHEADER => [
                    "accept: application/json",
                    "content-type: application/json",
                    "Authorization: Bearer KEY0174B8924CE49DEB7AD778DD6AF64E0D_3VdCyVZ793N3YQcpHega01",
                ],
            ]);

            $response = curl_exec($curl);

            $response = json_decode($response, true);
            if(isset($response['errors']) && $response['errors']!= '') {

                $response['error_msg'] = $response['errors'][0]['title'];
                unset($response['errors']);
            } else {
                $call_id = $response['data']['call_control_id'];
            }   

            curl_close($curl);


            log::debug('speak call response = ' );
            log::debug($response);
            return $returnVal =  $response;


        } catch (Exception $e) {
            $returnVal=[];
            throw new Exception($e->getMessage(), 500);
        }
    }


	/**
	 * Get call recording api by sapna
	 *
	 * @param request
	 * @return object
	 */
    public function callStartRecordApi($request)
    {
        $returnVal=null;
        try {
            log::debug('callStartRecordApi Fired= ' );

            $params = [
                'format' => 'mp3',
                'channels' => 'single',
            ];

            $url = 'https://api.telnyx.com/v2/calls/'.$request['call_control_id'].'/actions/record_start';

            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_FRESH_CONNECT => true,
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($params),
                CURLOPT_HTTPHEADER => [
                    "accept: application/json",
                    "content-type: application/json",
                    "Authorization: Bearer KEY0174B8924CE49DEB7AD778DD6AF64E0D_3VdCyVZ793N3YQcpHega01",
                ],
            ]);

            $response = curl_exec($curl);

            $response = json_decode($response, true);
            if(isset($response['errors']) && $response['errors']!= '') {

                $response['error_msg'] = $response['errors'][0]['title'];
                unset($response['errors']);
            } 
            curl_close($curl);


            log::debug('callStartRecordApi call response = ' );
           return $returnVal =  $response;
        } catch (Exception $e) {
            $returnVal=[];
            throw new Exception($e->getMessage(), 500);
        }
    }



    /**
     * Get call recording api by sapna
     *
     * @param request
     * @return object
     */
    public function callStopRecordApi($request)
    {
        $returnVal=null;
        try {
             log::debug('callStopRecordApi = ' );
            // log::debug($request);

            $params = [
                'format' => 'mp3',
                'channels' => 'single',
            ];

            $url = 'https://api.telnyx.com/v2/calls/'.$request['call_control_id'].'/actions/record_stop';

           // log::debug('call record_stop params = ', $params);          

            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_FRESH_CONNECT => true,
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($params),
                CURLOPT_HTTPHEADER => [
                    "accept: application/json",
                    "content-type: application/json",
                    "Authorization: Bearer KEY0174B8924CE49DEB7AD778DD6AF64E0D_3VdCyVZ793N3YQcpHega01",
                ],
            ]);

            $response = curl_exec($curl);

            $response = json_decode($response, true);
            if(isset($response['errors']) && $response['errors']!= '') {

                $response['error_msg'] = $response['errors'][0]['title'];
                unset($response['errors']);
            } 

            curl_close($curl);


            log::debug('callStopRecordApi call  response = ' );
            // log::debug($response);
            return $returnVal =  $response;
        } catch (Exception $e) {
            $returnVal=[];
            throw new Exception($e->getMessage(), 500);
        }
    }

}