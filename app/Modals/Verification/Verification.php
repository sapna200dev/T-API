<?php

namespace App\Modals\Verification;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Tymon\JWTAuth\Contracts\JWTSubject;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use App\Modals\User\Actions\Relations\UserRelatiohShip;
use App\Modals\Verification\Actions\Relations\VerificationRelations;

use Laravel\Passport\HasApiTokens;



class Verification extends Authenticatable
{
    use HasApiTokens, Notifiable, UserRelatiohShip, VerificationRelations;

    protected $table = 'tbl_verifications';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = array('*');

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
