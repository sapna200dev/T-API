<?php 
namespace App\Modals\Verification\Actions\Traits;

use Hash;
use Log;
use Auth;
use App\Modals\Status\Status;

use Carbon\Carbon;
use Exception;

trait VerificationTraits {


	public function getUserByNumber($number)
	{
		$returnVal=null;
		try {
			$query = config('common.tables.modals.tbl_end_users')::where('registered_mobile', $number)->orWhere('alternate_phone', $number)->first();

			$returnVal =  $query;
		} catch (Exception $e) {
			$returnVal=null;
			throw new Exception($e->getMessage(), 500);
		}

		return $returnVal;
	}



	public function getRecordByEmployeeNumber($number)
	{
		$returnVal=null;
		try {
			// $bank_id = Auth::user()->bank_id;

			$query = config('common.tables.modals.tbl_employees')::where('mobile_number', $number)->first();
			// 

			return $query;
		} catch (Exception $e) {
			$returnVal=null;
			throw new Exception($e->getMessage(), 500);
		}

		return $returnVal;
	}


	public function getEmployeeBankIdByCallControleId($call_control_id)
	{
		$returnVal=null;
		try {
			$query = config('common.tables.modals.tbl_verifications')::where('call_control_id', $call_control_id)->first();
			
			return $query;
		} catch (Exception $e) {
			$returnVal=null;
			throw new Exception($e->getMessage(), 500);
		}

		return $returnVal;
	}



	/**
	 * Get call record from db
	 *
	 * @param 
	 * @return object
	 */
	public function getRecordByCallControlId($request)
	{
		$returnVal=null;
		try {
			$query =config('common.tables.modals.tbl_verifications')::where('end_user_id', $id);
			$query = $query->where('call_control_id', $request['call_control_id']);
			$query = $query->first();

			$returnVal = $query;
		} catch (Exception $e) {
			$returnVal=null;
			throw new Exception($e->getMessage(), 500);
		}

		return $returnVal;
	}


	/**
	 * Check call record
	 *
	 * @param 
	 * @return object
	 */
	public function checkCallVerifyRecord($user, $request)
	{
		$returnVal=null;
		try {
			$query = config('common.tables.modals.tbl_verifications')::where('end_user_id', $user->id);
			$query = $query->where('bank_id', $user->bank_id);
			$query = $query->where('call_control_id', $request['call_control_id'])->first();

			$returnVal = $query;
		} catch (Exception $e) {
			$returnVal=null;
			throw new Exception($e->getMessage(), 500);
		}

		return $returnVal;
	}



	public function updateUserRecord($verify_call_record, $request)
	{
		$returnVal=null;
		try {
			$query =  config('common.tables.modals.tbl_verifications')::where('call_control_id', $verify_call_record->call_control_id)->first();

			if((isset($query['registration_1_call_status']) && $query['registration_1_call_status'] == null) ) {
				$this->updateCallStatus('registration_1_call_status', $request);
			
			}  else if(isset($query['registration_2_call_status']) && $query['registration_2_call_status'] == null) {
				$this->updateCallStatus('registration_2_call_status', $request);

			} else if(isset($query['registration_3_call_status']) && ($query['registration_3_call_status'] == null || $query['registration_3_call_status'] =='') ) {
				$this->updateCallStatus('registration_3_call_status', $request);
			}

			
			return $query;
			$returnVal = $query;
		} catch (Exception $e) {
			$returnVal=null;
			throw new Exception($e->getMessage(), 500);
		}

		return $returnVal;
	}


	/**
	 * Insert first call record
	 *
	 * @param 
	 * @return object
	 */
	public function insertFirstRegisterCall($call_record, $request)
	{
		$returnVal=null;
		try {
			$payload = $this->generateCallRegisteredPayload($call_record, $request);
			log::debug('insert call payload = ');
			// log::debug($payload);

			$query = config('common.tables.modals.tbl_verifications')::insert($payload);

			$returnVal = $query;
		} catch (Exception $e) {
			$returnVal=null;
			throw new Exception($e->getMessage(), 500);
		}

		return $returnVal;
	}



	/**
	 * Genrate call record payload
	 *
	 * @param 
	 * @return object
	 */
	private function generateCallRegisteredPayload($call_record, $request)
	{
		$returnVal=[];
		try {
			log::debug('insert call generateCallRegisteredPayload  = ');
				// log::debug(json_encode($call_record, JSON_PRETTY_PRINT));
			log::debug('insert call request = ');
			// log::debug(json_encode($request, JSON_PRETTY_PRINT));
			$payload=[];
			$payload['registration_1_call_date']=Carbon::now($request['created_at']);
			$payload['registration_1_call_to']= isset($request['to']) ? $request['to'] : null;
			$payload['registration_1_call_status']='initiated';
			$payload['registration_1_call_duration']=null;
			$payload['registration_2_call_date']=null;
			$payload['registration_2_call_to']=null;
			$payload['registration_2_call_status']=null;
			$payload['registration_2_call_duration']=null;
			$payload['registration_3_call_date']=null;
			$payload['registration_3_call_to']=null;
			$payload['registration_3_call_status']=null;
			$payload['registration_3_call_duration']=null;
			$payload['final_registration_status']=null;
			$payload['verification_1_call_to']=null;
			$payload['verification_1_call_status']=null;
			$payload['verification_1_call_duration']=null;
			$payload['verification_2_call_date']=null;
			$payload['verification_2_call_to']=null;
			$payload['verification_2_call_status']=null;
			$payload['verification_2_call_duration']=null;
			$payload['verification_3_call_date']=null;
			$payload['verification_3_call_to']=null;
			$payload['verification_3_call_status']=null;
			$payload['verification_3_call_duration']=null;
			$payload['final_verification_status']='';
			$payload['created_by']=$call_record->sale_agent_id;
			$payload['created_at']=Carbon::now();
			$payload['end_user_id']=$call_record->id;
			$payload['bank_id']=$call_record->bank_id;
			$payload['call_control_id']=$request['call_control_id'];

			$returnVal = $payload;
		} catch (Exception $e) {
			$returnVal=[];
			throw new Exception($e->getMessage(), 500);
		}

		return $returnVal;
	}


	/**
	 * Insert user registered call
	 *
	 * @param 
	 * @return object
	 */
	public function insertUserRegisteredCall($request)
	{
		$returnVal=[];
		try {
			// $bank_id = Auth::user()->bank_id; 

			$record = $this->getRecordByCallControlId($request);
			// $query = config('common.tables.modals.tbl_verifications')::

			$returnVal = $record;
		} catch (Exception $e) {
			$returnVal=[];
			throw new Exception($e->getMessage(), 500);
		}

		return $returnVal;
	}




	public function updateCallStatus( $call_type, $request)
	{
		$returnVal=null;
		try {			
			if($call_type == 'registration_1_call_status') {
				$query =  config('common.tables.modals.tbl_verifications')::where('call_control_id', $request['call_control_id']);
				$query = $query->update([
					'registration_1_call_date' => Carbon::parse($request['call_date'])->format('Y-m-d H:i:s'),
					'registration_1_call_to' => $request['to'],
					'registration_1_call_status' =>  'initiated',
					'registration_1_call_duration' => 0,
				]);

			} else if ($call_type == 'registration_2_call_status') {
				$query =  config('common.tables.modals.tbl_verifications')::where('call_control_id', $request['call_control_id']);
				$query = $query->update([
					'registration_2_call_date' =>   Carbon::parse($request['call_date'])->format('Y-m-d H:i:s'),
					'registration_2_call_to' => $request['to'],
					'registration_2_call_status' => 'initiated',
					'registration_2_call_duration' => 0,
				]);
	
			} else if ($call_type == 'registration_3_call_status') {
				$query =  config('common.tables.modals.tbl_verifications')::where('call_control_id', $request['call_control_id']);
				$query = $query->update([
					'registration_3_call_date' =>  Carbon::parse($request['call_date'])->format('Y-m-d H:i:s'),
					'registration_3_call_to' => $request['to'],
					'registration_3_call_status' =>  'initiated',
					'registration_3_call_duration' => 0,
				]);
			}
	
			


			$returnVal = $query;
		} catch (Exception $e) {
			$returnVal=[];
			throw new Exception($e->getMessage(), 500);
		}

		// return $returnVal;
	}




	/**
	 * Verify end user call status
	 *
	 * @param 
	 * @return object
	 */
	public function checkUserRegistrationStatus($user)
	{	
		$returnVal=null; 
		try {

			 $verifications = config('common.tables.modals.tbl_verifications')::where('end_user_id', $user['id'])->where('bank_id', $user['bank_id'])->first();

			if($verifications) {
				if(isset($verifications['registration_1_call_status']) && $verifications['registration_1_call_status']!=null) {
					if($verifications['registration_1_call_status'] == 'pass') {
						$this->updateEndUserStatus($user, 'Registration - 1st call', 'Success', 'Registration Successfully Completed'); 
					} else {
						$this->updateEndUserStatus($user, 'Registration - 1st call', 'Failed', 'Registration - 2nd call'); 
					} //End if-else		
				}


				if(isset($verifications['registration_2_call_status']) && $verifications['registration_2_call_status']!=null ) {
					if($verifications['registration_2_call_status'] == 'pass') {
						$this->updateEndUserStatus($user, 'Registration - 2nd call', 'Success', 'Registration Successfully Completed'); 
					} else {
						$this->updateEndUserStatus($user, 'Registration - 2nd call', 'Failed', 'Registration - 3rd call'); 
					} //End if-else
				}


				if(isset($verifications['registration_3_call_status']) && $verifications['registration_3_call_status']!=null ) {
					if($verifications['registration_3_call_status'] == 'pass') {
						$this->updateEndUserStatus($user, 'Registration - 3rd call', 'Success', 'Registration Successfully Completed'); 
					} else {
						$this->updateEndUserStatus($user, 'Registration - 3rd call', 'Failed', 'Registration Failed'); 
					} //End if-else
				}


				if(isset($verifications['verification_1_call_status']) && $verifications['verification_1_call_status']!=null ) {
					if($verifications['verification_1_call_status'] == 'pass') {
						$this->updateEndUserStatus($user, 'Verification - 1st call', 'Success', 'Verification Successfully Completed'); 
					} else {
						$this->updateEndUserStatus($user, 'Verification - 1st call', 'Failed', 'Verification - 2nd call'); 
					}
				} //End if


				if(isset($verifications['verification_2_call_status']) && $verifications['verification_2_call_status']!=null) {
					if($verifications['verification_2_call_status'] == 'pass') {
						$this->updateEndUserStatus($user, 'Verification - 2nd call', 'Success', 'Verification Successfully Completed'); 
					} else {
						$this->updateEndUserStatus($user, 'Verification - 2nd call', 'Failed', 'Verification - 3rd call'); 
					}
				} // End if



				if(isset($verifications['verification_3_call_status']) && $verifications['verification_3_call_status']!=null) {
					if($verifications['verification_3_call_status'] == 'pass') {
						$this->updateEndUserStatus($user, 'Verification - 3rd call', 'Success', 'Verification Successfully Completed'); 
					} else {
						$this->updateEndUserStatus($user, 'Verification - 3rd call', 'Failed', 'Verification Failed'); 
					}
				} // End if

				// $first_call_status = $verifications['registration_1_call_status'] == 'failed' ? $isRegisterFirstCall=false : $isRegisterFirstCall = true;
				// if(!$first_call_status) { $this->updateEndUserStatus($user, 'Registration Success'); }
				
				// $second_call_status = $verifications['registration_2_call_status'] == 'failed' ? $isRegisterSecondCall=false : $isRegisterSecondCall = true;	
				// if($second_call_status) { $this->updateEndUserStatus($user, 'Registration Success'); }

				// $third_call_status = $verifications['registration_3_call_status'] == 'failed' ? $isRegisterThirdCall=false : $isRegisterThirdCall = true;	
				// if($third_call_status) { $this->updateEndUserStatus($user, 'Registration Success'); }


				$returnVal = $verifications;
			}

		} catch (Exception $e) {
			$returnVal = null;
			throw new Exception($e->getMessage(), 400);		
		} // End Try-Catch

		return $returnVal;
	}




}