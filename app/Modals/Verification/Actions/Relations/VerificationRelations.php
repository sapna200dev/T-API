<?php 

namespace App\Modals\Verification\Actions\Relations;


use Illuminate\Database\Eloquent\Model;
use App\Modals\Role\Role;

use App\Modals\Loan\Loan;
use App\Modals\Language\Language;
// use App\Modals\User\User;


trait VerificationRelations 
{

	/**
	 * Get languages
	 */ 
	public function document()
	{
	     // return $this->hasOne(config('common.tables.modals.tbl_documents'), 'id', 'end_user_id');
		return $this->hasOne(config('common.tables.modals.tbl_documents'), 'id', 'end_user_id');
	}

	// /**
	//  * Get languages
	//  */ 
	// public function bank()
	// {
	//      return $this->hasOne(config('common.tables.modals.tbl_banks'), 'id', 'bank_id');
	// }

	/**
	 * Get languages
	 */ 
	public function user()
	{
	     return $this->hasOne(config('common.tables.modals.tbl_banks'), 'id', 'updated_by');
	}


}

