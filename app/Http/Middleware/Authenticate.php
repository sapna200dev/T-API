<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    // protected function redirectTo($request)
    // {
    //    if (!$request->expectsJson())
    //     {
    //          // return route('login');
    //         return response()->json(['status' => 401, 'msg' => 'Unauthenticated.']);
    //     }
    // }

    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    public function handle(Request $request, Closure $next)
    {
        // return !$request->user();
        if( !$request->header('Authorization') || !$request->user()) {
            return response()->json(['status' => 401, 'msg' => 'Unauthorized, Please login with valid token !.']);
        }

        return $next($request);
    }
}
