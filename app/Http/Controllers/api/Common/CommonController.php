<?php
namespace App\Http\Controllers\Api\Common;

use App\Http\Controllers\Controller;

use Auth;
use Illuminate\Http\Request;
use Log;

// Custom Tarits
use App\Modals\ErrorLogTrait;
use App\Modals\Loan\Actions\Traits\LoanTrait;


use Exception;
use Illuminate\Auth\AuthenticationException;
use App\Exceptions\TurantCustomException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CommonController extends Controller
{
    use ErrorLogTrait;


    /**
     * Get all sales agent
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSalesAgent(Request $request)
    {
        try {
            
            $bank_id = Auth::user()->bank_id;
            // Create customer for bank
            $loans_types = config('common.tables.modals.tbl_employees')::where('bank_id', $bank_id)
                                                                        ->where('role_id', config('common.values.sale_agent.role_id'))
                                                                        ->get();
            
            return response()->json(['status' => 200, 'data' => $loans_types], 200);            
        } catch (Exception $ex) {
            return $this->handleErrorLogs($ex);
        }
    }

  
    /**
     * Get all team leads
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTeamLead(Request $request)
    {
        try {
              $bank_id = Auth::user()->bank_id;
            // Create customer for bank
            $loans_types = config('common.tables.modals.tbl_employees')::where('bank_id', $bank_id)
                                                                        ->where('role_id', config('common.values.team_lead.role_id'))
                                                                        ->get();
            
            return response()->json(['status' => 200, 'data' => $loans_types], 200);            
        } catch (Exception $ex) {
            return $this->handleErrorLogs($ex);
        }
    }

    /**
     * Get all State
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStates(Request $request)
    {
        try {
            $bank_id = Auth::user()->bank_id;
            // Create customer for bank
            $loans_types = config('common.tables.modals.tbl_states')::where('bank_id', $bank_id)
                                                                        ->get();
            
            return response()->json(['status' => 200, 'data' => $loans_types], 200);            
        } catch (Exception $ex) {
            return $this->handleErrorLogs($ex);
        }
    }


    /**
     * Get all of city State
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCities(Request $request, $id)
    {
        try {
            // Create customer for bank
            $loans_types = config('common.tables.modals.tbl_cities')::where('state_id', $id)
                                                                        ->get();
            
            return response()->json(['status' => 200, 'data' => $loans_types], 200);            
        } catch (Exception $ex) {
            return $this->handleErrorLogs($ex);
        }
    }



    public function TestCurl()
    {
        try {
           $params = [
                'name' =>'audio_check',
                // 'loop' => 3,
                "session_id" => '1234',
                "audio_recording_set_size" => 3,
                'audio_data1' => 'https://drive.google.com/file/d/1PUZ0YL1KKnwvRPUO0TtaVRZf9_Re2T7T/view?usp=sharing',
                'audio_data2' => 'https://drive.google.com/file/d/1PUZ0YL1KKnwvRPUO0TtaVRZf9_Re2T7T/view?usp=sharing',
                'audio_data3' => 'https://drive.google.com/file/d/1PUZ0YL1KKnwvRPUO0TtaVRZf9_Re2T7T/view?usp=sharing',

                // 'stop' => 'current',
                // 'target_legs' => 'both'
            ];

            $url = 'http://10.8.0.1:4500/engine_service1';


            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_FRESH_CONNECT => true,
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($params),
                CURLOPT_HTTPHEADER => [
                    "content-type: multipart/form-data",
                ],
            ]);

            return $response = curl_exec($curl);

            $response1 = json_decode($response, true);
            // if(isset($response['errors']) && $response['errors']!= '') {

            //     $response['error_msg'] = $response['errors'][0]['title'];
            //     unset($response['errors']);
            // } else {
            //     $call_id = $response['data']['call_control_id'];
            // }   

            curl_close($curl);

            return 'okkkkkk';

        } catch (Exception $e) {
            throw new Exception($e->getMessage(), 500);
        }

                    return $response1;
    }


    
}

