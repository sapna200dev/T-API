<?php
namespace App\Http\Controllers\Api\Common;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Log;

use App\Events\TriggerCallRecordingURLsEvent;

use App\Modals\Verification\Actions\Traits\VerificationTraits;
use App\Modals\Employee\Actions\Traits\EmployeeTrait;
use App\Modals\IVRRules\Actions\Traits\IVRRuleTrait;
use App\Modals\EndUser\Actions\Traits\EndUserTraits;
use App\Modals\Telnyx\Actions\Traits\TelnyxTraits;
use App\Modals\CallMapping\Actions\Traits\CallMappingTraits;

use Exception;
use Illuminate\Auth\AuthenticationException;
use App\Exceptions\TurantCustomException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DialogManagerController extends Controller
{
    use VerificationTraits, EmployeeTrait, EndUserTraits, IVRRuleTrait, TelnyxTraits, CallMappingTraits;
    
    /** 
     * Get all roles
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function callback(Request $request)
    {
        try {
            log::debug(' ============================================  callback hit ============================');
           
           log::debug($request);
            // log::debug(json_encode($request));
            $seconds = 0;


            // $request = [
            //     'data' => [
            //         'event_type' => 'call.answered',
            //         'id' => 'e53148cb-46af-4d29-bd98-4124b42751eb',
            //         'occurred_at' => '2020-10-22T10:51:20.047738Z',
            //         'payload' => [
            //             'call_control_id' => 'v2:cyuyVDMYxV1UwFUtsqLz3-fyqhCVPygm9lCKL87eYWYII9vmv9ljxQ',
            //             'call_leg_id' => '363b21d6-151c-11eb-8b02-02420a0fca68',
            //             'call_session_id' => '3635f6ca-151c-11eb-bd08-02420a0fca68',
            //             'client_state' => NULL,
            //             'connection_id' => '1476163749613144047',
            //             'direction' => 'outgoing',
            //             'from' => '+919028466135',
            //             'state' => 'bridging',
            //             'to' => '+919890638629',
            //         ],
            //         'record_type' => 'event',
            //     ],
            //     'meta' => [
            //         'attempt' => 1,
            //         'delivered_to' => 'https://mturbine.com/api/outbound-call-webhook',
            //     ]
            // ]; 


            $payload=[];
            $payload['status']              = $request['data']['event_type'];
            $payload['call_id']             = $request['data']['id'];
            $payload['call_date']           = $request['data']['occurred_at'];
            $payload['call_control_id']     = $request['data']['payload']['call_control_id'];
            $payload['call_leg_id']         = $request['data']['payload']['call_leg_id'];
            $payload['call_session_id']     = $request['data']['payload']['call_session_id'];
            $payload['connection_id']       = $request['data']['payload']['connection_id'];
            $payload['from']                = isset($request['data']['payload']['from']) ? $request['data']['payload']['from'] : null;
            $payload['to']                  = isset($request['data']['payload']['to']) ? $request['data']['payload']['to'] : null;
            $payload['delivered_to']        = isset($request['meta']['delivered_to']) ? $request['meta']['delivered_to'] :null;
            $payload['created_at']          = isset($request['meta']['occurred_at']) ? $request['meta']['occurred_at'] :null;
            $payload['state']               = isset($request['data']['payload']['state']) && $request['data']['payload']['state']!='' ? $request['data']['payload']['state'] : null;

            
            log::debug('payload => ');
            log::debug($payload);

            if(isset($payload['status']) && $payload['status']) {

                switch ($payload['status']) {
                    case 'call.initiated':
                        log::debug('case - call initiated');
                        $this->registerUserCall($payload);
                        break;
                    case 'call.hangup':
                        break;

                    case 'call.answered': 
                        log::debug('case - call answered');
                       $this->playDefaultAudioRecordings($payload);
                        // return $this->callSpeakAPI($payload);
                        break;

                    case 'call.playback.ended':
                        log::debug('case - call playback.ended');
                         $this->playAudioPlaybackRecording($payload,$seconds);
                        break;

                    case 'call.recording.saved':
                         $this->saveAudioRecordings($payload, $request);
                        break;

                    default:
                        # code...
                        break;
                }

            }


            return 'ok';
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage(), 500);
            
            // return $this->handleErrorLogs($ex);
        }
    }

  
    /**
     * Manage call audio urls by sapna
     *
     * @param $from, $to, $audio_url
     * @return object
     */
    private function playDefaultAudioRecordings($payload)
    {
        try {
            log::debug('call playDefaultAudioRecordings  = ');
            $from_number = substr($payload['from'], 3);
            event(new TriggerCallRecordingURLsEvent($from_number, $payload));
        } catch (Exception $e) {
            
        }
    }



    private function registerUserCall($request)
    {
        $returnVal=null;
        try {
            log::debug('call registerUserCall  = ');
            $from_number = substr($request['from'], 3);
            $to_number = substr($request['to'], 3);

            $employee = $this->getRecordByEmployeeNumber($from_number);
            if($employee) {
                $user = $this->getUserByNumber($to_number);
                if($user) {
                    $this->insertFirstRegisterCall($user, $request);
                } // End if
            }
        } catch (Exception $e) {
            $returnVal=null;
            throw new Exception($e->getMessage(), 500);
        }
    
        return $returnVal;
    }

 

    public function playAudioPlaybackRecording($request,$seconds)
    {
        $returnVal=null;
        try { 
            $stop_play_audio_debug_default=5; 
            log::debug('start the reacrding rules logic for'.$request['call_control_id'] );

            // Get call details by control_id
            $callDetails = $this->getEmployeeBankIdByCallControleId($request['call_control_id']);
             log::debug('callDetails = ' );
            // log::debug(json_encode($callDetails, JSON_PRETTY_PRINT));

            if($callDetails != null) {
                // Get end user details
                $endUserDetails = $this->getEndUserForCall($callDetails->end_user_id);
                 log::debug('$endUserDetails = ');
                // log::debug(json_encode($endUserDetails, JSON_PRETTY_PRINT));

                // Get sage agent details
                $employeeDetails = $this->getEmployeeDetailById($callDetails->created_by);
                 log::debug('call employeeDetails = ');
                // log::debug(json_encode($employeeDetails, JSON_PRETTY_PRINT));


                if($employeeDetails == null) {
                    log::debug('Number dose not exist in our system');
                    throw new Exception("Number dose not exist in our system !", 500);
                } else {
                    // Find ivr rules for bank
                    $record = $this->getIvrRulesForCall($employeeDetails->bank_id);

                    if($record != null ) {
                        if($record->recording_rules) {
                            $count = count($record->recording_rules);
                            log::debug(' ****************  Total Recording Rules ******************** ');
                            log::debug($count);

                            $default_index_count = 0;


                            $isMapping = $this->fetchCallMapping($request['call_control_id']);

                            log::debug('fetch call mapping record = '. $isMapping);
                            if($isMapping != null) {
                               $default_index_count =  $isMapping->count;
                            } 


                            if($count > $default_index_count) {
                                if($default_index_count == 0) {
                                    log::debug('Stop play default recording =  '); 
                                    log::debug($default_index_count);

                                    $rules = $record->recording_rules;
                                    $recording_row = $rules[$default_index_count];

                                    log::debug('Record recording_row  == '. $recording_row); 

                                    $isUpdated = $this->updateCallCountByCallControlId($request['call_control_id'], $default_index_count+1);  
                                    log::debug('Record inserted == '. $isUpdated); 
                                   
                                    // Call stop recording API
                                    $this->callStopRecordApi($request);

                                    // Call audio play recording API
                                    $this->callAudioPlayAPI($request, $record, $recording_row->playback_recording);
                                    
                                    // Call start recording API
                                    $this->callStartRecordApi($request);
                                    
                                  
                                } else {
                                    $rules = $record->recording_rules;
                                    $recording_row = $rules[$default_index_count];

                                    log::debug('Play call recoring api recording_row= '); 
                                    log::debug($recording_row); 

                                    log::debug('Perform play music opration '); 
                                    log::debug($default_index_count);

                                    // Call stop recording API
                                    $this->callStopRecordApi($request);

                                    // Update call count
                                    $this->updateCallCountByCallControlId($request['call_control_id'], $default_index_count+1); 

                                    // Call audio play recording API   
                                    $this->callAudioPlayAPI($request, $record, $recording_row->playback_recording);

                                    // Call start recording API
                                    $this->callStartRecordApi($request);
                                } // End if-else

                            } else {
                                log::debug('Call mapping rules ended =  '); 
                            } // End if-else
                        } // End if
                    } // End if
                } // End if-else
            } // End if
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), 500);
        }
    }



    private function saveAudioRecordings($request, $call_back_req)
    {
        $returnVal=null;
        try {
            log::debug('saveAudioRecordings = ');
            log::debug(json_encode($request, JSON_PRETTY_PRINT));
            log::debug($call_back_req);
        } catch (Exception $e) {
           throw new Exception($e->getMessage(), 500); 
        }
    }

}

