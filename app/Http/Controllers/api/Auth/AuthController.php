<?php
namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Log;
use Hash;
use Mail;
use Laravel\Passport\Passport;
use Carbon\Carbon;

//Events added
use App\Events\UserForgotEmailEvent;

// Custom Tarits
use App\Modals\ErrorLogTrait;
use App\Modals\User\Actions\Traits\UserTrait;
use App\Modals\Token\Actions\Traits\TokenTrait;

// Request validation
use App\Http\Requests\AuthRequests\LoginRequests;
use App\Http\Requests\AuthRequests\ForgotPasswordRequest;
use App\Http\Requests\AuthRequests\ResetPasswordRequest;

use Exception;
use Illuminate\Auth\AuthenticationException;
use App\Exceptions\TurantCustomException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use DB;

class AuthController extends Controller
{
    use UserTrait, TokenTrait, ErrorLogTrait;

    /**
     * @OA\Get(
     *     path="/api/testing",
     *     @OA\Response(response="200", description="An example resource"),
     * security={ {"bearer": {} }},
     * )
     */
    public function index(Request $request)
    {
        echo $request;
    }

    /**
     * @OA\Post(
     * path="/api/auth/login",
     * summary="login",
     * description="Login by email, password",
     * tags={"Auth"},
     * security={ },
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass user credentials",
     *    @OA\JsonContent(
     *       required={"email","password"},
     *       @OA\Property(property="email", type="string", format="email", example=""),
     *       @OA\Property(property="password", type="string", format="password", example=""),
     *    ),
     * ),
     * @OA\Response(
     *    response=422,
     *    description="",
     *     ),
     * )
     */
    public function login(LoginRequests $request)
    {
        try
        {
            $bank_name='';
            
            // Check login user auth
            $user = $this->authCheck($request);
            if ($user == null || !Hash::check($request->password, $user->password))
            {
                throw new Exception('Provided Credentials do not match. Try Again !', 500);
            }
            else
            {
                $user_status = $this->getUserStatus($request);
                if ($user_status == null)
                {
                    throw new Exception('Your account is inactive, please contact with you bank manager', 500);
                }

                // Create login user token
                $tokenResult = $user->createToken('access_token');

                $accessToken = $tokenResult->accessToken;
                $token = $tokenResult->token;

                if($user->bank) {
                    $bank_name = $user->bank->bank_name;
                }

                return response()
                    ->json(['token' => 'Bearer ' . $accessToken, 'name' => $user->name, 'email' => $user->email, 'bank_name'=> $bank_name, 'username' => $user->username,'roles' => $user->role, 'id' => $user->id, 'profile' => $user->image, 'bank_id'=>$user->bank_id
                // 'token_expire' => $token
                ]);
            } // End if-else
        }
        catch(Exception $ex)
        {
            return $this->handleErrorLogs($ex);
        }
        catch(TurantCustomException $e)
        {
            throw new TurantCustomException($e->getMessage() , 500);
        } // End Try-Catch
        
    } // End function
    

    
    /**
     * @OA\Post(
     * path="/api/auth/forgot-password",
     * summary="Forgot Password",
     * description="Forgot password",
     * operationId="",
     * tags={"Auth"},
     * security={ },
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass user credentials",
     *    @OA\JsonContent(
     *       required={"email"},
     *       @OA\Property(property="email", type="string", format="email", example=""),
     *    ),
     * ),
     * @OA\Response(
     *    response=422,
     *    description="",
     *     )
     * )
     */
    public function forgotPassword(ForgotPasswordRequest $request)
    {
        try
        {
            // Check validate forgot email
            $user_obj = $this->validateUsername($request->email);
            if ($user_obj == null)
            {
                throw new NotFoundHttpException('Email dose not exists in system');
            }

            // Find user token
            $token = $this->generateToken();

            // Delete existing token
            $removeToken = $this->deleteToken($request, $token);

            // Update new token
            $saveToken = $this->saveUserToken($request, $token);

            // Fire send forgot mail event
            event(new UserForgotEmailEvent($token, $user_obj));

            return response()->json(['status' => 200, 'msg' => 'Password reset secure link sent to your email.'], 200);
        }
        catch(Exception $ex)
        {
            return $this->handleErrorLogs($ex);
        }

    }

    /**
     * Return reset password UI Added by Sapna
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return View
     */
    public function resetPassword(Request $request)
    {
        try
        {
            return view('email.resetPassword', ['token' => $request->token]);
        }
        catch(Exception $ex)
        {
            return $this->handleErrorLogs($ex);
        }
    }

    /**
     * @OA\Post(
     * path="/api/auth/reset-password",
     * summary="Reset Password",
     * description="Reset Password",
     * operationId="reset_pass",
     * security={ },
     * tags={"Auth"},
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass token, email, password, confirm password",
     *    @OA\JsonContent(
     *       required={"token","email","password","confirm_password"},
     *       @OA\Property(property="token", type="string", format="token", example=""),
     *       @OA\Property(property="email", type="string", format="email", example=""),
     *       @OA\Property(property="password", type="string", format="password", example=""),
     *       @OA\Property(property="confirm_password", type="string", format="password", example=""),
     *    ),
     * ),
     * @OA\Response(
     *    response=422,
     *    description="",
     *     )
     * )
     */
    public function resetUserPassword(ResetPasswordRequest $request)
    {
        try
        {
            // Check validate forgot email
            $isEmailExist = $this->validateUsername($request->email);
            if ($isEmailExist == null)
            {
                throw new NotFoundHttpException('Email dose not exists in system');
            }

            // Find user token
            $validToken = $this->getUserToken($request);
            if ($validToken == null)
            {
                throw new Exception('Invalid Token');
            } // End if
            // Update user with new password
            $userUpdate = $this->updateUserPassword($request->email, $request->password);

            return response()
                ->json(['status' => 200, 'msg' => 'Reset password successfully.'], 200);
        }
        catch(Exception $ex)
        {
            return $this->handleErrorLogs($ex);
        }
    }

    /**
     * @OA\GET(
     * path="/api/auth/logout",
     * summary="logout",
     * description="logout",
     * operationId="logout",
     * tags={"Auth"},
     * security={ {"bearer": {} }},
     * @OA\Response(
     *    response=200,
     *    description="Successfully logout",
     *     )
     * )
     */
    public function logout(Request $request)
    {
        try
        {
            $request->user()
                ->token()
                ->revoke();

            return response()
                ->json(['status' => 200, 'msg' => 'User successfully logged out'], 200);

        }
        catch(Exception $e)
        {
            return $this->handleErrorLogs($ex);
        }
    }

}

