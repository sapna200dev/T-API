<?php
namespace App\Http\Controllers\Api\EndUser;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Log;

// Custom Tarits
use App\Modals\ErrorLogTrait;
use App\Modals\EndUser\Actions\Traits\EndUserTraits;
use App\Http\Requests\EndUser\CreateEndUserRequest;
use App\Modals\Logs\Actions\Traits\LogTraits;
use App\Modals\Language\Actions\Traits\LanguageTrait;
use App\Http\Requests\EndUser\EditEndUserRequest;
use App\Modals\Loan\Actions\Traits\LoanTrait;
use App\Modals\Branch\Actions\Traits\BranchTrait;
use App\Modals\Employee\Actions\Traits\EmployeeTrait;

use Exception;
use Illuminate\Auth\AuthenticationException;
use App\Exceptions\TurantCustomException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class EndUserController extends Controller
{
    use ErrorLogTrait, EndUserTraits, LogTraits, LanguageTrait, LoanTrait, BranchTrait, EmployeeTrait;

    /**
     * Add new end user
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllEndUsers(Request $request)
    {
        try
        {

            // Create end user
            $users = $this->getEndUsers();
            foreach ($users as $key => & $value)
            {
                $value['name'] = $value['first_name'] . ' ' . $value['last_name'];
                $value['language_type'] = $value->language['language'];
                $value['loan_type_name'] = $value->loan_type['loan_type'];
                $value['sales_agents_name'] = $value->sales_agents['first_name'] . ' ' . $value->sales_agents['last_name'];
                $value['current_status'] = ucwords($value['current_status']);
            }

            return response()->json(['status' => 200, 'data' => $users], 200);
        }
        catch(Exception $ex)
        {
            return $this->handleErrorLogs($ex);
        }
    }

    /**
     * Get End user by id
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getById(Request $request, $id)
    {
        try
        {
            // Create end user
            $user = $this->getEndUserById($id);
            if($user->language) {
                $user['language_name'] = $user->language->language;  
            }
            
            if ($user->user)
            {
                $user['updated_user'] = $user
                    ->user->bank_name;
            }

            if ($user == null)
            {
                throw new Exception("Pleas pass valid end user id", 404);
            } //End if
            return response()->json(['status' => 200, 'data' => $user], 200);
        }
        catch(Exception $ex)
        {
            return $this->handleErrorLogs($ex);
        }
    }


    /**
     * Get End user by id
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEndUserDetails(Request $request, $id)
    {
        try
        {
            // Create end user
            $user = $this->getEndUserDetailById($id);
            if($user->language) {
                $user['language_name'] = $user->language->language;  
            }
            
            // if ($user->user)
            // {
            //     $user['updated_user'] = $user
            //         ->user->bank_name;
            // }
            // if ($user->verifications)
            // {
            //     $user['uploaded_by'] = $user
            //         ->verifications
            //         ->user->bank_name;
            // }

            if ($user == null)
            {
                throw new Exception("Pleas pass valid end user id", 404);
            } //End if
            return response()->json(['status' => 200, 'data' => $user], 200);
        }
        catch(Exception $ex)
        {
            if($ex->getCode() != 0) {
                return $this->handleErrorLogs($ex);
            } else {
               throw new Exception($ex->getMessage(), 500); 
            }
            // throw new Exception("Error Processing Request", 500);
            
            
        }
    }

    /**
     * GET EMPLOYEE NUMBER
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEmployeeNumber(Request $request, $id)
    {
        $retunVal = null;
        try {
            // Get employee number
            $employee_details =  $this->getEmployeeDetailById($id);
            $mobile_number = '';

            if(isset($employee_details->mobile_number)) $mobile_number = $employee_details->mobile_number;

            return response()->json(['status' => 200, 'number' => $mobile_number], 200);
        } catch(Exception $ex)
        {
            return $this->handleErrorLogs($ex);
        }
    }


    /**
     * Add new end user
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addEndUser(CreateEndUserRequest $request)
    {
        try
        {
            // Check end user is exist or not
            $isUser = $this->getEndUserByRegisteredMobile($request->registered_mobile, $request->sales_agent_contact, $request->team_leader_contact);
            if ($isUser)
            {
                throw new Exception("Registered mobile number is already exist, please use another", 500);
            }

            // Create end user
            $createUser = $this->createEndUser($request->toArray());

            return response()
                ->json(['status' => 200, 'msg' => 'New End User Create Successfully'], 200);
        }
        catch(Exception $ex)
        {
            return $this->handleErrorLogs($ex);
        }
    }

    /**
     * Update end user
     *
     * @param  \Illuminate\Http\Request  $request $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function editEndUser(EditEndUserRequest $request, $id)
    {
        try
        {
            // Create end user
            $updateUser = $this->updateEndUserRecord($request, $id);

            return response()->json(['status' => 200, 'msg' => 'End User Updated Successfully'], 200);
        }
        catch(Exception $ex)
        {
            return $this->handleErrorLogs($ex);
        }
    }


    /**
     * Update end user
     *
     * @param  \Illuminate\Http\Request  $request $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEndUserLogData(Request $request, $id)
    {
        try
        {
            // Create end user
            $logs = $this->getUserLogs($id);

            return response()->json(['status' => 200, 'data' => $logs], 200);
        }
        catch(Exception $ex)
        {
            return $this->handleErrorLogs($ex);
        }
    }




    /**
     * Delete end user
     *
     * @param  \Illuminate\Http\Request  $request $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteEndUser(Request $request, $id)
    {
        try
        {
            // Check exist or not
            $isExistUser = $this->getEndUserById($id);
            if ($isExistUser == null)
            {
                throw new Exception("User dose not exist", 500);
            } // End if
            // Create end user
            $updateUser = $this->deleteEndUserById($request, $id);

            return response()->json(['status' => 200, 'msg' => 'End User Deleted Successfully'], 200);
        }
        catch(Exception $ex)
        {
            return $this->handleErrorLogs($ex);
        }
    }
}

