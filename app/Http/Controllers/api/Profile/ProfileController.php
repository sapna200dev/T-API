<?php
namespace App\Http\Controllers\Api\Profile;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Log;
use Carbon\Carbon;
use Auth;

// Custom Tarits
use App\Modals\ErrorLogTrait;
use App\Modals\User\Actions\Traits\UserTrait;

// Request validation
use App\Http\Requests\CustomerRequests\CreateCustomerRequests;
use App\Http\Requests\CustomerRequests\updateCustomerRequests;



use Exception;
use Illuminate\Auth\AuthenticationException;
use App\Exceptions\TurantCustomException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProfileController extends Controller
{
    use UserTrait, ErrorLogTrait;


    /**
     * Get all customers for bank
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateProfile(Request $request, $id)
    {
        try {

            // Create customer for bank
           $user = $this->updateUserProfile($id,$request);
            
            return response()->json(['status' => 200, 'msg' => 'User Profile Updated Successfully !', 'data' => $user], 200);            
        } catch (Exception $ex) {
            return $this->handleErrorLogs($ex);
        }
    }

  
    
}

