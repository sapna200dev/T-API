<?php
namespace App\Http\Controllers\Api\Loan;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Log;

// Custom Tarits
use App\Modals\ErrorLogTrait;
use App\Modals\Loan\Actions\Traits\LoanTrait;


use Exception;
use Illuminate\Auth\AuthenticationException;
use App\Exceptions\TurantCustomException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class LoanController extends Controller
{
    use ErrorLogTrait;


    /**
     * Get all roles
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function allLoanTypes(Request $request)
    {
        try {
            
            // Create customer for bank
            $loans_types = config('common.tables.modals.tbl_loan_types')::get();
            
            return response()->json(['status' => 200, 'data' => $loans_types], 200);            
        } catch (Exception $ex) {
            return $this->handleErrorLogs($ex);
        }
    }

  
    
}

