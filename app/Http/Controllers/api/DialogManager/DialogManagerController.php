<?php
namespace  App\Http\Controllers\Api\DialogManager;


use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Log;

use App\Modals\DialogManager\Actions\Traits\DialogManagerTrait;
use App\Modals\IVRRules\Actions\Traits\IVRRuleTrait;

use Exception;
use Illuminate\Auth\AuthenticationException;
use App\Exceptions\TurantCustomException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DialogManagerController extends Controller
{
    use DialogManagerTrait, IVRRuleTrait;

    /**
     * Get all roles
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request, $id)
    {
        $returnVal='';
        try {

           
            // Initiate call 
            $response = $this->initiateOutboundCall($id, $request);
 			 if($response) {
                return response()->json(['status' => 200, 'data' => $response], 200);      
             } else {
                return response()->json(['status' => 500, 'data' => $response], 200);   
             }  

 			 
           // $returnVal = $response->data->result;
        } catch (Exception $ex) {
            $returnVal='';
            throw new Exception($ex->getMessage(), 500);
        }

    }    
}

