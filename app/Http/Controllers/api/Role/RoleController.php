<?php
namespace App\Http\Controllers\Api\Role;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Log;

// Custom Tarits
use App\Modals\ErrorLogTrait;
use App\Modals\Role\Actions\Traits\RoleTrait;


use Exception;
use Illuminate\Auth\AuthenticationException;
use App\Exceptions\TurantCustomException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RoleController extends Controller
{
    use RoleTrait, ErrorLogTrait;


    /**
     * Get all roles
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllRoles(Request $request)
    {
        try {
          
            // Create customer for bank
            $roles = $this->getRoles();
            
            return response()->json(['status' => 200, 'data' => $roles], 200);            
        } catch (Exception $ex) {
            return $this->handleErrorLogs($ex);
        }
    }

  
    
}

