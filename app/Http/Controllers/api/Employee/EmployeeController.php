<?php
namespace App\Http\Controllers\Api\Employee;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Log;
use Carbon\Carbon;
use Auth;

// Custom Tarits
use App\Modals\ErrorLogTrait;
use App\Modals\Employee\Actions\Traits\EmployeeTrait;
use App\Modals\Role\Actions\Traits\RoleTrait;
use App\Modals\User\Actions\Traits\UserTrait;

// Request validation
use App\Http\Requests\EmployeeRequests\CreateEmployeeRequests;
use App\Http\Requests\EmployeeRequests\UpdateEmployeeRequests;

use Exception;
use Illuminate\Auth\AuthenticationException;
use App\Exceptions\TurantCustomException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class EmployeeController extends Controller
{
    use EmployeeTrait, RoleTrait, UserTrait, ErrorLogTrait;




    /**
     * @OA\GET(
     * path="/api/employee/list",
     * summary="employee list",
     * description="Get all employees",
     * operationId="employee",
     * tags={"Employee"},
     *  security={{ "bearer": {} }},
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *          mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    /**
     * Get all customers for bank
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function allEmployees()
    {
        try
        {
            // Create customer for bank
            $employee_list = $this->getAllEmployessForBank();

            foreach ($employee_list as $key => & $value)
            {
                $value['name'] = $value['first_name'] . ' ' . $value['last_name'];
                $value['role'] = ucwords(str_replace("_", " ", $value
                    ->employee_role
                    ->name));
                $value['status'] = ucwords(str_replace("-", " ", $value['status']));
                $value['branch_name'] = $value
                    ->branch->branch_name;
            }

            return response()
                ->json(['status' => 200, 'data' => $employee_list], 200);
        }
        catch(Exception $ex)
        {
            return $this->handleErrorLogs($ex);
        }
    }


    /**
     * @OA\GET(
     * path="/api/employee/{id}",
     * summary="employee by id",
     * description="Get employee by id",
     * operationId="employee",
     * tags={"Employee"},
     *  security={{ "bearer": {} }},
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *          mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    /**
     * Get customer by id
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getById($id)
    {
        try
        {
            // Create customer for bank
            $employee = $this->getEmployeeDetailById($id);
            if ($employee == null)
            {
                throw new Exception("This pass valid employee id", 500);
            }

            return response()->json(['status' => 200, 'data' => $employee], 200);
        }
        catch(Exception $ex)
        {
            return $this->handleErrorLogs($ex);
        }
    }



    /**
     * Create employee for  bank
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createEmployee(CreateEmployeeRequests $request)
    {
        try
        {
            // Validate duplicate username
            $checkUser = $this->validateUsername($request->employee['username']);
            if ($checkUser)
            {
                throw new Exception("This admin username is already in use, please use another username", 500);
            }

            // Validate duplicate employee email
            $isEmployee = $this->validateEmployeeUsername($request->employee['username']);
            if ($isEmployee)
            {
                throw new Exception("This employee id already exist, please use another username", 500);
            }

            // Create customer for bank
            $created_customer = $this->createdNewEmployeForBank($request);
            return response()->json(['status' => 200, 'msg' => 'New Customer Created successfully.'], 200);

        }
        catch(Exception $ex)
        {
            return $this->handleErrorLogs($ex);
        }
    }

    /**
     * Update employee for  bank
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateEmployee(UpdateEmployeeRequests $request, $id)
    {
        try
        {
            // update customer for bank
            $updated_customer = $this->updateCustomerForBank($id, $request);
            if ($updated_customer)
            {
                return response()->json(['status' => 200, 'msg' => 'Customer Updated Successfully'], 200);
            } // End if
            
        }
        catch(Exception $ex)
        {
            return $this->handleErrorLogs($ex);
        }
    }


    /**
     * @OA\GET(
     * path="/api/employee/delete/{id}",
     * summary="employee delete by id",
     * description="Delete employee by id",
     * operationId="employee_delete",
     * tags={"Employee"},
     *  security={{ "bearer": {} }},
     *   @OA\Response(
     *      response=200,
     *       description="Success",
     *      @OA\MediaType(
     *          mediaType="application/json",
     *      )
     *   ),
     *   @OA\Response(
     *      response=401,
     *       description="Unauthenticated"
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    /**
     * Delete employee for bank by id
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteEmployee(Request $request, $id)
    {
        try
        {
            // delete customer for bank
            $deleted_customer = $this->deleteCustomerForBank($id, $request);
            if ($deleted_customer)
            {
                return response()->json(['status' => 200, 'msg' => 'Employee Deleted Successfully.'], 200);
            } // End if
            
        }
        catch(Exception $ex)
        {
            return $this->handleErrorLogs($ex);
        }
    }

}

