<?php
namespace App\Http\Controllers\Api\Status;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Log;

// Custom Tarits
use App\Modals\ErrorLogTrait;
use App\Modals\Status\Actions\Traits\StatusTraits;


use Exception;
use Illuminate\Auth\AuthenticationException;
use App\Exceptions\TurantCustomException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class StatusController extends Controller
{
    use StatusTraits, ErrorLogTrait;


    /**
     * Get all roles
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDefaultStatues(Request $request)
    {
        try {
          
            // Create customer for bank
            $statuses = $this->getAllStatuses();
            
            return response()->json(['status' => 200, 'data' => $statuses], 200);            
        } catch (Exception $ex) {
            return $this->handleErrorLogs($ex);
        }
    }

  
    
}

