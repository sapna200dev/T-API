<?php
namespace App\Http\Controllers\Api\Bank;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Log;
use Hash;
use Mail;
use Laravel\Passport\Passport;
use Carbon\Carbon;
use Auth;
use App\Events\SendUserEmail;
// Custom Tarits
use App\Modals\ErrorLogTrait;
use App\Modals\Bank\Actions\Traits\BankTrait;

use App\Modals\Role\Actions\Traits\RoleTrait;
use App\Modals\Employee\Actions\Traits\EmployeeTrait;
use App\Modals\User\Actions\Traits\UserTrait;

// Request validation
use App\Http\Requests\BankRequests\createBankRequests;
use App\Http\Requests\BankRequests\updateBankRequests;
use App\Http\Requests\AuthRequests\ResetPasswordRequest;

// Mail
use App\Mail\ForgotPassword;

use Exception;
use Illuminate\Auth\AuthenticationException;
use App\Exceptions\TurantCustomException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BankController extends Controller
{
    use BankTrait, RoleTrait, UserTrait, EmployeeTrait, ErrorLogTrait;

    /**
     * @OA\GET(
     * path="/api/bank/all",
     * summary="bank",
     * description="Get all banks",
     * operationId="bank",
     * tags={"Bank"},
     *  security={{ "bearer": {} }},
     * @OA\Response(
     *    response=422,
     *    description="",
     *     )
     * )
     */
    public function banksList(Request $request)
    {
        try
        {
            // Create new bank
            $banks = $this->getAll($request);
            foreach ($banks as $key => & $value)
            {
                $value['status'] = ucwords(str_replace("-", " ", $value['status']));
                $value['billing_cycle'] = ucwords($value['billing_cycle']);
            }


           return response()->json(['status' => 200, 'data' => $banks], 200);
        }
        catch(Exception $ex)
        {
            return $this->handleErrorLogs($ex);
        }
        catch(TurantCustomException $e)
        {
            throw new TurantCustomException($e->getMessage() , 500);
        } // End Try-Catch
        
    } // End function
    

    /**
     * @OA\GET(
     * path="/api/bank/{id}",
     * summary="bank",
     * description="Get banks by id",
     * operationId="bankId",
     * tags={"Bank"},
     *  security={{ "bearer": {} }},
     * @OA\Response(
     *    response=422,
     *    description="",
     *     )
     * )
     */
    /**
     * Get bank by id
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBankDetail($id)
    {
        try
        {
            // Create new bank
            $bank = $this->getBankById($id);
            if ($bank == null)
            {
                throw new Exception("Record dose not exist", 404);
            }

            // Get all customers
            $customers = $this->getAllBankCustomers($id);

            $bank['customers'] = $customers;
            return response()->json(['status' => 200, 'data' => $bank], 200);
        }
        catch(Exception $ex)
        {
            return $this->handleErrorLogs($ex);
        }
        catch(TurantCustomException $e)
        {
            throw new TurantCustomException($e->getMessage() , 500);
        } // End Try-Catch
        
    } // End function
    


    /**
     * Create new bank
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createBank(createBankRequests $request)
    {
        try
        {
            // Check username
            $isBank = $this->validateUsername($request->admin['admin_username']);
            if ($isBank)
            {
                throw new Exception("This admin username is already in use, please use another username", 500);
            }

            // Create new bank
           $created = $this->createNewBank($request);

            return response()->json(['status' => 200, 'msg' => 'Bank created successfully.'], 200);
        }
        catch(Exception $ex)
        {
            return $this->handleErrorLogs($ex);
        }
        catch(TurantCustomException $e)
        {
            throw new TurantCustomException($e->getMessage() , 500);
        } // End Try-Catch
        
    } // End function
    

    
    /**
     * Update bank by id
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateBank(updateBankRequests $request, $id)
    {
        try
        { // Create new bank
            $updated = $this->updatedBank($id, $request);

            return response()->json(['status' => 200, 'msg' => 'Bank updated successfully.'], 200);
        }
        catch(Exception $ex)
        {
            return $this->handleErrorLogs($ex);
        }
        catch(TurantCustomException $e)
        {
            throw new TurantCustomException($e->getMessage() , 500);
        } // End Try-Catch
        
    } // End function
    

    
    /**
     * Delete bank by id
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteBank(Request $request, $id)
    {
        try
        {
            // Delete new bank
            $deleted = $this->deleteBankRecord($id, $request);

            return response()->json(['status' => 200, 'msg' => 'Bank deleted successfully.'], 200);
        }
        catch(Exception $ex)
        {
            return $this->handleErrorLogs($ex);
        }
        catch(TurantCustomException $e)
        {
            throw new TurantCustomException($e->getMessage() , 500);
        } // End Try-Catch
        
    } // End function
    

    
}

