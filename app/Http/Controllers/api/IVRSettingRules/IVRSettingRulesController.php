<?php
namespace App\Http\Controllers\Api\IVRSettingRules;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Log;
use Storage;
use Auth;
use Carbon\Carbon;
// Custom Tarits
use App\Modals\ErrorLogTrait;
use App\Modals\Loan\Actions\Traits\LoanTrait;
use App\Modals\IVRRules\Actions\Traits\IVRRuleTrait;
use App\Modals\Bank\Actions\Traits\BankTrait;
use App\Modals\IVRRulesRecording\Actions\Traits\IVRRulesRecordingTrait;

use Exception;
use Illuminate\Auth\AuthenticationException;
use App\Exceptions\TurantCustomException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class IVRSettingRulesController extends Controller
{
    use ErrorLogTrait, IVRRuleTrait, IVRRulesRecordingTrait, BankTrait;


    /**
     * Get all rule sets
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $returnVal = [];
        try
        {
            $rules = $this->getAllIvrRules();
            foreach ($rules as $key => & $value)
            {
                $value['ivr_rule_status'] = ucwords(str_replace("-", " ", $value['rule_status']));
                $value['ivr_rule_set_name'] = ucwords($value['ivr_rule_set_name']);
            }

            return response()->json(['status' => 200, 'data' => $rules], 200);
        }
        catch(Exception $e)
        {
            $returnVal = [];
            throw new Exception($ex->getMessage() , 500);
        }
    }


    /**
     * Get all rule sets
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRuleById(Request $request, $id)
    {
        $returnVal = [];
        try
        {
            $rule = $this->getIvrRuleById($id);
            return response()->json(['status' => 200, 'data' => $rule], 200);
        }
        catch(Exception $e)
        {
            $returnVal = [];
            throw new Exception($ex->getMessage() , 500);
        }
    }

    /**
     * Get all roles
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addNewRule(Request $request)
    {
        try
        {
            if ($request)
            {
                $isExist = $this->checkIvrRuleSet($request);               
                if($isExist && $request->isUpdate == false) {
                   throw new TurantCustomException('Rule Set Name is already exist, please use another one !', 500);
                } else {

                    $isUploadingError = false;

                    $recordings = $request->recording_fields;
                    $data = json_decode($recordings);
                    if ($data)
                    {
                        foreach ($data as $key => & $record)
                        {
                             $recording_fields_file = 'recording_fields_file_' . $key;

                            if ($request->hasFile($recording_fields_file) && $request->hasFile('check_upload_recording') && $request->hasFile('ai_dm_pass_text_uploading_recording') && $request->hasFile('ai_dm_fail_text_uploading_recording') && $request->hasFile('welcome_text_audio_recording') )
                            {
                                $file_1 = $request->file($recording_fields_file);
                                $this->validateRequestedFile($file_1);

                                $file_2 = $request->file('welcome_text_audio_recording');
                                $this->validateRequestedFile($file_2);

                                $file_3 = $request->file('check_upload_recording');
                                $this->validateRequestedFile($file_3);


                                $file_4 = $request->file('ai_dm_pass_text_uploading_recording');
                                $this->validateRequestedFile($file_4);


                                $file_5 = $request->file('ai_dm_fail_text_uploading_recording');
                                $this->validateRequestedFile($file_5);

                            } else {
                                if($request->isUpdate == 'false') {
                                    $isUploadingError = true;
                                    throw new TurantCustomException('Please Fill all fields with valid file!', 500);
                                }

                            }

                        }

                    }


                    if(!$isUploadingError) {
                   
                            $folder_name = config('common.s3.upload.audio.folder');


                            $bank_id = Auth::user()->bank_id;

                            // Get bank id
                            $bank = $this->getBankById($bank_id);
                            $bankName = isset($bank->bank_name) ? $bank->bank_name : 'No_Bank';


                            $storage_driver = "'" . config('common.s3.driver_name') . "'";
                            $uploaded_urls = [];
                            // return  $request->hasFile('check_upload_recording');
                            if ($request->hasFile('check_upload_recording'))
                            {
                                $file_1 = $request->file('check_upload_recording'); 
                                $folder = config('common.s3.upload.check_audio');
                                // return 'ssss';
                                // validate request files
                                $isError = $this->validateRequestedFile($file_1);
                                if ($isError)
                                {
                                    throw new Exception("We only accept mp3 and wav formatted file, please check your file. If you have further questions, please contact Bank Admin support. Thanks!", 500);
                                }
                                else
                                {
                                    $filenamewithextension = $file_1->getClientOriginalName();
                                    $extension = $file_1->getClientOriginalExtension();
                                    $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

                                    // $file_path = $folder_name . '/' . $folder . '/' . $filename . '_' . time() . '.' . $extension;
                                    $file_path = $folder_name .'/'. $bankName . '/' . $folder . '/' . $filename . '_' . time() . '.' . $extension;

                                    $exists = Storage::disk('s3')->exists($file_path);

                                    if ($exists)
                                    {
                                        $url = Storage::disk('s3')->url($file_path);
                                    }
                                    else
                                    {
                                        //Upload File to s3
                                        Storage::disk('s3')->put($file_path, fopen($file_1, 'r+') , 'public');

                                        $url = Storage::disk('s3')->url($file_path);
                                    }

                                    $request['check_audio_recording'] = $url;

                                } // End if 
                            }

                            if ($request->hasFile('welcome_text_audio_recording'))
                            {
                                $file_1 = $request->file('welcome_text_audio_recording');
                                $folder = config('common.s3.upload.welcome_text_audio_recording');
                                // return 'ssss';
                                // validate request files
                                $isError = $this->validateRequestedFile($file_1);
                                if ($isError)
                                {
                                    throw new Exception("We only accept mp3 and wav formatted file, please check your file. If you have further questions, please contact Bank Admin support. Thanks!", 500);
                                }
                                else
                                {
                                    $filenamewithextension = $file_1->getClientOriginalName();
                                    $extension = $file_1->getClientOriginalExtension();
                                    $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

                                    // $file_path = $folder_name . '/' . $folder . '/' . $filename . '_' . time() . '.' . $extension;
                                    $file_path = $folder_name .'/'. $bankName . '/' . $folder . '/' . $filename . '_' . time() . '.' . $extension;

                                    $exists = Storage::disk('s3')->exists($file_path);

                                    if ($exists)
                                    {
                                        $url = Storage::disk('s3')->url($file_path);
                                    }
                                    else
                                    {
                                        //Upload File to s3
                                        Storage::disk('s3')->put($file_path, fopen($file_1, 'r+') , 'public');

                                        $url = Storage::disk('s3')->url($file_path);
                                    }


                                    $request['welcome_text_audio_recording_url'] = $url;

                                } // End if 
                            }

                            if ($request->hasFile('ai_dm_pass_text_uploading_recording'))
                            {
                                $file_2 = $request->file('ai_dm_pass_text_uploading_recording');
                                # $folder = '/ai-dm-pass-audio/';
                                $folder = config('common.s3.upload.ai_dm_pass_audio');

                                // validate request files
                                $isError = $this->validateRequestedFile($file_2);
                                if ($isError)
                                {
                                    throw new Exception("We only accept mp3 and wav formatted file, please check your file. If you have further questions, please contact Bnak Admin support. Thanks!", 500);
                                }
                                else
                                {
                                    $filenamewithextension = $file_2->getClientOriginalName();
                                    $extension = $file_2->getClientOriginalExtension();
                                    $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
                                    // $file_path = $folder_name . '/' . $folder . '/' . $filename . '_' . time() . '.' . $extension;
                                    $file_path = $folder_name .'/'. $bankName . '/' . $folder . '/' . $filename . '_' . time() . '.' . $extension;

                                    $exists = Storage::disk('s3')->exists($file_path);

                                    if ($exists)
                                    {
                                        $url = Storage::disk('s3')->url($file_path);
                                    }
                                    else
                                    {
                                        //Upload File to s3
                                        Storage::disk('s3')->put($file_path, fopen($file_2, 'r+') , 'public');

                                        $url = Storage::disk('s3')->url($file_path);
                                    }

                                    $request['am_dm_pass_text_recording'] = $url;
                                } // End if
                                

                                
                            }

                            if ($request->hasFile('ai_dm_fail_text_uploading_recording'))
                            {
                                $file_3 = $request->file('ai_dm_fail_text_uploading_recording');
                                $folder = config('common.s3.upload.ai_dm_fail_audio');

                                // validate request files
                                $isError = $this->validateRequestedFile($file_3);
                                if ($isError)
                                {
                                    throw new Exception("We only accept mp3 and wav formatted file, please check your file. If you have further questions, please contact Bnak Admin support. Thanks!", 500);
                                }
                                else
                                {
                                    $filenamewithextension = $file_3->getClientOriginalName();
                                    $extension = $file_3->getClientOriginalExtension();
                                    $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
                                    // $file_path = $folder_name . '/' . $folder . '/' . $filename . '_' . time() . '.' . $extension;

                                    $file_path = $folder_name .'/'. $bankName . '/' . $folder . '/' . $filename . '_' . time() . '.' . $extension;
                                    $exists = Storage::disk('s3')->exists($file_path);

                                    if ($exists)
                                    {
                                        $url = Storage::disk('s3')->url($file_path);
                                    }
                                    else
                                    {
                                        //Upload File to s3
                                        Storage::disk('s3')->put($file_path, fopen($file_3, 'r+') , 'public');

                                        $url = Storage::disk('s3')->url($file_path);
                                    }

                                    $request['am_dm_fail_text_recording'] = $url;
                                } // End if
                            
                            }

                        // return $request;
                            $payload = $this->generateIvrRegistrationPayload($request);


                            if($request->isUpdate == true) {
                                $rules = $this->updateIvrRuleSettings($payload, $request->id);
                            }  else {
                                // return [$payload , 'created'];
                                $rules = $this->saveIvrRuleSettings($payload);
                            }
                           
                            // Update and create IVR Registration Recording
                            $success = $this->saveIvrRecordings($rules, $bankName, $request->recording_fields, $request);

                            if($request->isUpdate == true) {
                                return response()->json(['status' => 200, 'msg' => 'IVR Rule Updated successfully'], 200);
                            } else {
                               return response()->json(['status' => 200, 'msg' => 'IVR Rule Added successfully'], 200);      
                            }
                    }

                }
            }
        }
        catch(Exception $ex)
        {
            throw new Exception($ex->getMessage() , 500);

            return $this->handleErrorLogs($ex);
        }
    }

    /**
     * Validate uploaded files
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function validateRequestedFile($file)
    {
        $returnVal = null;
        try
        {
            $file_mimetypes_type = $file->getClientMimeType();

            $csv_mimetypes = config('common.s3.upload.audio.files_validation');

            if (!in_array($file_mimetypes_type, $csv_mimetypes))
            {
                throw new TurantCustomException('We only accept mp3 and wav formatted file, please check your file. If you have further questions, please contact Bank Admin support. Thanks!', 500);
            }

        }
        catch(Exception $ex)
        {
            $returnVal = null;
            throw new Exception($ex->getMessage() , 500);

            // return $this->handleErrorLogs($ex);
            
        }

        return $returnVal;
    }

    /**
     * Genrate payload
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function generateIvrRegistrationPayload($request)
    {
        $payload = [];
        try
        {

            $user_id = Auth::user()->id;

            $params = [];
            $params['ivr_rule_set_name'] = strtolower($request->ivr_rules_set);
            $params['bank_name'] = $request->bank_name;
 
            $params['welcome_text'] = $request->welcome_text;
            $params['total_recordings'] = $request->total_recording;
            $params['rule_status'] = $request->activation_status;
            $params['check_audio'] = $request->check_audio;
            $params['am_dm_pass_text'] = $request->ai_dm_pass_text;
            $params['am_dm_fail_text'] = $request->ai_dm_fail_text;
             $params['is_registration'] = $request->is_registration;
            $params['created_by'] = $user_id;
            $params['created_at'] = Carbon::now();

            if(isset($request->welcome_text_audio_recording_url) && $request->welcome_text_audio_recording_url != null){
                 $params['welcome_text_audio_recording'] = $request->welcome_text_audio_recording_url;
            }
                      
            if(isset($request->check_audio_recording) && $request->check_audio_recording != null ) {
                $params['check_audio_recording'] = $request->check_audio_recording;
            }

            if(isset($request->am_dm_pass_text_recording) &&  $request->am_dm_pass_text_recording != null ) {
                $params['am_dm_pass_text_recording'] = $request->am_dm_pass_text_recording;
            }
            if(isset($request->am_dm_fail_text_recording) && $request->am_dm_fail_text_recording != null ) {
                 $params['am_dm_fail_text_recording'] = $request->am_dm_fail_text_recording;
            }
            if(isset($request->activation_date) && $request->activation_date != null) {
                   $params['activation_date'] = $request->activation_date;
            } 

            if(isset($request->deactivation_date) && $request->deactivation_date !=null) {
                $params['deactivation_date'] = $request->deactivation_date;
            }


            $payload = $params;
        }
        catch(Exception $e)
        {
            $payloadaylo = [];
            throw new Exception("Create payload error", 500);

        }

        return $payload;
    }

}

