<?php
namespace App\Http\Controllers\Api\Language;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Log;

// Custom Tarits
use App\Modals\ErrorLogTrait;
use App\Modals\Loan\Actions\Traits\LoanTrait;


use Exception;
use Illuminate\Auth\AuthenticationException;
use App\Exceptions\TurantCustomException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class LanguageController extends Controller
{
    use ErrorLogTrait;


    /**
     * Get all roles
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function allLanguages(Request $request)
    {
        try {
            
            // Create customer for bank
            $languages = config('common.tables.modals.tbl_languages')::get();
            
            return response()->json(['status' => 200, 'data' => $languages], 200);            
        } catch (Exception $ex) {
            return $this->handleErrorLogs($ex);
        }
    }

  
    
}

