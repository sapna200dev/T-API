<?php
namespace App\Http\Controllers\Api\Document;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Log;
use Storage;
use Auth;
use Carbon\Carbon;

// Custom Tarits
use App\Modals\ErrorLogTrait;
use App\Modals\Branch\Actions\Traits\BranchTrait;
use App\Modals\EndUser\Actions\Traits\EndUserTraits;

use App\Modals\Language\Actions\Traits\LanguageTrait;
use App\Modals\Employee\Actions\Traits\EmployeeTrait;
use App\Modals\Documents\Actions\Traits\DocumentTraits;
use App\Modals\Verification\Actions\Traits\VerificationTraits;
use App\Modals\Loan\Actions\Traits\LoanTrait;

use Exception;
use App\Exceptions;
use Illuminate\Auth\AuthenticationException;
use App\Exceptions\TurantCustomException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DocumentController extends Controller
{
    use DocumentTraits, EndUserTraits, BranchTrait, LanguageTrait, EmployeeTrait, VerificationTraits, LoanTrait, ErrorLogTrait;

    /**
     * UPlaod files
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadFile(Request $request)
    {
        try
        {

            ini_set("memory_limit", "-1");
            set_time_limit(0);
            ini_set('post_max_size', '64M');
            ini_set('upload_max_filesize', '64M');

            $response = [];
            if ($request->file('file'))
            {

                $csv_mimetypes = array(
                    'text/csv',
                    'text/plain',
                    'application/csv',
                    'text/comma-separated-values',
                    'application/excel',
                    'application/vnd.ms-excel',
                    'application/vnd.msexcel',
                    'text/anytext',
                    'application/octet-stream',
                    'application/txt',
                );

                $bank_id = Auth::user()->bank_id;
                $date = Carbon::now()->toDateString();

                $csv_file = $request->file('file');

                $file_original_name = $csv_file->getClientOriginalName();
                $file_mimetypes_type = $csv_file->getClientMimeType();

                if (!in_array($file_mimetypes_type, $csv_mimetypes))
                {
                    throw new TurantCustomException('We only accept CSV formatted file, please check your file. If you have further questions, please contact Bnak Admin support. Thanks!', 500);
                }

                $file_path_name = '/uploads/documents/' . $bank_id . '/' . $date . '/' . $file_original_name;

                if (file_exists(public_path($file_path_name)))
                {
                    throw new TurantCustomException('The file name is already uploaded !');
                }
                else
                {
                    $request->file('file')
                        ->move(public_path('/uploads/documents/' . $bank_id . '/' . $date) , $file_original_name);
                    // $filePath1 = 'uploads/documents/'.$bank_id.$date.'/'.$file_original_name;
                    $filePath1 = $file_original_name;

                    $response['status'] = true;
                    $response['msg'] = 'Successfully file uploaded !';
                    $response['filename'] = $filePath1;

                    return $response;
                }
            }
        }
        catch(TurantCustomException $ex)
        {
            return $this->handleErrorLogs($ex);
            // throw new TurantCustomException($ex->getMessage());
            
        }
        catch(Exception $ex)
        {
            return $this->handleErrorLogs($ex);
            throw $ex;
        }

    }

    /**
     * Proccess uploaded files
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function processFile(Request $request)
    {
        $returVal = null;
        try
        {

            if ($request->file('file'))
            {
                $bank_id = Auth::user()->bank_id;
                $date = Carbon::now()->toDateString();

                $csv_file = $request->file('file');
                $file_original_name = $csv_file->getClientOriginalName();
                $file_mimetypes_type = $csv_file->getClientMimeType();

                $filePath1 = 'uploads/documents/' . $bank_id . '/' . $date . '/' . $file_original_name;
                $csv = array_map("str_getcsv", file($filePath1));

                $resultArray = [];

                $finalFailedArry = [];
                $finalSuccessArry = [];

                $first_name = 'End Customer First Name';
                $middle_name = 'End Customer Middle Name';
                $last_name = 'End Customer Last Name';
                $regd_mobile = 'Regd Mobile';
                $phone_number = 'Alternate Phone';
                $status = 'Current Status ( Registration / Verification )';
                $preferred_langauge = 'Preferred Langauge';
                $loan_ac_no = 'Loan Acc No';
                $branch_name = 'Branch Name';
                $loan_amt = 'Loan Amt';
                $sales_agent = 'Sales Agent';
                $sale_agent_contact = 'Sale Agent Contact ';
                $sales_team_leader = 'Sales Team Leader';
                $sales_team_leader_contact = 'Sales Team Leader Contact';
                $loan_type = 'Loan Type';

                $coulmnArray = [];

                $firstNameIndex = '';
                $lastNmaeIndex = '';
                $middleNameIndex = '';
                $regdMobileIndex = '';
                $statusIndex = '';
                $preferredLangIndex = '';
                $loanAcNoIndex = '';
                $branchNameIndex = '';
                $loanAmtIndex = '';
                $salesAgentIndex = '';
                $salesAgentContactIndex = '';
                $salesTeamLeaderIndex = '';
                $salesTeamLeaderContactIndex = '';
                $phoneNumberIndex = '';

                $j = 0;
                $errorfalg = false;

                $successArry = [];
                $failedArry = [];
                foreach ($csv as $i => $row)
                {
                    if ($i == 2)
                    {
                        $coulmnArray = $row;
                        foreach ($coulmnArray as $i1 => $row1)
                        {
                            switch ($row1)
                            {
                                case $first_name:
                                    $firstNameIndex = $i1;
                                break;
                                case $last_name:
                                    $lastNmaeIndex = $i1;
                                break;

                                case $middle_name:
                                    $middleNameIndex = $i1;
                                break;

                                case $regd_mobile:
                                    $regdMobileIndex = $i1;
                                break;

                                case $phone_number:
                                    $phoneNumberIndex = $i1;
                                break;

                                case $status:
                                    $statusIndex = $i1;
                                break;

                                case $preferred_langauge:
                                    $preferredLangIndex = $i1;

                                case $loan_ac_no:
                                    $loanAcNoIndex = $i1;

                                case $branch_name:
                                    $branchNameIndex = $i1;

                                case $loan_amt:
                                    $loanAmtIndex = $i1;

                                case $sales_agent:
                                    $salesAgentIndex = $i1;

                                case $sale_agent_contact:
                                    $salesAgentContactIndex = $i1;

                                case $sales_team_leader:
                                    $salesTeamLeaderIndex = $i1;

                                case $sales_team_leader_contact:
                                    $salesTeamLeaderContactIndex = $i1;
                                break;
                                default:
                                    # code...
                                    
                                break;
                            }
                        }
                    }
                    if ($i > 2)
                    {

                        if ($firstNameIndex != '')
                        {
                            if ($row[$firstNameIndex] == '')
                            {
                                $resultArray['errormsg'][] = 'please enter valide ' . $first_name . ' , This is Mandatory at row' . $i;
                            }
                        }

                        if ($lastNmaeIndex != '')
                        {
                            if ($row[$lastNmaeIndex] == '')
                            {
                                $resultArray['errormsg'][] = 'please enter valide ' . $last_name . ' , This is Mandatory at row' . $i;
                            }
                        }

                        if ($middleNameIndex != '')
                        {
                            if ($row[$middleNameIndex] == '')
                            {
                                $resultArray['errormsg'][] = 'please enter valide ' . $middle_name . ' , This is Mandatory at row' . $i;
                            }
                        }

                        if ($regdMobileIndex != '')
                        {
                            if ($row[$regdMobileIndex] == '')
                            {
                                $errorfalg = true;
                                // $resultArray['errormsg'][] = 'please enter valide phone number to row '.$i. ' and field name is '. $phone_number;
                                $resultArray['errormsg'][] = 'please enter valide ' . $regd_mobile . ' , This is Mandatory at row ' . $i;
                            }
                            if (strlen($row[$regdMobileIndex]) != 10)
                            {
                                $errorfalg = true;
                                $resultArray['errormsg'][] = $regd_mobile . ' must be 10 digits at row ' . $i;
                            }
                        }

                        if ($phoneNumberIndex != '')
                        {
                            if ($row[$phoneNumberIndex] == '')
                            {
                                $errorfalg = true;
                                // $resultArray['errormsg'][] = 'please enter valide phone number to row '.$i. ' and field name is '. $phone_number;
                                $resultArray['errormsg'][] = 'please enter valide ' . $phone_number . ' , This is Mandatory at row  ';
                            }
                            if (strlen($row[$phoneNumberIndex]) != 10)
                            {
                                $errorfalg = true;
                                $resultArray['errormsg'][] = $phone_number . ' must be 10 digits at row ' . $i;
                            }
                        }

                        // if ($statusIndex != '')
                        // {
                        //     if ($row[$statusIndex] == '')
                        //     {
                        //         $resultArray['errormsg'][] = 'please enter valide ' . $status . ' , This is Mandatory at row' . $i;
                        //     }
                        // }
                        if ($preferredLangIndex != '')
                        {
                            if ($row[$preferredLangIndex] == '')
                            {
                                $errorfalg = true;
                                $resultArray['errormsg'][] = 'please enter valide ' . $preferred_langauge . ' , This is Mandatory at row' . $i;
                            }
                        }

                        if ($loanAcNoIndex != '')
                        {
                            if ($row[$loanAcNoIndex] == '')
                            {
                                $errorfalg = true;
                                $resultArray['errormsg'][] = 'please enter valide ' . $loan_ac_no . ' , This is Mandatory at row' . $i;
                            }
                        }

                        if ($branchNameIndex != '')
                        {
                            if ($row[$branchNameIndex] == '')
                            {
                                $errorfalg = true;
                                $resultArray['errormsg'][] = 'please enter valide ' . $branch_name . ' , This is Mandatory at row' . $i;
                            }
                        }

                        if ($loanAmtIndex != '')
                        {
                            if ($row[$loanAmtIndex] == '')
                            {
                                $errorfalg = true;
                                $resultArray['errormsg'][] = 'please enter valide ' . $loan_amt . ' , This is Mandatory at row' . $i;
                            }
                        }

                        if ($salesAgentIndex != '')
                        {
                            if ($row[$salesAgentIndex] == '')
                            {
                                $errorfalg = true;
                                $resultArray['errormsg'][] = 'please enter valide ' . $sales_agent . ' , This is Mandatory at row' . $i;
                            }
                        }

                        if ($salesAgentContactIndex != '')
                        {
                            if ($row[$salesAgentContactIndex] == '')
                            {
                                $errorfalg = true;
                                $resultArray['errormsg'][] = 'please enter valide ' . $sale_agent_contact . ' , This is Mandatory at row' . $i;
                            }
                        }

                        if ($salesTeamLeaderIndex != '')
                        {
                            if ($row[$salesTeamLeaderIndex] == '')
                            {
                                $errorfalg = true;
                                $resultArray['errormsg'][] = 'please enter valide ' . $sales_team_leader . ' , This is Mandatory at row' . $i;
                            }
                        }

                        if ($salesTeamLeaderContactIndex != '')
                        {
                            if ($row[$salesTeamLeaderContactIndex] == '')
                            {
                                $errorfalg = true;
                                $resultArray['errormsg'][] = 'please enter valide ' . $sales_team_leader_contact . ' , This is Mandatory at row' . $i;
                            }
                        }

                        if ($errorfalg)
                        {
                            array_push($failedArry, $row);
                        }
                        else
                        {
                            array_push($successArry, $row);
                        }

                    }

                    $j++;
                }

                $response = [];
                $failed_file_name = null;
                $success_file_name = null;

                if (count($failedArry) > 0)
                {
                    $failed_file_name = "failed_file_" . date("Y-m-d h:i:s") . ".csv";
                    $file_path_check = 'uploads/documents/' . $bank_id . '/' . $date . '/Failed/';
                    // $new_csv = fopen('uploads/documents/'.$bank_id.'/'.$date.'/Success/'.$file_name, 'w');
                    $file_path = '';

                    if (!file_exists($file_path_check))
                    {
                        mkdir($file_path_check, 0777, true);
                        $new_csv = fopen($file_path_check . $failed_file_name, 'w');
                    }
                    else
                    {
                        $file_path = $file_path_check;
                        $new_csv = fopen($file_path_check . $failed_file_name, 'w');
                    }

                    fputcsv($new_csv, $coulmnArray);

                    foreach ($failedArry as $key => $value)
                    {
                        fputcsv($new_csv, $value);
                    }
                    fclose($new_csv);
                }

                if (count($successArry) > 0)
                {
                    $success_file_name = "success_file_" . date("Y-m-d h:i:s") . ".csv";
                    $file_path_check = 'uploads/documents/' . $bank_id . '/' . $date . '/Success/';
                    $file_path = '';

                    if (!file_exists($file_path_check))
                    {
                        mkdir($file_path_check, 0777, true);
                        $new_csv = fopen($file_path_check . $success_file_name, 'w');
                    }
                    else
                    {
                        $file_path = $file_path_check;
                        $new_csv = fopen($file_path_check . $success_file_name, 'w');
                    }

                    fputcsv($new_csv, $coulmnArray);

                    foreach ($successArry as $key => $value)
                    {
                        fputcsv($new_csv, $value);
                    }
                    fclose($new_csv);
                }

                if ($j <= 3)
                {
                    $resultArray['status'] = false;
                    $resultArray['errormsg'] = "Please upload valid csv file with records";
                } // End if
                

                $uploaded = null;
                if (count($successArry) != 0 || count($failedArry) != 0)
                {
                    // Save proccessed records in db
                    $uploaded = $this->fillRecordsCountInDB($file_original_name, $failedArry, $successArry, $failed_file_name, $success_file_name);
                    if ($uploaded == null)
                    {
                        throw new Exception("File not processed, please check with file !", 500);
                    } // End if
                    
                } // End if
                

                if (count($successArry) != 0)
                {
                    // Insert bulk records
                    $this->insertEndUserRecordsInDB($csv, $regdMobileIndex, $statusIndex, $salesAgentContactIndex, $salesTeamLeaderContactIndex, $successArry);
                } // End if
                

                $resultArray['status'] = $errorfalg ? false : true;
                $resultArray['columns'] = $coulmnArray;
                $resultArray['failedArry'] = $failedArry;
                $resultArray['successArry'] = $successArry;
                $resultArray['successMsg'] = (count($successArry) > 0 || count($failedArry) > 0) ? 'File successfully uploaded !' : '';
                $resultArray['isProccessed'] = ($uploaded != null) ? true : false;

                return $resultArray;
            }

        }
        catch(Exception $ex)
        {
            $returVal = null;
            return $this->handleErrorLogs($ex);
        }
    }

    /**
     * Save processed files in DB
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function fillRecordsCountInDB($file_name, $failedRecords, $successRecords, $failedFileName, $successFileName)
    {
        $returVal = null;
        try
        {
            // Save process document
            $save = $this->saveProcessDocument($file_name, $failedRecords, $successRecords, $failedFileName, $successFileName);

            $returVal = $save;
        }
        catch(Exception $ex)
        {
            $returVal = null;
            return $this->handleErrorLogs($ex);
        }

        return $returVal;
    }

    /**
     * Create End user record
     *
     * @param  \Illuminate\Http\Request  $csv $data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function insertEndUserRecordsInDB($csv, $regdMobileIndex, $statusIndex, $salesAgentContact, $teamAgentContact, $records)
    {
        $returVal = null;
        try
        {
            $coulmnArray = [];

            foreach ($records as $i => $row)
            {
                if ($row[$regdMobileIndex] != '')
                {

                    // Get user registered number
                    $endUserExist = $this->getEndUserByRegisteredMobile($row[$regdMobileIndex], $row[$salesAgentContact], $row[$teamAgentContact]);
                    if ($endUserExist == null)
                    {

                        // Create payload
                        $payload = $this->createRequestPayload($csv, $row);

                        // Creat new record
                        $this->insertBulkRecord($payload);
                    }
                    else
                    {

                        if (isset($row[$statusIndex]) && $row[$statusIndex] != '')
                        {
                            switch ($row[$statusIndex])
                            {
                                case 'disbursement':
                                    // print_r(json_encode($endUserExist));
                                    // Update user call status
                                    $this->checkUserRegistrationStatus($endUserExist);
                                break;

                                default:
                                    # code...
                                    
                                break;
                            }
                        }

                    }
                }
            }

            $returVal = 'Status updated';
        }
        catch(Exception $ex)
        {
            $returVal = null;
            return $this->handleErrorLogs($ex);
        }

        return $returVal;
    }

    /**
     * Create Request Payload
     *
     * @param  \Illuminate\Http\Request  $csv $data
     *
     * @return \Illuminate\Http\JsonResponse
     */
    private function createRequestPayload($csv, $data)
    {
        $returVal = [];
        try
        {

            // return $data;
            // Get langauge name
            $preferred_langauge_id = $this->getLanguageByName($data[7])->id;

            // Loan type
            $loan_type = $this->getLoanType($data[8]);

            $branch = $this->getBranchByName($data[10]);
            if ($branch == null)
            {
                $branch_id = $this->createNewBranch($data[10])->id;
            }
            else
            {
                $branch_id = $branch->id;
            }

            // Get sale agent
            $saleAgent = $this->getSaleAgentByContact($data[13], true);

            // Get team leader
            $teamAgent = $this->getSaleAgentByContact($data[15], false);

            // Create custom payload for create EUR
            $request = [];
            $request['title'] = $data[0];
            $request['first_name'] = $data[1];
            $request['middle_name'] = $data[2];
            $request['last_name'] = $data[3];
            $request['registered_mobile'] = $data[4];
            $request['alternate_phone'] = $data[5];
            $request['current_status'] = 'Provisioned';
            $request['call_action'] = 'Registration - 1st call';
            $request['call_status'] = 'Provisioned';
            $request['preferred_langauge_id'] = $preferred_langauge_id;
            $request['loan_account_number'] = $data[9];
            $request['branch_id'] = $branch_id;
            $request['loan_amount'] = $data[11];
            $request['sale_agent_id'] = $saleAgent->id;
            $request['sales_agent_contact'] = $saleAgent->mobile_number;
            $request['team_leader_id'] = $teamAgent->id;
            $request['team_leader_contact'] = $teamAgent->mobile_number;
            $request['loan_type_id'] = $loan_type->id;

            $returVal = $request;
        }
        catch(Exception $e)
        {
            $returVal = [];
            return $this->handleErrorLogs($ex);
        }

        return $returVal;
    }

    /**
     * Get all proccessed files from DB
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProcessedFiles(Request $request)
    {
        $returVal = [];
        try
        {
            $files = $this->getProccessedFilesFromDB($request);

            return response()->json(['status' => 200, 'data' => $files], 200);
        }
        catch(Exception $e)
        {
            $returVal = [];
            return $this->handleErrorLogs($ex);
        }
    }

    /**
     * Download failed csv file
     *
     * @param  \Illuminate\Http\Request  $request, $id, $bank_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function failedDownloadFile(Request $request, $id, $bank_id)
    {
        try
        {
            $file = $this->getDocmentFileById($id, $bank_id);

            if ($file == null)
            {
                throw new Exception("File dose not exist in the system !", 500);
            }
            $date = Carbon::now()->toDateString();

            $file_name = $file->file_name;
            $failed_file_name = $file->failed_file_name;

            $filePath1 = 'uploads/documents/' . $bank_id . '/' . $date . '/Failed/' . $failed_file_name;
            header('Set-Cookie: fileDownload=true; path=/');
            header("Content-type: text/csv");
            header("Access-Control-Allow-Headers: *");
            header("Cache-Control: max-age=60, must-revalidate");
            header("Content-Disposition: attachment; filename=" . $failed_file_name . "");

            readfile($filePath1);
        }
        catch(Exception $e)
        {
            return $this->handleErrorLogs($ex);
        }
    }

    /**
     * Download success csv file
     *
     * @param  \Illuminate\Http\Request  $request, $id, $bank_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function successDownloadFile(Request $request, $id, $bank_id)
    {
        try
        {
            $file = $this->getDocmentFileById($id, $bank_id);
            // return $file;
            if ($file == null)
            {
                throw new Exception("FIle dose not exist in the system !", 500);
            }
            $date = Carbon::now()->toDateString();

            $file_name = $file->file_name;
            $success_file_name = $file->success_file_name;

            $filePath1 = 'uploads/documents/' . $bank_id . '/' . $date . '/Success/' . $success_file_name;
            header('Set-Cookie: fileDownload=true; path=/');
            header("Content-type: text/csv");
            header("Access-Control-Allow-Headers: *");
            header("Cache-Control: max-age=60, must-revalidate");
            header("Content-Disposition: attachment; filename=" . $success_file_name . "");

            readfile($filePath1);
        }
        catch(Exception $ex)
        {
            return $this->handleErrorLogs($ex);
        }
    }

    /**
     * Download csv file
     *
     * @param  \Illuminate\Http\Request  $request, $id, $bank_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function DownloadFile(Request $request, $id, $bank_id)
    {
        try
        {
            $file = $this->getDocmentFileById($id, $bank_id);
            // return $file;
            if ($file == null)
            {
                throw new Exception("FIle dose not exist in the system !", 500);
            }
            $date = Carbon::now()->toDateString();
            $file_name = $file->file_name;

            $filePath1 = 'uploads/documents/' . $bank_id . '/' . $date . '/' . $file_name;
            header('Set-Cookie: fileDownload=true; path=/');
            header("Content-type: text/csv");
            header("Access-Control-Allow-Headers: *");
            header("Cache-Control: max-age=60, must-revalidate");
            header("Content-Disposition: attachment; filename=" . $file_name . "");

            readfile($filePath1);
        }
        catch(Exception $ex)
        {
            return $this->handleErrorLogs($ex);
        }
    }

    /**
     * Delete processed file
     *
     * @param  \Illuminate\Http\Request  $request, $id, $bank_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteProcessedFile(Request $request, $id)
    {
        $returVal = null;
        try
        {
            // set visible flag 0
            $delete_file = $this->deleteProcessedFileFromDB($id);
            if (!$delete_file)
            {
                throw new Exception("File dose not exist in our system !", 500);
            }

            return response()->json(['status' => 200, 'msg' => 'Successfully deleted file !'], 200);
        }
        catch(Exception $ex)
        {
            return $this->handleErrorLogs($ex);
        }
    }

}

