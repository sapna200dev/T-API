<?php
namespace App\Http\Controllers\Api\Branch;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Log;

// Custom Tarits
use App\Modals\ErrorLogTrait;
use App\Modals\Branch\Actions\Traits\BranchTrait;

use App\Http\Requests\BranchRequests\CreateBranchRequest;


use Exception;
use Illuminate\Auth\AuthenticationException;
use App\Exceptions\TurantCustomException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BranchController extends Controller
{
    use BranchTrait, ErrorLogTrait;


    /**
     * Get all employees for bank
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllBracnhes()
    {
        try {
     
            // Create customer for bank
            $branches = $this->getBranches();
            
            return response()->json(['status' => 200, 'data' => $branches], 200);            
        } catch (Exception $ex) {
            return $this->handleErrorLogs($ex);
        }
    }

  

    /**
     * Get all employees for bank
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createBranch(CreateBranchRequest $request)
    {
        try {
     
            $this->createNewBranch($request);


            // Create customer for bank
            $branches = $this->getBranches();
            
            return response()->json(['status' => 200, 'data' => $branches], 200);            
        } catch (Exception $ex) {
            return $this->handleErrorLogs($ex);
        }
    }

  



    
}

