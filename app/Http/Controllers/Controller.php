<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;


    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="Turant API documentation",
     *      description="Turan api documnetation for api testing",
     * )
     *
      * @OA\SecurityScheme(
      *     type="http",
      *     description="Set authentication token",
      *     name="Token based Based",
      *     in="header",
      *     scheme="bearer",
      *     bearerFormat="passport",
      *     securityScheme="bearer",
      * )
     *
     *
     */

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
