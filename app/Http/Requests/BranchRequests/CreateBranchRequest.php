<?php

namespace App\Http\Requests\BranchRequests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class CreateBranchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "branch_name"=>"required|min:2|alpha",
            "city_id"=>"required|",
            "state_id"=>"required",
            "address"=>"required",
            "area"=>"required",
            "locality"=>"required|min:5|max:20",
            "pin_code"=>"required|numeric",
            "branch_manager_name"=>"required|min:2",
            "phone"=>"required|min:10|numeric",
            "team_lead_id"=>"required",
            "sale_agent_id"=>"required",
            // "phone"=>"required|min:6",

        ];
    }

     /**
     * Set custom validation messages
     *
     * @return array
     */
    public function messages() {
        return [
            'branch_name.required' => 'Branch name is required',
            'city_id.required' =>'Slect city name is required.',
            'branch_name.required' =>'Select Branch name isrequired.',
            'state_id.required' =>'state is required.',
            'address.required' =>'Address  is required.',
            'area.required' =>'Area is required.',
            'locality.required' =>'Locality field is required.',
            'pin_code.required' =>'Pin code is required.',
            'branch_manager_name.required' =>'Branch manager name is required.',
            'phone.required' =>'Phone is required.',
            'team_lead_id.required' =>'Team lead is required.',
            'sale_agent_id.required' =>'Sale agent is required.',
        ];
    }
     /**
     * [failedValidation [Overriding the event validater for custom error response]]
     * @param  Validator $validator [description]
     * @return [object][object of various validation errors]
     */
    public function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json(['status' => 400, 'msg' => $validator->errors()->first()], 400));
    }
}
