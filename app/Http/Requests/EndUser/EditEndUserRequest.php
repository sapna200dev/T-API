<?php

namespace App\Http\Requests\EndUser;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class EditEndUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "first_name"=>"required|min:2|alpha",
            "last_name"=>"required|min:2|alpha",
            "registered_mobile"=>"required|regex:/^[0-9]+$/|min:10|max:10",
            "loan_account_number"=>"required|regex:/^[0-9]+$/",
            "loan_amount"=>"required|regex:/^[0-9]+$/",
            "alternate_phone"=>"nullable|regex:/^[0-9]+$/|min:10|max:10",
            // "preferred_langauge_id"=>"required",
            // "loan_type_id"=>"required",
            // "branch_id"=>"required",
            "sale_agent_id"=>"required",
            // "team_leader_id"=>"required",
            "registeration_trigger"=>"nullable",
            "verification_trigger"=>"nullable",
        ];
    }

     /**
     * Set custom validation messages
     *
     * @return array
     */
    public function messages() {
        return [
            'first_name.required' => 'First name is required',
            'last_name.required' =>'Last name is required.',
            'registered_mobile.required' =>'Registered Mobile isrequired.',
            'loan_account_number.required' =>'Loan Account Number is required.',
            'loan_amount.required' =>'Loan Amount  is required.',
            'alternate_phone.required' =>'Alternate Phone is required.',
            'preferred_langauge_id.required' =>'preferred langauge field is required.',
            'loan_type_id.required' =>'Loan Type is required.',
            'branch_id.required' =>'Branch is required.',
            'sale_agent_id.required' =>'Sales agent is required.',
            'team_leader_id.required' =>'Team leader is required.',
        ];
    }
     /**
     * [failedValidation [Overriding the event validater for custom error response]]
     * @param  Validator $validator [description]
     * @return [object][object of various validation errors]
     */
    public function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json(['status' => 400, 'msg' => $validator->errors()->first()], 400));
    }
}
