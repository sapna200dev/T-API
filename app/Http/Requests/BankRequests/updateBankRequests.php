<?php

namespace App\Http\Requests\BankRequests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class updateBankRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "bank_name" => "required|alpha",
            "location"=>"required",
            "address"=>"required",
            "status"=>"required",
            "billing_status"=>"required",
            "company_email"=>"required|email|regex:/\S+@\S+\.\S+/",
            "compnay_phone"=>"nullable|min:10|max:10",
            "contact_email"=>"required|email|regex:/\S+@\S+\.\S+/",
            "contact_name"=>"required",
            "contact_phone"=>"required|min:10|max:10",
            "contact_location"=>"required",
            "billing_cycle"=>"required",
            "billing_cycle_from"=>"required",
            "billing_cycle_to"=>"required"
        ];
    }
     /**
     * Set custom validation messages
     *
     * @return array
     */
    public function messages() {
        return [
            'bank_name.required' => 'Bank name field is required.',
            'location.required' =>'Location field is required.',
             'address.required' =>'Address field is required.',
            'status.required' =>'Statue field is required.',
            'billing_status.required' =>'Billing Status field is required.',
            'company_email.required' =>'Compnay Email field is required.',
            'compnay_phone.required' =>'Company Phone field is required.',
             'contact_email.required' =>'Contact email field is required.',
            'contact_name.required' =>'Contact person name field is required.',
            'contact_location.required' =>'Contact location field is required.',
            'billing_cycle.required' =>'Billing Cycle field is required.',
            'billing_cycle_from.required' =>'Billing Cycle from field is required.',
            'billing_cycle_to.required' =>'Billing Cycle to field is required.',
        ];
    }
     /**
     * [failedValidation [Overriding the event validater for custom error response]]
     * @param  Validator $validator [description]
     * @return [object][object of various validation errors]
     */
    public function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json(['status' => 400, 'msg' => $validator->errors()->first()], 400));
    }
}
