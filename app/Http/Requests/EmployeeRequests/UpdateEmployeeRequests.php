<?php

namespace App\Http\Requests\EmployeeRequests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class UpdateEmployeeRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "employee.first_name"=>"required|min:2|alpha",
            "employee.last_name"=>"required|min:2|",
            "employee.branch_id"=>"required",
            "employee.status"=>"required",
            "employee.role_id"=>"required",
            "employee.mobile_number"=>"required|min:10|max:10",
            "employee.email"=>"required|email",
            "employee.username"=>"required|min:2",

        ];
    }

     /**
     * Set custom validation messages
     *
     * @return array
     */
    public function messages() {
        return [
            'first_name.required' => 'First name is required',
            'last_name.required' =>'Last name is required.',
            'branch_name.required' =>'branch isrequired.',
            'status.required' =>'status is required.',
            'role.required' =>'role  is required.',
            'mobile_number.required' =>'mobile number is required.',
            'email.required' =>'email field is required.',
            'username.required' =>'username is required.',

        ];
    }
     /**
     * [failedValidation [Overriding the event validater for custom error response]]
     * @param  Validator $validator [description]
     * @return [object][object of various validation errors]
     */
    public function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json(['status' => 400, 'msg' => $validator->errors()->first()], 400));
    }
}
