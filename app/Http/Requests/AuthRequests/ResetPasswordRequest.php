<?php

namespace App\Http\Requests\AuthRequests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class ResetPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "token" => "required",
            "email" => "required|email",
            "password" => "min:6|required_with:confirm_password|same:confirm_password",
            "confirm_password" => "min:6",
        ];
    }
     /**
     * Set custom validation messages
     *
     * @return array
     */
    public function messages() {
        return [
            'email.required' => 'Email field is required.',
            'email.password' => 'password field is required and minimum 6 charctors.',
            'email.confirm_password' => 'confirm_password field is required must be match with password. ',
        ];
    }
     /**
     * [failedValidation [Overriding the event validater for custom error response]]
     * @param  Validator $validator [description]
     * @return [object][object of various validation errors]
     */
    public function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json(['status' => 400, 'msg' => $validator->errors()->first()], 400));
    }
}
