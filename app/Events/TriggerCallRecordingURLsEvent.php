<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class TriggerCallRecordingURLsEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $record;
    public $payload;
    public $number;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct( $number=null, $payload)
    {
        // $this->record = $record;
        $this->payload = $payload;
        $this->number = $number;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
