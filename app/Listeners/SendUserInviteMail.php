<?php

namespace App\Listeners;

use Log;
use Mail;
use App\Mail\SendUserPassEmail;
use App\Events\SendUserEmail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendUserInviteMail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendUserEmail  $event
     * @return void
     */
    public function handle(SendUserEmail $event)
    {
        // Send email for reset password
        $to_email = $event->user->admin_username;

        Mail::to($to_email)->send(new SendUserPassEmail($event->user));

    }
}
