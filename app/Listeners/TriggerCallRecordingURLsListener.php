<?php

namespace App\Listeners;

use Log;

use App\Events\TriggerCallRecordingURLsEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use App\Modals\EndUser\Actions\Traits\EndUserTraits;

use App\Modals\Telnyx\Actions\Traits\TelnyxTraits;
use App\Modals\Employee\Actions\Traits\EmployeeTrait;
use App\Modals\IVRRules\Actions\Traits\IVRRuleTrait;
use Exception;


class TriggerCallRecordingURLsListener
{
    use TelnyxTraits, EmployeeTrait, IVRRuleTrait, EndUserTraits;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    { 
        //
    }

    /**
     * Handle the event.
     * 
     * @param  TriggerCallRecordingURLsEvent  $event
     * @return void
     */
    public function handle(TriggerCallRecordingURLsEvent $event)
    {

        try {
            // Get sales (employee) agent number
            $employee = $this->getEmployeeBankIdByNumber($event->number);
            if($employee == null) {
                throw new Exception("Number dose not exist in our system !", 500);
            } else {
                // Get iVR Rules for call according bank
                $record = $this->getIvrRulesForCall($employee->bank_id);
                if($record != null ) {
                    log::debug('call first welcome playback api ');

                    // Call playback api
                    $this->callAudioPlayAPI($event->payload, $record, $record->welcome_text_audio_recording);

                } else {
                    throw new Exception("Recording URLs not found ! ", 500);
                } // End if-else    
            } // End if

        } catch (Exception $e) {
            throw new Exception($e->getMessage(), 500);
            
        }


    }
}
