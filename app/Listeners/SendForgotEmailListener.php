<?php

namespace App\Listeners;

use Mail;

// Mail
use App\Mail\ForgotPassword;

use App\Events\UserForgotEmailEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendForgotEmailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserForgotEmailEvent  $event
     * @return void
     */
    public function handle(UserForgotEmailEvent $event)
    {
        // Send email for reset password
        $to_email = $event->user->email;

        Mail::to($to_email)->send(new ForgotPassword($event->token, $event->user));
    }
}
