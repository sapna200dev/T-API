<?php

namespace App\Listeners;

use Mail;
use Log;

// Mail
use App\Mail\SendEmployeeLoginMail;

use App\Events\SendEmployeeUserPassEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmployeeUserPassListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendEmployeeUserPassEvent  $event
     * @return void
     */
    public function handle(SendEmployeeUserPassEvent $event)
    {

        log::debug('user login sssss= ');
        log::debug($event->user);
        log::debug($event->user->employee);

        // Send email for reset password
        $to_email = $event->user->employee['username'];



        Mail::to($to_email)->send(new SendEmployeeLoginMail($event->user));
    }
}
