<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Exception;
use Storage;

use App\Events\TriggerCallRecordingURLsEvent;
use App\Modals\Employee\Actions\Traits\EmployeeTrait;
use App\Modals\IVRRules\Actions\Traits\IVRRuleTrait;


use Illuminate\Support\Facades\Mail;

class CallRecordingUrls extends Command
{
    use IVRRuleTrait, EmployeeTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check-audio-urls {number?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check call recordings';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $number = $this->argument('number');

       if(!$number) {
            $this->error('Number is missing');
            return false;
        }
        // return $this->info($number);
        // return $number;
        // $employee = $this->getEmployeeBankIdByNumber($number);

        // $record = $this->getIvrRulesForCall($employee->bank_id);
        // if($record != null) {
        //  if(isset($record->recording_rules) && $record->recording_rules != null ) {
        //      $audio_url = $record->recording_rules;
        //  }

        // }    
        $payload = $this->getPayload();


        event(new TriggerCallRecordingURLsEvent($number, $payload));

        $this->info('successfully Done!');
        // $token = $this->argument('token');
        // $isDebug = $this->argument('debug');

        // if(!$token) {
        //     $this->error('Token is missing');
        //     return false;
        // }
        // if($token !== config('configs.report_process_token')) {
        //     $this->error('Token does not match.');
        //     return false;
        // }        
        // $this->info('Done!');
    }



    private function getPayload()
    {
        $returnVal=[];
        try {

            $request = [
                'data' => [
                    'event_type' => 'call.answered',
                    'id' => 'e53148cb-46af-4d29-bd98-4124b42751eb',
                    'occurred_at' => '2020-10-22T10:51:20.047738Z',
                    'payload' => [
                        'call_control_id' => 'v2:dIlTRWnR2p6y38Z-FiLMORvOumkb4_oM2efFhhGLTx7uAwuvLWcbMg',
                        'call_leg_id' => '363b21d6-151c-11eb-8b02-02420a0fca68',
                        'call_session_id' => '3635f6ca-151c-11eb-bd08-02420a0fca68',
                        'client_state' => NULL,
                        'connection_id' => '1476163749613144047',
                        'direction' => 'outgoing',
                        'from' => '+919028466135',
                        'state' => 'bridging',
                        'to' => '+919890638629',
                    ],
                    'record_type' => 'event',
                ],
                'meta' => [
                    'attempt' => 1,
                    'delivered_to' => 'https://mturbine.com/api/outbound-call-webhook',
                ]
            ];


            $payload=[];
            $payload['status'] = $request['data']['event_type'];
            $payload['call_id'] = $request['data']['id'];
            $payload['call_date'] = $request['data']['occurred_at'];
            $payload['call_control_id'] = $request['data']['payload']['call_control_id'];
            $payload['call_leg_id'] = $request['data']['payload']['call_leg_id'];
            $payload['call_session_id'] = $request['data']['payload']['call_session_id'];
            $payload['state'] = isset($request['data']['payload']['state']) && $request['data']['payload']['state']!='' ? $request['data']['payload']['state'] : null;
            $payload['connection_id'] = $request['data']['payload']['connection_id'];
            $payload['from'] = isset($request['data']['payload']['from']) ? $request['data']['payload']['from'] : null;
            $payload['to'] = isset($request['data']['payload']['to']) ? $request['data']['payload']['to'] : null;
            $payload['delivered_to'] = isset($request['meta']['delivered_to']) ? $request['meta']['delivered_to'] :null;
            
            // log::debug('payload => ');
            // log::debug($payload)?;

            $returnVal=$payload;
        } catch (Exception $e) {
            $returnVal=[];
            throw new Exception("Error occured during payload", 500);
        }

        return $returnVal;
    }
    
    
}
