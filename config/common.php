<?php


use App\Modals\User\User;
use App\Modals\Role\Role;
use App\Modals\Token\Token;
use App\Modals\Bank\Bank;
use App\Modals\Branch\Branch;
use App\Modals\Loan\Loan;
use App\Modals\Language\Language; 
use App\Modals\Employee\Employee;
use App\Modals\EndUser\EndUser;
use App\Modals\Documents\Document;
use App\Modals\Verification\Verification;
use App\Modals\Logs\ProvisionLog;
use App\Modals\IVRRules\IVRRule;
use App\Modals\IVRRulesRecording\IVRRulesRecording;
use App\Modals\CallMapping\CallMapping;
use App\Modals\States\State;
use App\Modals\City\City;

return [
    /*
    |--------------------------------------------------------------------------
    | Handle comman fields
    |--------------------------------------------------------------------------
    |
    */

    'telynx_config' => [
        'turant' => [
            'call' => [
                'headers' => [
                    "accept: application/json",
                    "content-type: application/json",
                    "Authorization: Bearer KEY0174B8924CE49DEB7AD778DD6AF64E0D_3VdCyVZ793N3YQcpHega01",
                ],
                'domain' => env('CALL_DOMAIN', 'https://api.telnyx.com/'),
                'outbound' => [
                    'register' => [
                        'v2' => [
                            'initiate_call' => 'https://api.telnyx.com/v2/calls',
                            'answer_call' => ''
                        ]
                    ]
                ]
            ],
        ],


        'api_key' => env('API_KEY', 'KEY0174B8924CE49DEB7AD778DD6AF64E0D_3VdCyVZ793N3YQcpHega01'),
        'connection_id' => env('CONNECTION_ID', '1476163749613144047'),
        'audio_url' => env('AUDIO_URL', 'https://file-examples-com.github.io/uploads/2017/11/file_example_MP3_700KB.mp3')
    ],

    'token' => [
        'forgot_password_type' => 'FORGOT_PASSOWRD',
        'reset_password_type' => 'RESET_PASSOWRD'
    ],

    'values' => [
        'sale_agent' => [
            'role_id' => 2
        ],
        'team_lead' => [
            'role_id' => 4
        ],
    ],


    'host_url' => env('MAIL_URL', 'http://sapna-dev.turant.com:3000/'),
    's3' => [
        'upload' => [
            'audio' => [
                'folder' => env('UOLOAD_FOLDER', 'recordings'),
                'files_validation' => [
                    'audio/mp3',
                    'mp3',
                    'wav',
                    'audio/mpeg',
                    'audio/wav',
                ]
            ],
            'check_audio' =>  env('CHECK_AUDIO_FOLDER', 'check_audio'),
            'ivr_info_recording' =>  env('CHECK_IVR_INFO_RECORDING', 'ivr_info_recording'),
            'welcome_text_audio_recording' =>  env('WELCOME_TEXT_AUDIO', 'welcome_text_audio_recording'),
            'ai_dm_pass_audio' =>  env('AI_DM_PASS_AUDIO_FOLDER', 'ai-dm-pass-audio'),
            'ai_dm_fail_audio' =>  env('AI_DM_FAIL_AUDIO_FOLDER', 'ai-dm-fail-audio'),
            'playback_recording' =>  env('UPLOAD_PLAYBACK_RECORDING', 'playback_recording'),
        ],
        'driver_name' => env('AWS_DRIVER', 's3'),
        'domain' => 'https://'. env('AWS_BUCKET', 'turantbucket').'.s3.amazonaws.com',
    ],




    /*
    |--------------------------------------------------------------------------
    | Handle all turant modals
    |--------------------------------------------------------------------------
    |
    */
    'tables' => [
        'modals' => [
            'tbl_loan_types' => Loan::class,
            'tbl_users' => User::class,
            'tbl_roles' => Role::class,
            'tbl_tokens' => Token::class,
            'tbl_banks' => Bank::class,
            'tbl_branches' => Branch::class,
            'tbl_languages' => Language::class,
            'tbl_employees' => Employee::class,
            'tbl_end_users' => EndUser::class,
            'tbl_documents' => Document::class,
            'tbl_verifications' => Verification::class,
            'tbl_logs' => ProvisionLog::class,
            'tbl_ivr_rules' => IVRRule::class,
            'tbl_ivr_rules_recording' => IVRRulesRecording::class,
            'call_mapping' => CallMapping::class,
            'tbl_states' => State::class,
            'tbl_cities' => City::class,
        ],
    ],

];
